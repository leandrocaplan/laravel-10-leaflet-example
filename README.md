This is an update for an original example project for using [Leaflet JS](https://leafletjs.com) and [OpenStreetMap](https://www.openstreetmap.org) with Laravel.

The original one is this one:
https://github.com/nafiesl/laravel-leaflet-example

Since the original project it wasn't updated with major changes since 5 years ago, and was built with Laravel 5.8, this version is updated for working with Laravel 10, and with the lastest versions of all the packages used by it, since the versions used by the original project run out of support long ago.

In this version, I've migrated the project from working with Laravel Mix to work with Laravel Vite, since the first one is now obsolete and deprecated, and Vite it's a pretty faster and more efficient replacement.

The only package used in the original project that I couldn't update and make work, was "luthfi/simple-crud-generator", since it doesn't has support for working in Laravel 10 at the moment.
Maybe in a few weeks, it will have better support for it, but at the moment, we just won't use it, since this package is helpful for the app development, but not indispensable. Also, it isn't needed at all just for cloning and running the application locally.


I guess that the original installation instructions will mostly work for this update.
However, to work with Vite, you'll need to run this commands in addition to those:
'$ npm install'
'$ npm run dev'

The last one you should run it in a separated terminal, since we need to have both the Vite and the PHP servers running together, for a proper functioning of the app.

If you don't want to keep the Vite server listening to changes in the app, you can use '$ npm run build' instead of '$ npm run dev', but it's not recommended.

If there's something wrong when cloning and installing the project locally by following the installation steps showed here, please tell me (I've got pretty messed up when trying to do it with the original one, it took me hours to make it work).



So then, here's the content of the README.md of the original project:

# Laravel Leaflet JS - Example

This is an example project for [Leaflet JS](https://leafletjs.com) and [OpenStreetMap](https://www.openstreetmap.org) built with Laravel 5.8.

![Laravel Leaflet JS Project Example](public/screenshots/leaflet-map-01.jpg)

## Features

In this project, we have an Outlet Management (CRUD) with localtion/coordinate point that shown in map. We also have coordinate entry with direct map pointing on Outlet Create and Edit form.

## Installation Steps

Follow this instructions to install the project:

1. Clone this repo.
    ```bash
    $ git clone git@github.com:nafiesl/laravel-leaflet-example.git
    # or
    $ git clone https://github.com/nafiesl/laravel-leaflet-example.git
    ```
2. `$ cd laravel-leaflet-example`
3. `$ composer install`
4. `$ cp .env.example .env`
5. `$ php artisan key:generate`
6. Set **database config** on `.env` file
7. `$ php artisan migrate`
8. `$ php artisan serve`
10. Open `https://localhost:8000` with browser.

### Demo Records

If we need some outlet demo records, we can use model factory within tinker:

```bash
$ php artisan tinker
>>> factory(App\Outlet::class, 30)->create();
```

### Leaflet config

We have a `config/leaflet.php` file in this project. Set default **zoom level** and **map center** coordinate here (or in `.env` file).

```php
<?php

return [
    'zoom_level'           => 13,
    'detail_zoom_level'    => 16,
    'map_center_latitude'  => env('MAP_CENTER_LATITUDE', '-3.313695'),
    'map_center_longitude' => env('MAP_CENTER_LONGITUDE', '114.590148'),
];
```

> Please note that this is not an official or required config file from Leaflet JS, it is just a custom config for this project.

## Testing

Run PHPUnit to run feature test:

```bash
$ vendor/bin/phpunit
```

## License

This project is open-sourced software licensed under the [MIT license](LICENSE).
