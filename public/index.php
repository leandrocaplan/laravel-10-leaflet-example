<?php



/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

define('LARAVEL_START', microtime(true));

//debug_to_console("Logs de index.php:");


/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

//$app es un objeto de tipo IlluminateFoundationApplication
//La clase está definida en 'vendor/laravel/framework/src/Illuminate/Foundation/Application.php'
//Esta extiende de 'vendor/laravel/framework/src/Illuminate/Container/Container.php' (Contenedor de servicios)

//Este objeto es el mas importante de toda la aplicación Laravel: es nuestra instancia general de la Aplicación.
$app = require_once __DIR__.'/../bootstrap/app.php'; 
//debug_to_console("Tipo de objeto contenido en \$app:");
//debug_to_console(get_class($app));


/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

//$kernel es un objeto de tipo AppHttpKernel (devuelto por 'make' de '$app')
//La clase está definida en 'app/Http/Kernel.php'
//Esta extiende de 'vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php'
$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class); 


//debug_to_console("Tipo de objeto contenido en \$kernel:");
//debug_to_console(get_class($kernel));

//$response es un objeto de tipo IlluminateHttpResponse (devuelto por 'handle' de '$kernel')
//La clase está definida en 'vendor/laravel/framework/src/Illuminate/Http/Response.php'
$response = $kernel->handle(                        
    //$request es un objeto de tipo IlluminateHttpRequest
    //La clase está definida en 'vendor/laravel/framework/src/Illuminate/Http/Request.php'
    $request = Illuminate\Http\Request::capture()   
);

//debug_to_console("Tipo de objeto contenido en \$request:");
//debug_to_console(get_class($request));

//debug_to_console("Tipo de objeto contenido en \$response:");
//debug_to_console(get_class($response));

$response->send();

$kernel->terminate($request, $response);
