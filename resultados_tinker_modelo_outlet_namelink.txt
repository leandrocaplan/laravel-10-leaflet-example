Psy Shell v0.11.12 (PHP 8.2.5 — cli) by Justin Hileman
> use App\Outlet;
> use App\Http\Resources\Outlet as OutletResource;
> $outlets = Outlet::all();
= Illuminate\Database\Eloquent\Collection {#4887
    all: [
      App\Outlet {#4886
        id: 1,
        name: "Casa de Lean",
        address: "Arcos 3130 7° A, Nuñez, CABA",
        latitude: "-34.55116913874",
        longitude: "-58.46255421638",
        creator_id: 1,
        created_at: "2023-03-15 00:49:43",
        updated_at: "2023-03-27 09:50:20",
        +coordinate: "-34.55116913874, -58.46255421638",
        +map_popup_content: "<div class="my-2"><strong>Outlet Name:</strong><br><a href="http://localhost:8000/outlets/1" title="View Casa de Lean Outlet detail">Casa de Lean</a></div><div class="my-2"><strong>Outlet Address:</strong><br>Arcos 3130 7° A, Nuñez, CABA</div><div class="my-2"><strong>Coordinate:</strong><br>-34.55116913874, -58.46255421638</div>",
        +name_link: "<a href="http://localhost:8000/outlets/1" title="View Casa de Lean Outlet detail">Casa de Lean</a>",
      },
      App\Outlet {#4901
        id: 2,
        name: "Casa de Gaby",
        address: "Liniers 557, Lomas del Mirador",
        latitude: "-34.66036884245",
        longitude: "-58.52985620498",
        creator_id: 1,
        created_at: "2023-03-15 02:14:02",
        updated_at: "2023-03-15 02:29:23",
        +coordinate: "-34.66036884245, -58.52985620498",
        +map_popup_content: "<div class="my-2"><strong>Outlet Name:</strong><br><a href="http://localhost:8000/outlets/2" title="View Casa de Gaby Outlet detail">Casa de Gaby</a></div><div class="my-2"><strong>Outlet Address:</strong><br>Liniers 557, Lomas del Mirador</div><div class="my-2"><strong>Coordinate:</strong><br>-34.66036884245, -58.52985620498</div>",
        +name_link: "<a href="http://localhost:8000/outlets/2" title="View Casa de Gaby Outlet detail">Casa de Gaby</a>",
      },
      App\Outlet {#4914
        id: 99,
        name: "INSPT",
        address: "Triunvirato y Tronador",
        latitude: "-34.58419301438",
        longitude: "-58.46635222434",
        creator_id: 1,
        created_at: "2023-04-27 17:55:47",
        updated_at: "2023-04-27 17:55:47",
> $outlet = $outlets[0];
= App\Outlet {#4886
    id: 1,
    name: "Casa de Lean",
    address: "Arcos 3130 7° A, Nuñez, CABA",
    latitude: "-34.55116913874",
    longitude: "-58.46255421638",
    creator_id: 1,
    created_at: "2023-03-15 00:49:43",
    updated_at: "2023-03-27 09:50:20",
    +coordinate: "-34.55116913874, -58.46255421638",
    +map_popup_content: "<div class="my-2"><strong>Outlet Name:</strong><br><a href="http://localhost:8000/outlets/1" title="View Casa de Lean Outlet detail">Casa de Lean</a></div><div class="my-2"><strong>Outlet Address:</strong><br>Arcos 3130 7° A, Nuñez, CABA</div><div class="my-2"><strong>Coordinate:</strong><br>-34.55116913874, -58.46255421638</div>",
    +name_link: "<a href="http://localhost:8000/outlets/1" title="View Casa de Lean Outlet detail">Casa de Lean</a>",
  }

> $resource=new OutletResource($outlet);
= App\Http\Resources\Outlet {#4880
    +resource: App\Outlet {#4886
      id: 1,
      name: "Casa de Lean",
      address: "Arcos 3130 7° A, Nuñez, CABA",
      latitude: "-34.55116913874",
      longitude: "-58.46255421638",
      creator_id: 1,
      created_at: "2023-03-15 00:49:43",
      updated_at: "2023-03-27 09:50:20",
      +coordinate: "-34.55116913874, -58.46255421638",
      +map_popup_content: "<div class="my-2"><strong>Outlet Name:</strong><br><a href="http://localhost:8000/outlets/1" title="View Casa de Lean Outlet detail">Casa de Lean</a></div><div class="my-2"><strong>Outlet Address:</strong><br>Arcos 3130 7° A, Nuñez, CABA</div><div class="my-2"><strong>Coordinate:</strong><br>-34.55116913874, -58.46255421638</div>",
      +name_link: "<a href="http://localhost:8000/outlets/1" title="View Casa de Lean Outlet detail">Casa de Lean</a>",
    },
    +with: [],
    +additional: [],
  }

