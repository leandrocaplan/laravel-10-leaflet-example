<?php

namespace Routes;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

//Necesitamos importar los Controllers, para migrar a la sintaxis nueva utilizada por Laravel 10
use App\Http\Controllers\OutletController;
use App\Http\Controllers\OutletMapController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Esta línea me genera automáticamente, todas las rutas utilizadas para la autenticación de los usuarios
Auth::routes();


/*
POST            login .................................................... Auth\LoginController@login
POST            logout .......................................... logout › Auth\LoginController@logout
POST            password/email ..... password.email › Auth\ForgotPasswordController@sendResetLinkEmail
GET|HEAD        password/reset .. password.request › Auth\ForgotPasswordController@showLinkRequestForm
POST            password/reset .................. password.update › Auth\ResetPasswordController@reset
GET|HEAD        password/reset/{token} ... password.reset › Auth\ResetPasswordController@showResetForm
GET|HEAD        register ..................... register › Auth\RegisterController@showRegistrationForm
POST            register ............................................ Auth\RegisterController@register

*/



//Si estamos autenticados, esta ruta nos dirije a la vista 'home' (nos dice que ya estamos logueados)
//Si no, nos dirije a la vista de login (al igual que otra ruta definida en api.php)
//Lo único que muestra es un cuadro que indica que nos logueamos correctamente.
Route::get('/home',[HomeController::class, 'index'])->name('home');

//GET|HEAD        home .............. home › HomeController@index




//Aquí definimos las rutas que son manejadas por los métodos de 'OutletMapController'


//Definimos la ruta raíz con método GET, que nos dirije a la vista principal donde se muestran todos los outlets
//registrados en la aplicación (igual que la ruta raíz). Nos permite dibujar un polígono que podemos pasar a la vista del mapa filtrado.
Route::get('/', [OutletMapController::class, 'index']);

//Definimos una ruta 'our_outlets' con método GET, que nos dirije a la vista principal, de igual forma que la ruta raíz (son equivalentes).
Route::get('/our_outlets', [OutletMapController::class, 'index'])->name('outlet_map.index');



//Definimos una ruta 'filter', asociada a una request POST.
//Esta ruta, al recibir en el cuerpo del POST un objeto GeoJSON correspondiente a un polígono, nos
//dirije a la vista filtrada, codificada en el mismo archivo Blade correspondiente a la vista principal pero con un contenido
//diferente, al ser el mismo renderizado condicionalmente en base a los datos que le pasamos desde el controlador.
//Le transferimos a dicha vista, un GeoJSON de tipo FeatureCollection con todos los Outlets localizados dentro del polígono recibido en la Request,
//y otro GeoJSON de tipo Point, donde está definido el centroide de dicho polígono.
Route::post('/filter', [OutletMapController::class, 'filter'])->name('outlet.filter');

//Definimos una ruta 'filter', pero en este casp asociada a una request GET.
//A esta ruta le asignamos el mismo método de controlados que la anterior.
//La única diferencia con aquella es que recibe el GeoJSON dentro del cuerpo de una petición GET en lugar de una POST.
Route::get('/filter', [OutletMapController::class, 'filter'])->name('outlet_map.filter_url');

//Ejemplo de URL para hacer una petición GET a la ruta definida arriba:
/*
http://localhost:8000/filter?json=%7B%22type%22%3A%22Feature%22%2C%22properties%22%3A%7B%22name%22%3A%22zona%22%7D%2C%22geometry%22%3A%7B%22type%22%3A%22Polygon%22%2C%22coordinates%22%3A%5B%5B%5B-58.42048645019532%2C-34.61921693491219%5D%2C%5B-58.388385772705085%2C-34.612011953145455%5D%2C%5B-58.39920043945313%2C-34.58827347289634%5D%2C%5B-58.40915679931641%2C-34.578097761700334%5D%2C%5B-58.421344757080085%2C-34.574281548799924%5D%2C%5B-58.445549011230476%2C-34.5814898038672%5D%2C%5B-58.46065521240235%2C-34.60169785184796%5D%2C%5B-58.4549903869629%2C-34.61554388509679%5D%2C%5B-58.42048645019532%2C-34.61921693491219%5D%5D%5D%7D%7D
*/



//Tenemos entonces, dos rutas diferentes que nos dirigen a la vista 'outlets.filtered' y le pasan la información por ella requerida:
//una que recibe una petición GET, y otra que recibe una petición POST.
//Si bien para ambas rutas definimos la misma URL, estas serán rutas diferentes, al manejar diferentes métodos HTTP  de alguna Request.
//Podremos utilizar cualquiera de las dos, según lo que nos convenga en cierto momento.


//Comentamos las rutas arriva definidas, devueltas por 'pho artisan route:list':

/*
GET|HEAD        / ...................................... OutletMapController@index
GET|HEAD        our_outlets.......... outlet_map.index › OutletMapController@index
POST            filter ................ outlet.filter › OutletMapController@filter
GET|HEAD        filter ........ outlet_map.filter_url › OutletMapController@filter

*/


//Ahora, definiremos las rutas que seran manejadas por los métodos de 'OutletController' (otra clase de controlador, diferente de la anterior).


//Definimos, con 'resource', en una sola línea, todas las rutas siete rutas estándar de la convención de Laravel,
//asociada cada una de ellas a cada método con nombre estandar de Laravel, definidos todos en la clase OutletController.
//Solo podemos hacer esto si respetamos la convenciones de nombres de Laravel, para los siete métodos estandar en un controlador,
//a los cuales a cada uno le asigna manejar a cada una de las siete rutas estandar de la convención de Laravel.

//Si a 'OutletController' le añadimos métodos fuera del estandar, 'resource' seguiría funcionando correctamente, mientras los estándar sí estén
//definidos.

Route::resource('outlets', OutletController::class);


//Comentamos las rutas que nos generó ese 'resource', devueltas por 'pho artisan route:list':

/*
Nos dirije a la vista de Lista de Outlets:
GET|HEAD        outlets ..... outlets.index › OutletController@index             

Recibe un Request POST con la información del formulario definido en la vista 'create', y lo graba en la base de datos:
POST            outlets ..... outlets.store › OutletController@store

A partir del ID pasado como parámetro, muestra la información de un Outlet (lo muestra en un mapa Leaflet):
GET|HEAD        outlets/{outlet} ...... outlets.show › OutletController@show 

Tiene un formulario (integrado con Leaflet) para crear un nuevo Outlet, y mandarlo a 'store':
GET|HEAD        outlets/create ..... outlets.create › OutletController@create

Recibe un Request PUT con la información del formulario definido en la vista 'edit', y lo graba en la base de datos:
PUT|PATCH       outlets/{outlet} ..... outlets.update › OutletController@update

A partir del ID pasado como parámetro, recibiendo un request DELETE, elimina un Outlet de la base de datos:
DELETE          outlets/{outlet} ..... outlets.destroy › OutletController@destroy

A partir del ID pasado como parámetro, muestra un formulario (integrado con Leaflet) para editar un Outlet y mandarlo a 'update':
GET|HEAD        outlets/{outlet}/edit ..... outlets.edit › OutletController@edit
*/

//Esta sentencia por ejemplo, si se ejecutara, asignaría un método definido en el controlador correspondiente, que no
//tenga nombre estandar, para esta ruta que estámos definiendo y que tampoco respeta un nombre estandar de Laravel.
//Vemos que no me altera el funcionamiento de 'resource'.
//Route::get('hola', [OutletController::class,'otra_cosa'])->name('hola');



//Ruta de debug, para obtener la información de la sesión existente en cierto momento, al usar la aplicación (me permite ver el objeto 'session').

//Esta información en ese objeto, tendrá mayores diferencias en su estructura de acuerdo a si hay un usuario logueado en este momento, o no. 
///Lo devuelve en forma de texto con formato JSON, tendrá la información que requerimos del objeto de sesión,
//utilizado por Laravel para almacenar, de alguna forma, el "estado" de la aplicación entre las distintas Request que le lleguen.

//Basicamente, la única información relevante que nos debemos guardar entre distintas Request, es la información que nos resulte
//relevante para un usuario determinado al hacer login en la aplicación, hasta que haga logout o expire el tiempo máximo de la sesión.
//Tendremos algunos atributos que no estarán presentes en el objeto 'session', devuelto al cliente como texto formato JSON,
//cuando en cierto momento no haya ningún usuario logueado, pero que al estar alguno logueado, sí existirán en ese texto JSON
//(podremos saber así si hay o no un usuario logueado en este momento).
//La sesión solo finaliza al hacer logout en el navegador, o al expirar el tiempo de duración máxima de sesión que tengamos definido 
//en nuestro '.env' (en SESSION_LIFETIME, por defecto 120 minutos). En ese '.env' también definimos la forma en que nuestra aplicación
//Laravel, almacenará internamente los datos de sesión (en SESSION_DRIVER).
Route::get('/sesion', function(Request $request){
    $request->session()->put('key', 'value');
    $auth=$request->session()->get('auth');
    $data = $request->session()->all();
    return $data;
})->name('sesion');

//GET|HEAD        sesion ............ sesion
