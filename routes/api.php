<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\OutletApiController;
use App\Http\Controllers\Api\OutletApiController2;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Si estamos autenticados, esta ruta nos dirije a la vista 'home' (nos dice que ya estamos logueados)
//Si no, nos dirije a la vista de login
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//ToDo: Ver bien en detalle el tema de los middlewares

use App\Outlet; //Importamos esta clase para utilizarla en la ruta 'prueba'

/* 
NOTA: Al definir que en nuestra aplicación, nuestras vistas, para obtener el objeto GeoJSON
que necesitan (para codificar información en el mapa Leaflet), no solicitan ahora ese recurso a través de la API (como en versiones previas),
sinó que se los pasamos directamente a Blade mediante el método 'view' desde el controlador,
que se procesará por Blade en un paso previo a que este genere el recurso HTML/JS devuelto al cliente.
Esta modificación incrementó drásticamente la performance de la aplicación, pero sin embargo, ya no podríamos observar la implementación
y el funcionamiento de una API de Laravel, siendo que esta API ahora es prescindible para la utilización de la aplicación por el usuario final.
Sin embargo, para propósitos ilustrativos y de ejemplificación, dejamos definido el enrutamiento e implementación de la API de la misma,
siendo que también, podría ejemplificar una forma adecuada de pasarle los datos necesarios que podría solicitar cierta aplicación externa a la nuestra.
Sería información existente en nuestra base de datos, pero esta misma solo puede ser accedida directamente por nuestra aplicación.
La aplicación externa, a traves de esta API, podrá solicitar solamente cierta información de nuestra base de datos (podemos filtrar
la información que pasemos), y en el formato que definamos (en este caso, pasamos un GeoJSON en formato texto al cliente, o 
a la aplicación externa que lo solicite).


Previamente teníamos aquí implementada la generación de la estructura del objeto GeoJSON de tipo FeatureCollection que le pasabamos a la vista.
Sin embargo, al ser requerido este ahora directamente por el controlador principal, transferimos dicha responsabilidad a una nueva
clase que definimos: 'GeoJSONService'. Este es un servicio que definimos en nuestra app, para que su funcionamiento pueda ser invocado
directamente tanto desde los métodos de controladores de la API, como de los controladores web estandar de la aplicación.
De esta forma, podemos reutilizar código, y dejar definidos de forma mucho mas modularizada, tanto los métodos del controlador estandar
de la aplicación web, como los métodos del controlador de la API.
Lo hacemos así porque se considera mala práctica llamar a los métodos de una instancia de un controlador, desde los métodos de una instancia de
otro controlador. Al haber definido nuestra clase 'GeoJSONService', en el archivo 'AppServiceProvider.php' para este fin,
podremos obtener facilmente, una instancia de esta clase en los métodos de ambos controladores (estandar y API),
utilizando inyección de dependencias (caracteristica fundamental del Service Container de Laravel).
*/

//Todas las rutas definidas dentro de este 'group', estarán definidas en relación al path '/api',
//y sus controladores están definidos con el namespace 'Api'.
Route::group(['as' => 'api.', 'namespace' => 'Api'], function () {
    /*
     * Outlets Endpoints
     */
    
    //Esta es la única ruta hasta ahora que nos devuelve datos relevantes para nuestra aplicación
    //Al ser invocada mediante una petición GET a su URL, nos trae un objeto GeoJSON con los datos
    //de todos los inmuebles registrados actualmente en la app.
    //En este caso, es el mismo que el objeto utilizado por la vista principal.
    Route::get('outlets', [OutletApiController::class, 'index'])->name('outlets.index');

    //Estas dos rutas que definimos a continuación, tendrán la misma URL, y llaman al mismo método 'filtered' del controlador de la API.
    //La diferencia es que una, recibe un GeoJSON mediante una petición POST, y la otra mediante una petición GET (con el JSON definido dentro de
    //la URL, pasado como parámetro).
    //En ambos casos, en estas rutas recibimos el contenido del GeoJSON enviado desde la vista mediante la request, conteniendo un polígono
    //(en formato de texto percent-encoding), y a partir del procesamiento realizado por el método 'filtered' definido en el controlador,
    //devolvemos a la vista un GeoJSON de tipo FeatureCollection que en este caso, contienen solo los Outlets que existan dentro del polígono.
    //En este caso, es el mismo que el objeto utilizado por la vista filtrada.
    Route::post('filtered', [OutletApiController::class, 'filtered'])->name('outlets.filtered');
    Route::get('filtered', [OutletApiController::class, 'filtered'])->name('outlets.filtered_get');
    

    //Ejemplo de URL para hacer una petición GET a la ruta definida arriba con 'Route::get':
    /*
    http://localhost:8000/api/filtered?json=%7B%22type%22%3A%22Feature%22%2C%22properties%22%3A%7B%22name%22%3A%22zona%22%7D%2C%22geometry%22%3A%7B%22type%22%3A%22Polygon%22%2C%22coordinates%22%3A%5B%5B%5B-58.42048645019532%2C-34.61921693491219%5D%2C%5B-58.388385772705085%2C-34.612011953145455%5D%2C%5B-58.39920043945313%2C-34.58827347289634%5D%2C%5B-58.40915679931641%2C-34.578097761700334%5D%2C%5B-58.421344757080085%2C-34.574281548799924%5D%2C%5B-58.445549011230476%2C-34.5814898038672%5D%2C%5B-58.46065521240235%2C-34.60169785184796%5D%2C%5B-58.4549903869629%2C-34.61554388509679%5D%2C%5B-58.42048645019532%2C-34.61921693491219%5D%5D%5D%7D%7D
    */

    //Definimos dos rutas similares a las anteriores, pero nos devolverán aquí un GeoJSON correspondiente a una geometría de
    //tipo Point, con las coordenadas del centroide calculado para el polígono recibido como GeoJSON en la petición.
    Route::post('centroide', [OutletApiController::class, 'centroide'])->name('outlets.centroide');
    Route::get('centroide', [OutletApiController::class, 'centroide'])->name('outlets.centroide_get');
    
    //Ejemplo de URL para hacer una petición GET a la ruta definida arriba con 'Route::get':
    /*
    http://localhost:8000/api/centroide?json=%7B%22type%22%3A%22Feature%22%2C%22properties%22%3A%7B%22name%22%3A%22zona%22%7D%2C%22geometry%22%3A%7B%22type%22%3A%22Polygon%22%2C%22coordinates%22%3A%5B%5B%5B-58.42048645019532%2C-34.61921693491219%5D%2C%5B-58.388385772705085%2C-34.612011953145455%5D%2C%5B-58.39920043945313%2C-34.58827347289634%5D%2C%5B-58.40915679931641%2C-34.578097761700334%5D%2C%5B-58.421344757080085%2C-34.574281548799924%5D%2C%5B-58.445549011230476%2C-34.5814898038672%5D%2C%5B-58.46065521240235%2C-34.60169785184796%5D%2C%5B-58.4549903869629%2C-34.61554388509679%5D%2C%5B-58.42048645019532%2C-34.61921693491219%5D%5D%5D%7D%7D
    */

    //Estas son entonces, por el momento, las rutas mas relevantes definidas para nuestra API, para ser utilizadas por un usuario final.




    
    //Estas otras rutas, al igual que la clase OutletApiController2, que contiene los métodos que manejadores de estas siguientes rutas 
    //definidas, existen solamente para propósitos de debug, y no tienen directamente
    //ninguna aplicación práctica para el correcto funcionamiento de la aplicación para un usuario final (si esta estuviera
    //en el modo release,versión lanzada al público).

    //Sin embargo, dado que este proyecto de ejemplo existe a propósitos explicativos y didácticos, las dejamos definidas para
    //ayudar a dicho propósito (sirve mas bien para el modo debug).
    
    //Esta ruta, nos devolverá una colección  de objetos de tipo Outlet
    //(instancias del modelo ORM correspondiente) devuelta por 'Outlets::all()'.
    //En la Response de este GET, le será devuelto un texto conteniendo un elemento JSON (un array JSON en el mayor jerarquía).
    //Consiste básicamente, en devolver al cliente el resultado de un paso intermedio,
    //del procedimiento que utilizamos para 
    //generar el objeto GeoJSON de tipo FeatureCollection, el que debe ser finalmente procesado en las vistas, del lado del cliente.
    //Nos devuleve lo que tendremos en el atributo "properties" de cada Feature correspondiente a un Outlet dentro de un FeatureCollection.
    Route::get('outlets2', [OutletApiController2::class, 'index'])->name('outlets2.index');
    

    //Esta ruta, nos devolverá una única instancia (pasada como un objeto JSON al lado del cliente), de un objeto de tipo Outlet,
    //devuelto en este caso por 'Outlets::all()->first()' (El que tengamos primero en la tabla 'outlets' de la base de datos).
    Route::get('outlets2/obj', [OutletApiController2::class, 'obj'])->name('outlets2.obj');
    

    //Esta ruta, nos devolverá lo mismo que la ruta '../api/outlets2' definida previamente.
    //Sin embargo, esta ruta lo hace mediante una función Closure muy sencilla, definida aquí mismo, pasada como callback a 'Route::get',
    //en lugar de hacerlo invocando a un método del controlador (el cual devuelve exactamente lo mismo que esta Closure).
    Route::get('prueba', function (){
        return Outlet::all();
    })->name('prueba');

    //Rutas de la API:

    /*
    POST            api/centroide ........................... api.outlets.centroide › Api\OutletApiController@centroide
    GET|HEAD        api/................................. api.outlets.centroide_get › Api\OutletApiController@centroide
    POST            api/filtered .............................. api.outlets.filtered › Api\OutletApiController@filtered
    GET|HEAD        api/filtered .......................... api.outlets.filtered_get › Api\OutletApiController@filtered
    GET|HEAD        api/outlets ..................................... api.outlets.index › Api\OutletApiController@index
    GET|HEAD        api/outlets2 .................................. api.outlets2.index › Api\OutletApiController2@index
    GET|HEAD        api/outlets2/obj .................................. api.outlets2.obj › Api\OutletApiController2@obj
    GET|HEAD        api/prueba ............................................................................. api.prueba

    */
});
