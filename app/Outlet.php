<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

use MatanYadaev\EloquentSpatial\SpatialBuilder;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Traits\HasSpatial;

class Outlet extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     use HasSpatial;

    //Si quisieramos que este modelo, al ser paginado, por defecto obtengamos tres resultados por página
    //protected $perPage=3;
    
    //Estas son las propiedades que recibo desde el form, salvo 'creator_id',  la cual el método 'store' de OutletController
    //genera a partir del ID del usuario autenticado. Se generará la query de SQL 'insert' con los campos aquí definidos.
    protected $fillable = [
        'name', 'address', 'creator_id','coordinates',
        //La latitud y longitud no son columnas de la tabla 'outlets' en nuestra base de datos, por lo que las dejamos comentadas.
 //       'latitude','longitude' 
    ];
    
    //El atributo 'coordinates' lo dejamos como hidden, ya que a la hora de generar el GeoJSON correspondiente a
    //un grupo de Outlets, tendremos la información de las coordenadas en el atributo 'geometry', por
    //lo que veríamos información redundante en el al replicar esta información dentro del campo 'properties' de cada Feature generado,
    //correspondiente a cada Outlet que tengamos.
    protected $hidden=['coordinates'];

    //En nuestro modelo Eloquent, utilizando el plugin Laravel Eloquent Spatial, el atributo 'coordinates', que en
    //nuestra tabla es de tipo 'point' (el cual es soportado por la extensión MySQL Spatial, incorporada en las versiones mas recientes),
    //será aquí modelizado como un objeto de tipo 'Point', que es una clase definida en el plugin mencionado.
    protected $casts = [
        'coordinates' => Point::class
    ];
    /**
     * Array de accesores:
     * Campos que le asignamos accesores para ser devueltos en el JSON generado a partir de una instancia de un modelo.
     * (The accessors to append to the model's array form.)
     *
     * @var array
     */

    //La propiedad 'map_popup_content' NO está en ningún campo de la tabla 'outlets' en la base de datos.
    //Eloquent la generará como atributo de cada instancia de este modelo creada a partir de cada registro,
    //a partir de la información que si está definida en el en la base datos.
    public $appends = [
        //Debo añadir aquí 'latitude' y 'longitude' para que los accesores lo casteen correctamente a float (desde String).
        
        'map_popup_content',
        //'latitude', 'longitude' 
    ];
    
    //Métodos de accesores:

    //Estos dos métodos nos sirven para convertir a tipo 'float' los campos correspondientes a latitud y longitud
    //de un Outlet, codificados en la base como 'decimal(16,13)'.
    //De no implementarlos, nuestro modelo los codificaría como strings.
    //Los dejamos comentados ya que no necesitamos estos atributos en nuestro modelo, al estar los valores de latitud y
    //longitud definidos como sub-atributos del atributo 'coordinates' (de tipo Point)
    
/*
    public function getLatitudeAttribute() 
    {
        return floatval($this->coordinates->latitude);
    }

    public function getLongitudeAttribute() 
    {
        return floatval($this->coordinates->longitude);
    }
*/
    /**
     * Get outlet name_link attribute.
     *
     * @return string
     */
    

    //Genero el atributo 'name_link'. Este estará oculto: no se mostrará por defecto en una instancia de un objeto Outlet,
    //sinó que será reutilizado por otro método de accesor, para generar otro atributo que sí será visible por defecto (map_popup_content).
    public function getNameLinkAttribute() 
    {
        //La función '__(archivo.propiedad)', busca en el directorio '<root>/resources/lang/en', el string que devolverá.
        //Busca el archivo correspondiente y dentro de el, en el array que esté allí definido, la clave
        //que le estemos pasando a la función como argumento luego del punto. Devuelve el valor asociado a esa clave.

        $title = __('app.show_detail_title', [ //$title valdrá: "View <nombre_outlet> Outlet detail"
            'name' => $this->name,
            'type' => __('outlet.outlet'),
        ]);
        //Genero el comienzo del tag <a>. El valor del atributo "href", será lo que me devuelva el método 'route',
        //pasándole como primer parámetro el nombre de la vista 'show', y como segundo parámetro '$this'.
        //Si bien '$this' referencia a este objeto completo del modelo (con todos los campos del presente outlet),
        //al pasarselo como sefundo parámetro a 'route',
        //simplemente añadira el atributo ID asignado al presente registro de outlet, a la URL generada por 'route'.
        $link = '<a href="'.route('outlets.show', $this).'"';  //Un ejemplo de URL devuelta es: 'http://localhost:8000/outlets/100'
        
        //Generamos la segunda parte del tag de apertura <a>, pasandole al atributo 'title' lo que previamente guardamos en '$title'
        $link .= ' title="'.$title.'">'; 
        
        //El texto mostrado del link que estamos generando, será el nombre del Outlet clickeado.
        $link .= $this->name;

        //Añadimos el tag de cierre de <a>
        $link .= '</a>';

        //Devolvemos el string generado, correspondiente al código HTML completo del link
        return $link;
    }

    /**
     * Outlet belongs to User model relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    
    //Si este método se llama 'creator', el 'belongsTo' va a buscar, por defecto, una FK llamada 'creator_id' dentro de los campos de
    //un Outlet, para asociarla con el campo 'id' (la PK) del User asociado.
    public function creator()
    {   
        //Si hago "$outlet->creator;" en una instancia de 'Outlet', obtendré un objeto 'User',
        //con todos los campos asociados al usuario que lo publicó.
        return $this->belongsTo(User::class);
    }

    /**
     * Get outlet coordinate attribute.
     *
     * @return string|null
     */
    
    //Genero un string correspondiente al valor del atributo 'coordinate', que está siendo generado aquí por Eloquent.
    //Su valor es obtenido a partir de los valores
    //de latitud y longitud almacenados en la base de datos, correspondientes al registro de este outlet.
    /*
    public function getCoordinateAttribute()
    {
        if ($this->latitude && $this->longitude) {
            return $this->latitude.', '.$this->longitude;
        }
    }
*/
    /**
     * Get outlet map_popup_content attribute.
     *
     * @return string
     */
    
    //Genero un string correspondiente al valor del atributo 'map_popup_content', que está siendo generado aquí por Eloquent.
    //Su valor es obtenido a partir del atributo oculto 'name_link', que definimos previamente.
    //Contendrá el código HTML que se renderízará en el Popup mostrado al clickear un determinado ícono de marcador,
    //el cual señale un Outlet en el mapa Leaflet mostrado en la vista.
    public function getMapPopupContentAttribute()
    {
        //Este código HTML consiste en tres <div>. Obtiene parte de su texto a partir de lo definido en los archivos de 'language'
        //(mediante la función '__(...)').

        $latitude=$this->coordinates->latitude;
        $longitude=$this->coordinates->longitude;
        $coordinates= $latitude.' , '.$longitude;

        //El primer <div> muestra: "Outlet Name" en negrita, y en el renglón siguiente, el nombre del outlet (sin negrita).
        $mapPopupContent = '<div class="my-2"><strong>'.__('outlet.name').':</strong><br>'.$this->name_link.'</div>';
        
        //El segundo <div> muestra: "Outlet Address" en negrita, y en el renglón siguiente, la dirección del outlet (sin negrita).
        $mapPopupContent .= '<div class="my-2"><strong>'.__('outlet.address').':</strong><br>'.$this->address.'</div>';
        
        //El tercer <div> muestra: "Coordinate" en negrita, y en el renglón siguiente, las coordenadas del outlet (sin negrita).
        $mapPopupContent .= '<div class="my-2"><strong>'.__('outlet.coordinate').':</strong><br>'.$coordinates.'</div>';

        return $mapPopupContent;
    }
}
