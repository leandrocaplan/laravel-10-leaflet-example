<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Outlet extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /*
        error_log('Salida:');
        error_log($request);
        $toArr = parent::toArray($request);
        error_log(gettype($request));
        error_log(gettype($toArr));
        return $toArr;
        */
        return parent::toArray($request);
    }
}
