<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\GeoJSONService;

class OutletMapController extends Controller
{
    /**
     * Show the outlet listing in LeafletJS map.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */

    //NOTA: Vemos aquí que, tanto el método 'index' como el método 'filter', al hacer el 'return', invocan a la misma vista
    //Blade 'outlets.map' mediante el método 'view'.
    //Sin embargo, la respuesta que devuelvan al cliente como HTML estos dos métodos distintos, será diferente.
    //Previamente teníamos una vista Blade 'outlet.filtered', en un archivo '.../filtered.blade.php.
    //Sin embargo, al tener estos dos archivos distintos ('map.blade.php' y 'filtered.blade.php') mucho código JavaScript en común,
    //para evitar tener tanto código redundante definido, decidimos definir todo ese código en un solo archivo Blade ('map.blade.php'),
    //y renderizar condicionalmente (con directivas Blade) el código del recurso devuelto al cliente, de acuerdo a los distintos parámetros que le
    //pasamos desde estos dos métodos distintos del controlador.

    //Se vizualiza mejor el enrutamiento definido para los métodos de este controlador, en el archivo '<root>/routes/web.php'
    //¡OJO! Esta clase controlador es 'OutletMapController', la cual difiere de la clase hermana 'OutletController'
    
    //Este método nos devuelve el HTML correspondiente a la vista del mapa principal.
    //Se ejecutará al hacer una petición GET con el URL de la ruta raíz de la aplicación, o bien de la ruta '/out_outlets'
    //(le asociamos a ambas este mismo método 'index' de este controlador).
    
    //Mediante inyección de dependencias, le pasamos al método una instancia de nuestro 'GeoJSONService' que definimos en la aplicación.
    public function index(Request $request,GeoJSONService $geoJSONService)
    {
        
        //El método 'view' devuelve el renderizado de HTML/JS realizado a partir del archivo de vista de Blade
        //que pasamos como primer argumento. En el array asociativo que pasamos a 'view' como segundo argumento,
        //sus valores contienen información de las variables que tenemos en este método del controlador, mientras
        //que el valor de sus claves será el nombre de la variable recibida por la vista Blade, con la misma información que
        //aquí.

        //Utiliza el método 'allOutlets' del GeoJSONService para generar un array PHP, donde estará codificado el contenido
        //del objeto GeoJSON de tipo FeatureCollection, conteniendo información en este caso de TODOS los Outlets
        //existentes en nuestra aplicación, el cual es requerido por la vista del mapa principal. Se lo pasamos
        //a la vista mediante el array del segundo argumento de 'view'. De estea misma manera, le pasamos un boolean indicando
        //que esta será la vista del mapa principal, para que sea renderizada condicionalmente desde el Blade 'outlets.map', de
        //acuerdo a este valor pasado aquí.
        
        return view('outlets.map',['outletsFeatColl' => $geoJSONService->allOutlets(), 'principal'=>True]);
    }





    //Este método siguiente, nos devuelve la vista del mapa filtrado, cuyo código en el lado del cliente es renderizado condicionalmente,
    //a partir del codigo Blade definido en el mismo archivo Blade con el que se genera el renderizado del mapa principal.
    //Nos permite también, visualizar por consola, el tipo y contenido de la información definida
    //en las variables que usamos en los pasos intermedios en el procedimiento de convertir el texto recibido desde la Request,
    //a traerme un GeoJSON FeatureCollection, con cada Feature correspondiente a un Outlet que pasó el filtro deseado
    //(estar ubicado dentro del polígono definido en la Request).

    //Mediante inyección de dependencias, le pasamos al método una instancia de nuestro 'GeoJSONService' que definimos en la aplicación.
    public function filter(Request $request,GeoJSONService $geoJSONService) {
    
        //En $contenido me guardo el objeto GeoJSON recibido desde el form dinámico de la vista 'map',  codificado como String PHP
        $poligono = $request->input('json'); 
    
        error_log(gettype($poligono)); //string
        error_log($poligono); //{"type":"Feature","properties":....}
    
    
        //Convierto ese string que guardé, con el contenido del GeoJSON, en un Array PHP con 'json_decode'.
        //Me guardo ese array PHP en $arr_polígono.
        $arr_poligono = json_decode($poligono, true);

        error_log(gettype($arr_poligono)); //array
    
        foreach ($arr_poligono as $clave=>$valor){ //type:Feature.  properties:array.  geometry:array.  
            if(! is_array($valor))
                error_log($clave .':'. $valor);
            else
                error_log($clave .':'. gettype($valor));
        }

        //Desde mi instancia de GeoJSONService aquí inyectada, llamo al método 'filteredOutlets' definida en ella.
        //Recibe como argumento, el array PHP compatible con formato GeoJSON que me había guardado, el cual contiene la información del
        //polígono que debemos aplicar como filtro.
        
        //Nos devuelve también un GeoJSON de tipo FeatureCollection, con información de cada Outlet en sus elementos hijos.
        //No obstante, este FeatureCollection no contendrá aquí todos los Outlets existentes en la aplicación, sinó solo aquellos que,
        //devueltos por la query generada y ejecutada dentro de 'filteredOutlets'. Serán los que se
        //encuentren geográficamente ubicados dentro del polígono recibido en el único parámetro de 'filteredOutlets'.
        $outletsFiltrados=$geoJSONService->filteredOutlets($arr_poligono);
        $centroidePoligono=$geoJSONService->centroide($arr_poligono);
        error_log(gettype($centroidePoligono));
        //Vemos aquí una URL de ejemplo que necesitaríamos generar, para llamar a este método desde una peticion GET.
        //Vemos que este 'route', nos genera una URL del tipo 'http://localhost:8000/filtered?json=%7B%22type%22%3A%22Feature%22....'
        //Dicha URL tiene codificado el contenido completo del objeto GeoJSON (como string) en su único parámetro: 'json'.
        error_log(route('outlet_map.filter_url', ['json' => $poligono]));

        //En este caso, volvemos a llamar al método 'view', con 'outlets.map' pasado como primer argumento al igual que
        //en nuestro método 'index' de este controlador, pero el array pasado como segundo argumento, será diferente.
        //En '$outletsFeatColl', recibirá el objeto FeatureCollection devuelto aquí por 'filteredOutlets'.
        //En '$poligono', el GeoJSON correspondiente al polígono que fue utilizado como filtro (para que pueda calcular el centro inicial del mapa).
        //Finalmente, en '$principal' recibe un flag con valor False, que indica que esta llamada NO busca devolver la vista principal,
        //sinó la vista filtrada. Se renderizará el código correspondiente a la vista filtrada definido en el Blade,
        //de acuerdo al valor de ese flag.
        
        return view('outlets.map',['outletsFeatColl' => $outletsFiltrados, 'centroidePoligono' => $centroidePoligono,'principal'=>False]);
       

        //Teníamos en versiones previas, código que fue reemplazado aquí por otro, que hace lo mismo pero de forma mas eficiente.
        //Antes teníamos el código de la vista filtrada, definida en un archivo Blade 'outlets.filtered', en vez de reutilizar 'outlets.map'.
        //Asímismo, las vistas no recibían el FeatureCollection necesario directamente al momento de ser invocadas por 'view' desde
        //aquí en el controlador, sinó mediante una Request a la API, ejecutado luego de que la vista sea renderizada en el cliente.

        //Incluso, teníamos una primera versión donde el método del controlados no devolvía la vista, sinó que nos redirigía a una ruta que recibía
        //el GeoJSON desde una petición GET, para luego redirigirlo a otra ruta, la cual finalmente devolvía la vista correspondiente.
        
        //Teníamos antes así, tres Requests diferentes ejecutadas, mientras que ahora ejecutando una sola logramos el mismo resultado.

    }

    /*
    Le asignaremos en este caso a este método 'filter', dos rutas diferentes definidas enrutamiento de la aplicación,
    las cuales ambas reciben un texto JSON en la Request, el cual debió ser generado desde la vista principal.
    Por ahora, desde esa vista principal, tenemos el único link existente en nuestra aplicación hacia la vista filtrada.

    Esas dos rutas definidas, reciben ese mismo objeto en forma de texto, pero una lo hace al recibirlo mediante una petición POST,
    y la otra al recibirlo mediante una petición GET.
    
    La vista principal lo envía mediante una petición POST, pero al necesitar exactamente el mismo código
    en el método del controlador para procesar ese recurso recibido, indistintamente que haya sido transferido mediante una petición POST o GET,
    simplemente podemos dejar ambas rutas definidas en el enrutamiento (una para cada método HTTP), asociadas a este mismo método del controlador,
    siendo así este código absolutamente reutilizable.

    La principal ventaja de enviar el objeto mediante una petición POST es básicamente la prolijidad de la aplicación,
    ya que el usuario final no tendría que ver, en la barra de direcciones de su navegador, la larguísima y poco estética
    URL que debió generada para hacer la petición GET, en donde estaría codificado el GeoJSON (que contenga un polígono) requerido por esta vista.

    Sin embargo, también asignamos este método para ser ejecutado, a la ruta mencionada que recibe el objeto mediante petición GET,
    dado que por una parte nos toma solo una línea en el archivo de enrutamiento, sirve como buena ejemplificación de reutilización
    de código, y puede también sernos útil, al aceptar manejar ambas peticiones. 
    Por ejemplo, un usuario quiere guardarse en
    un .txt, la URL con ese polígono allí definido utilizado para el filtrado, o bien quiere mandarla por mail.
    Pueden también ocurrir diversas situaciones en que nos convenga o necesitemos, pasarle información a esta vista filtrada
    codificada directamente en la URL (como podría ser al utilizar una API de un servicio externo).
    */
   
}
