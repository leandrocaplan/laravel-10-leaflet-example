<?php

namespace App\Http\Controllers\Api;

use App\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
//use App\Http\Resources\Outlet as OutletResource; (Linea deprecada: no es necesaria para el correcto funcionamiento del controlador)
use App\Services\GeoJSONService;

class OutletApiController extends Controller
{
    /**
     * Get outlet listing on Leaflet JS geoJSON data structure.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Illuminate\Http\JsonResponse
     */
    



    //Devolvemos al cliente un GeoJSON de tipo FeatureCollection, con todos los Outlets registrados en nuestra aplicación, en forma de texto
    public function index(Request $request,GeoJSONService $geoJSONService)
    {
        error_log(get_class($geoJSONService));

        //En el 'return' de este método, invocamos al método 'json', contenido en el objeto 'Response' devuelto por el helper 'response()'.
        //Este método devolverá al cliente el array PHP que le pasemos como argumento, pero codificado en formato de JSON.
        //Le pasamos como argumento el array PHP devuelto por el método 'allOutlets' de nuestra instancia de GeoJSONService.
        return response()->json($geoJSONService->allOutlets());

    }

    //Devolvemos al cliente un GeoJSON de tipo FeatureCollection, con todos los Outlets localizados dentro del polígono recibido
    //en la Request, en forma de texto.
    public function filtered(Request $request,GeoJSONService $geoJSONService)
    {
        error_log("Vista desde Filtered");

        //En '$request->geoJsonPolygon' tengo un array PHP en el cual está codificado un objeto GeoJSON de tipo "Feature",
        //que contiene una geometría de tipo Polygon, recibido en el cuerpo de la petición POST que maneja este método de la API.
        //Me lo guardo en $featurePoligono
        $featurePoligono=$request->json;       
        $featurePoligono=json_decode($featurePoligono, true);
        error_log(get_class($geoJSONService));

        //En el 'return' de este método, invocamos al método 'json', contenido en el objeto 'Response' devuelto por el helper 'response()'.
        //Este método 'json', devolverá al cliente el array PHP que le pasemos como argumento, pero codificado en formato de JSON.
        //Le pasamos como argumento el array PHP devuelto por el método 'filteredOutlets' de nuestra instancia de GeoJSONService,
        //al que le pasamos como argumento '$featurePoligono', con el contenido que le asignamos previamente.
        //Nos devolverá en forma de GeoJSON, un FeatureCollection con todos los Outlets localizados dentro del polígono recibido
        //en la Request.
        return response()->json($geoJSONService->filteredOutlets($featurePoligono));
    }

    //Devolvemos al cliente un GeoJSON de tipo Point, con el centroide del polígono recibido en la Request, en forma de texto.
    public function centroide(Request $request,GeoJSONService $geoJSONService)
    {
        error_log("Vista desde Filtered");

        //En '$request->geoJsonPolygon' tengo un array PHP en el cual está codificado un objeto GeoJSON de tipo "Feature",
        //que contiene una geometría de tipo Polygon, recibido en el cuerpo de la petición POST que maneja este método de la API.
        //Me lo guardo en $featurePoligono
        $featurePoligono=$request->json;       
        $featurePoligono=json_decode($featurePoligono, true);
        error_log(get_class($geoJSONService));
        
        //En el 'return' de este método, invocamos al método 'json', contenido en el objeto 'Response' devuelto por el helper 'response()'.
        //Este método 'json', devolverá al cliente el array PHP que le pasemos como argumento, pero codificado en formato de JSON.
        //Le pasamos como argumento el array PHP devuelto por el método 'filteredOutlets' de nuestra instancia de GeoJSONService,
        //al que le pasamos como argumento '$featurePoligono', con el contenido que le asignamos previamente.
        //Nos devolverá en forma de GeoJSON, una geometría de tipo Point, con el centroide calculado del polígono que recibimos
        //en la Request.
        return response()->json($geoJSONService->centroide($featurePoligono));
    }
}