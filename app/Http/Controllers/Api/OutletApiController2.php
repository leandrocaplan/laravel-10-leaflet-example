<?php

namespace App\Http\Controllers\Api;

use App\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Outlet as OutletResource;
use Illuminate\Http\Resources\Json\JsonResource;

//Incluimos aquí, mucho código que solo nos sirve para debug, para visualizar resultados intermedios
//y resultados de operaciones que fuimos probando

class OutletApiController2 extends Controller
{
   
    public function index(Request $request)
    {
        $outlets = Outlet::all(); //$outlets es un objeto de tipo "Illuminate\Database\Eloquent\Collection"
          //error_log($outlets);
        $ejemplo = new OutletResource($outlets);
        //$ejemplo->toArray("Hola");
        $geoJSONdata = $outlets->map(
            function ($outlet) {
              //error_log($outlet);
                //error_log(gettype($outlet));
                error_log(json_encode($outlet));
                error_log(json_encode(new OutletResource($outlet)));

                //$recurso= new OutletResource($outlet);
            //$GLOBALS['ejemplo'] = new OutletResource($outlet);
            //return new OutletResource($outlet);
            return $outlet;
        });
        //error_log($geoJSONdata);

        $prueba = (object) ["a"=>"b"];
        /*
        error_log($outlets[1]);
        error_log($outlets[1]);
        error_log(gettype($outlets[1] ));
        error_log(gettype($prueba));
        */
        //error_log(json:$prueba);
        //error_log(gettype(new OutletResource("Hola")));
        
        //return $outlets[0];
        //return new OutletResource($outlets[0]);
        //return $ejemplo;
        //return new OutletResource("Hola");
        // return get_class(response()); -> 'Illuminate\Routing\ResponseFactory'
        return $geoJSONdata;
        //return $prueba;

        /*
        return response()->json([
            'type'     => 'FeatureCollection',
            'features' => $geoJSONdata,
        ]);
        */
    }
    public function obj(Request $request)
    {

        return Outlet::all()->first();
        
    }
}
