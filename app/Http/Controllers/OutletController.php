<?php

namespace App\Http\Controllers;

use App\Outlet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use MatanYadaev\EloquentSpatial\Objects\Point;
use App\Http\Controllers\Controller;

//NOTA: En esta clase, respetaremos las convenciones de Laravel para nombrar a los siete métodos estandar que aquí definimos.
//De esta forma, será mucho mas fácil y práctico asociar cada una a cada ruta de la aplicación, que definamos en el archivo de enrutamiento.

//Se vizualizará mejor el enrutamiento definido para los métodos de este controlador, en el archivo '<root>/routes/web.php'

//¡OJO! Esta clase de controlador es 'OutletController', la cual difiere de la clase hermana 'OutletMapController'.
//Si bien podríamos definir todos los métodos existentes en esa clase hermana, en esta misma clase (ademas de definir aquí
//los siete métodos que respeten las convenciones del estandar de Laravel), y tenerlos todos agrupados en esta clase de
//controlador, dejamos dos clases de controlador diferentes implementadas en esta aplicación, sobre todo a efectos ilustrativos,
//y para obtener cierta modularización en nuestro código.


class OutletController extends Controller
{
    /**
     * Display a listing of the outlet.
     *
     * @return \Illuminate\View\View
     */

    //Nos dirije a la vista de Lista de Outlets:
    //GET|HEAD        outlets ..... outlets.index › OutletController@index
    public function index()
    {
        error_log("Prueba controlador");
        
        //El método 'authorize' devuelve una instacia de 'Illuminate\Auth\Access\Response' (un objeto muy simple con unas pocas propiedades)
        //error_log("Authorize: ".get_class($this->authorize('manage_outlet'))); 
        //Gate::authorize('manage_outlet'); --> Equivale a $this->authorize

        //Para debugear, utilizamos el método 'inspect', el cual devuelve un objeto 'Illuminate\Auth\Access\Response',
        //pero a diferencia de 'authorize', no lanza ninguna AuthorizeException si su propiedad "allowed" vale 'false'.
        $response = Gate::inspect('manage_outlet');
 
        
        error_log("Entro al if de inspect");
        error_log(get_class($response)); //Illuminate\Auth\Access\Response.
        error_log($response); //Illuminate\Auth\Access\Response.
        //return $response;
        if ($response->allowed()) {
            error_log($response->message());
            error_log($response->status());
        } else {
            error_log($response->message());
            error_log($response->status());
        }


        //Si el valor de la propiedad "allowed" del objeto 'Illuminate\Auth\Access\Response' devuelto por 'authorize'
        //es 'false', se interrumpe el flujo de la Request, y es lanzada una AuthorizeException.
        //Cuando esto ocurre, al cliente le llega una Response HTTP con código 403, y verá en el navegador el mensaje
        //de error definido para esa eventualidad.
        
        $this->authorize('manage_outlet'); //Funciona igual que 'Gate::allowIf(auth()->check());'



        /*Ejemplo de las propiedades visibles de un objeto de tipo 'Illuminate\Auth\Access\Response':
        
        Con un usuario Logueado:
        {
        "allowed": true,
        "message": null,
        "code": null
        }

        Sin un usuario Logueado:
        {
        "allowed": false,
        "message": "Necesitas estar logueado",
        "code": null
        }
        */

        //Si 'authorize' no arrojó ninguna excepción, continúa el flujo normal del código definido en este método del controlador.

        error_log("Definicion de queries:");
        $outletQuery = Outlet::query(); //Obtengo una instancia de la clase "Illuminate\Database\Eloquent\Builder".
        //var_dump($outletQuery);
        error_log($outletQuery->toSql()); //select * from `outlets`
        
        //En el parámetro 'q' recibido por URL (Ej: 'http://localhost:8000/outlets?q=Lean'), tengo el string que quiero
        //buscar en el nombre de los Outlets.
        $outletQuery->where('name', 'like', '%'.request('q').'%');

        error_log($outletQuery->toSql()); //select * from `outlets` where `name` like ?  


        //En $dbQuery tendré una instancia del DB Query Builder de Laravel (no de Eloquent): "Illuminate\Database\Query\Builder",
        //con toda la información necesaria cargada para ser traducida a una sentencia SQL, para acceder así a la base de datos.
        $dbQuery=$outletQuery->getQuery(); 

        error_log($dbQuery->toSql()); //select * from `outlets` where `name` like ?  
        $bindings = $dbQuery->getBindings();
        
        error_log($bindings[0]);//%Lean%
        error_log(json_encode($dbQuery->bindings)); // {"select":[],"from":[],"join":[],"where":["%Lean%"],"groupBy":[],"having":[],"order":[],"union":[],"unionOrder":[]}
        error_log(json_encode($dbQuery->wheres)); //[{"type":"Basic","column":"name","operator":"like","value":"%Lean%","boolean":"and"}]

        //Una posible consulta real que se terminaría ejecutando sería:
        //" select * from outlets where name like '%Lean%' " (No podremos verla desde aquí directamente)

        //Sin el parámetro 'q' recibido por URL, tendríamos:
        //" select * from outlets where name like '%%' "


        
        //El método 'paginate()' de un Builder de Eloquent, devuelve un objeto instancia de 'Illuminate\Pagination\LengthAwarePaginator'
        //Una instancia de esa clase, es un objeto PHP con una propiedad $items, que es una instancia de
        //'Illuminate\Database\Eloquent\Collection', la cual contiene todas las instancias de un modelo correspondientes a los registros
        //que trae una determinada página, y otras propiedades con información sobre la página actual y las opciones de paginación.

        //Estas propiedades, entre las mas importantes, incluyen:
        /*
            ["perPage":protected]=> int(2) -> Cantidad de resultados de la consulta mostrados por página
            ["currentPage":protected]=> int(1)  -> Número de la página actual
            ["path":protected]=> string(29) "http://localhost:8000/outlets" -> URL base con la que se hace la request del objeto Paginator
            ["query":protected]=> array(0) { }
            ["fragment":protected]=> NULL 
            ["pageName":protected]=> string(6) "pagina"  -> Nombre del parámetro recibido por URL que se utilizará para indicar el número de página deseado a ser devuelto por 'paginator'
            ["onEachSide"]=> int(3)
            ["options":protected]=> array(2) { -> Replica 'path' y 'pageName' ya descriptos
                ["path"]=> string(29) "http://localhost:8000/outlets" 
                ["pageName"]=> string(6) "pagina"
            }
            ["total":protected]=> int(7) -> Total de resultados devueltos por la consulta
            ["lastPage":protected]=> int(4) -> Número de la última página generada (cantidad de páginas generadas)
        */

        //Parámetros: paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
        
        //$perPage -> Cantidad de resultados por página. Si toma su valor por defecto (null), adoptará el valor de la propiedad '$perPage'
        //del modelo asociado, que por defecto es 15

        //$columns-> Columnas de la tabla asociada al modelo que me interesa obtener en la colección devuelta al usuario
        //Por defecto, todas

        //$pageName -> Nombre del parámetro recibido por URL que se utilizará para indicar el número
        //de página deseado a ser devuelto por 'paginator'. Por defecto, 'page'.

        //$page -> Número de página que debe ser generada y devuelta por esta instancia del objeto 'http://localhost:8000/outlets?page=2'.
        //Por defecto, adopta el valor de un parámetro recibido URL, cuyo nombre está definido en '$perPage'.
        //Si este parámetro no está incluido en la URL, por defecto será la página 1.

        //Ejemplo de URL utilizada para solicitar una página:
        //http://localhost:8000/outlets?page=2

        //Un objeto instancia de 'LengthAwarePaginator', tiene métodos 'toArray' y 'toJson' (este último llama al primero),
        //que sirven para que, al devolver un método del controlador o el Closure asociado a una ruta, este objeto,
        //en nuestra response HTTP tendremos directamente un objeto JSON generado a partir de las propiedades del 'LengthAwarePaginator'.

        //Cuando pasamos este objeto a una vista Blade, por el segundo argumento de 'view' o con 'with', en la vista
        //Blade recibiremos la instancia del objeto PHP original de tipo 'LengthAwarePaginator', que puede ser iterado como un array PHP
        //cuyas claves son un número entero que indíca el número del registro devuelto
        //existente en la página representada (de 0 a $perPage-1), y cuyos valores son instancias del modelo Eloquent generado
        //a partir de cada registro de la tabla devuelto por la consulta.

        //Un objeto 'LengthAwarePaginator', ademas, contiene métodos que nos permiten operar mas fácilmente con los resultados
        //de la consulta devuelta que está siendo paginada (como 'firstItem()', que nos devuelve el número de registro total
        //devuelto por la consulta que constituya el primer elemento mostrado en una página determinada).


        //Pido 25 resultados por página a 'paginate':
        $outlets = $outletQuery->paginate(25);

        //Si no le pasámos parámetros a 'paginate', adopta los valores por defecto:
        //Mostraría 15 resultados por página de Outlets, todas sus columnas, el parámetro URL utilizado para
        //indicar el número de página se llamará 'page' y si este no es recibido en la URL, nos devolverá la página 1.
        //$outlets = $outletQuery->paginate();
        
        //Decimos que nos muestre 3 resultados por página, que solo nos traiga las columnas ID y name del modelo Outlet,
        //el parámetro URL utilizado para indicar el número de página es 'pagina',
        //y si este no es recibido en la URL, nos devolverá la página 1. Si como cuarto argumento pasaramos el valor
        //2 (como entero), forzaríamos a la función a que siempre nos devuelva la página 2 independientemente de lo
        //que tengamos en la URL.
        //$outlets = $outletQuery->paginate(3,['id','name'],'pagina');

        //Nuestra URL que debemos utilizar sería:
        //http://localhost:8000/outlets?pagina=2

        
        //get_class($outlets); -> Illuminate\Pagination\LengthAwarePaginator
        //return $outlets; -> En el navegador veríamos un JSON generado a partir de la información del objeto LengthAwarePaginator
        //$salida=(string) var_dump($outlets); -> Utilizamos esto para visualizar el contenido del objeto LengthAwarePaginator
        //error_log($salida);
        
        //Le pasamos a la vista 'index' el objeto LengthAwarePaginator cuando la devolvemos en este método
        return view('outlets.index', compact('outlets'));
    }

    
    
    /**
     * Show the form for creating a new outlet.
     *
     * @return \Illuminate\View\View
     */

    //Tiene un formulario (integrado con Leaflet) para crear un nuevo Outlet, y mandarlo a 'store':
    //GET|HEAD        outlets/create ..... outlets.create › OutletController@create
    public function create()
    {
        $this->authorize('create', new Outlet);

        return view('outlets.create');
    }

    /**
     * Store a newly created outlet in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */

    //Recibe un Request POST con la información del formulario definido en la vista 'create', y lo graba en la base de datos:
    //POST            outlets ..... outlets.store › OutletController@store
    public function store(Request $request)
    {
        $this->authorize('create', new Outlet);

        error_log("Metodo store:");
        error_log(gettype($request->latitude));
        error_log(gettype($request->longitude));
        $newOutlet = $request->validate([
            'name'      => 'required|max:60',
            'address'   => 'nullable|max:255',
            'latitude'  => 'required_with:longitude|max:17',
            'longitude' => 'required_with:latitude|max:17',
        ]); //Devuelve un array. Si no incluyo una clave en el array que pasé como argumento, no la devuelve aunque la reciba del form.
        
        $newOutlet['coordinates'] = new Point((double) $request->latitude, (double) $request->longitude);
        $newOutlet['creator_id'] = auth()->id();

        
        error_log(gettype($newOutlet));
        error_log("Log desde el OutletController");
        foreach ($newOutlet as $clave=>$valor) {
            error_log($clave); //Las claves son los campos que incluí en el array que pasé a 'validate'
            error_log($valor); //Los valores son los que me llegan desde el form, mediante la petición POST

        }
        error_log("Fin de Log desde el OutletController");

        $outlet = Outlet::create($newOutlet);
        //return $outlet;
        return redirect()->route('outlets.show', $outlet);
    }

    //Ejemplo de resultado de los 'error_log' de 'store' al efectuar una Request POST:
    /*
    array.  

   WARN  Log desde el OutletController.  

   WARN  name.  

   WARN  Estacion Retiro.  

   WARN  address.  

   WARN  Libertador Y Ramos Mejía.  

   WARN  latitude.  

   WARN  -34.59251298482.  

   WARN  longitude.  

   WARN  -58.37482452392.  

   WARN  creator_id.  

   WARN  1.  

   WARN  Fin de Log desde el OutletController.  
   */

    /**
     * Display the specified outlet.
     *
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\View\View
     */


    //A partir del ID pasado como parámetro, muestra la información de un Outlet (lo muestra en un mapa Leaflet):
    //GET|HEAD        outlets/{outlet} ...... outlets.show › OutletController@show
    
    //El método 'show' recibe una inyección de dependencia de 'Outlet', correspondiente a la instancia del Outlet
    //cuyo ID es el que pasamos por parámetro a traves de la URL utilizada para llamar a este método del controlador.
    public function show(Outlet $outlet)
    {
        //A la vista 'show' le pasamos dicha instancia de 'Outlet'
        return view('outlets.show', compact('outlet'));
    }

    /**
     * Show the form for editing the specified outlet.
     *
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\View\View
     */

    //A partir del ID pasado como parámetro, muestra un formulario (integrado con Leaflet) para editar un Outlet y mandarlo a 'update':
    //GET|HEAD        outlets/{outlet}/edit ..... outlets.edit › OutletController@edit
    public function edit(Outlet $outlet)
    {
        $this->authorize('update', $outlet);

        return view('outlets.edit', compact('outlet'));
    }

    /**
     * Update the specified outlet in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\Routing\Redirector
     */

    //Recibe un Request PUT con la información del formulario definido en la vista 'edit', y lo graba en la base de datos:
    //PUT|PATCH       outlets/{outlet} ..... outlets.update › OutletController@update
    public function update(Request $request, Outlet $outlet)
    {
        $this->authorize('update', $outlet);

        $outletData = $request->validate([
            'name'      => 'required|max:60',
            'address'   => 'nullable|max:255',
            'latitude'  => 'nullable|required_with:longitude|max:15',
            'longitude' => 'nullable|required_with:latitude|max:15',
        ]);
        $outlet->coordinates = new Point($request->latitude, $request->longitude);
        $outlet->update($outletData);

        return redirect()->route('outlets.show', $outlet);
    }

    /**
     * Remove the specified outlet from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\Routing\Redirector
     */

    //A partir del ID pasado como parámetro, recibiendo un request DELETE, elimina un Outlet de la base de datos:
    //DELETE          outlets/{outlet} ..... outlets.destroy › OutletController@destroy
    public function destroy(Request $request, Outlet $outlet)
    {
        $this->authorize('delete', $outlet);

        $request->validate(['outlet_id' => 'required']);

        if ($request->get('outlet_id') == $outlet->id && $outlet->delete()) {
            return redirect()->route('outlets.index');
        }

        return back();
    }

    //Acá podríamos definir un nuevo método que no se llame como ninguno de los siete nombres estandar de Laravel.
    //Al hacerlo, esto no alteraría el funcionamiento del 'Route::resource(...'OutletController::Class)'  definido en
    //el archivo de enrutamiento 'web.php', siempre y cuando los siete nombres estándares estén.
    //Podriamos asociarle, en el mismo 'web.php', una nueva ruta GET que lo tenga por manejador.
    //De estár este método aquí implementado en este archivo, como también la sentencia correspondiente en nuestro 'web.php'
    //(dejamos la codificación de ambos comentada por si se quiere descomentar y probar), al acceder a '<root>/hola', veríamos
    //en el navegador simplemente el texto "Hola" (de acuerdo a lo definido en el método que comentamos a continuación).
    /*
    public function otra_cosa(){
        return "Hola";
    }
    */
}
