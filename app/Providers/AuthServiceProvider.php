<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Access\Response;

use App\User;
use App\Outlet;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Outlet' => 'App\Policies\OutletPolicy',
        'App\Model'  => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        //registerPolicies() es un método heredado de la clase 'AuthServiceProvider' definida en el framework.
        //Hace un 'foreach' del array 'policies', y dentro de el: 'Gate::policy($model, $policy);' por cada par clave/valor de ese array.
        $this->registerPolicies(); 
       
        error_log("Log desde 'boot' de AuthServiceProvider. Este método se ejecuta ni bien corremos el server\n");

        error_log("Tipo de \$this->policies():");
        error_log(gettype($this->policies()));

        error_log("\n");
        
        //policies() es un método heredado de la clase 'AuthServiceProvider' definida en el framework.
        //Devuelve el array en $this->policies.
        foreach ($this->policies() as $politica) { 
            error_log($politica); //Imprimo solo los valores del array 'policies'

        }

        error_log("\n");
        foreach ($this->policies() as $model => $policy) {
            //error_log($politica);
            error_log($model);  //Imprimo las claves del array 'policies'
            error_log($policy); //Imprimo los valores del array 'policies'
        }

        //Definimos una Gate, que nos permite acceder a la lista de Outlets si y solo si, en este momento hay un usuario logueado.
        Gate::define('manage_outlet', 
            //Este callback se ejecuta al ir a la lista de Outlets, cuando en el método 'index' de OutletController se ejecuta:
            //'$this->authorize('manage_outlet');', que invoca a esta Gate que definimos aquí.

            //Si no pasamos '?User $user' como argumento a este callback, ni siquiera lo ejecutaría, y devolvería 'false'
            //directamente antes de ejecutar los 'error_log'.
            function (?User $user) {            
               
                error_log("Entro al callback de Gate::define\n");
                
                if(auth()->check())
                {
                    error_log("True");
                    return Response::allow();
                }
                else
                {
                    error_log("False");
                    return Response::deny('Necesitas estar logueado');
                    
                }

                //return auth()->check();
            }
        );

        Gate::define('prueba-gate', 
            function () {
                error_log("Definimos una gate en donde corresponde");                        
                return false;
            }
        );
        error_log("Fin de Log desde 'boot' de AuthServiceProvider");
    }
}
