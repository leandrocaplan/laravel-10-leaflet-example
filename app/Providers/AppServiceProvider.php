<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Foundation\Application;
use App\Services\GeoJSONService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        error_log("Log desde 'boot' de AppServiceProvider");
        //Desde cualquier clase que extienda de Illuminate\Support\ServiceProvider, podremos acceder mediante '$this->app'
        //al objeto que es instancia de nuestra aplicación Laravel (tendremos una sola instancia de el a lo largo de toda la aplicación)
        error_log(get_class($this->app));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Registramos nuestro servico GeoJSONService, que contendrá métodos para generar los objetos GeoJSON necesitados por la
        //aplicación, 
        $this->app->singleton(GeoJSONService::class, function (Application $app) {
            return new GeoJSONService();
        });
        error_log("Log desde 'register' de AppServiceProvider");
    }
}
