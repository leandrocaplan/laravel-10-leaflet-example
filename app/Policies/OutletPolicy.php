<?php

namespace App\Policies;

use App\User;
use App\Outlet;
use Illuminate\Auth\Access\HandlesAuthorization;

class OutletPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the outlet.
     *
     * @param  \App\User  $user
     * @param  \App\Outlet  $outlet
     * @return mixed
     */
    public function view(User $user, Outlet $outlet)
    {
        // Update $user authorization to view $outlet here.
        return true;
    }

    /**
     * Determine whether the user can create outlet.
     *
     * @param  \App\User  $user
     * @param  \App\Outlet  $outlet
     * @return mixed
     */

    //SOLO se ejecuta este método si hay algun usuario logueado
    public function create(User $user)
    {
        // Update $user authorization to create $outlet here.
        error_log("Log desde OutletPolicy:");
        error_log("Nombre de usuario:");
        //error_log($user->name);
        error_log("Nombre de outlet:");
        //error_log($outlet->name);
        error_log("Fin de Log desde OutletPolicy:");
        return true;
    }

    //Si hubieramos hecho en la firma del método anterior, en cambio :  
    //public function create(?User $user, Outlet $outlet) -> (con el caracter '?' antes de 'User')
    
    //O bien:
    //public function create(User $user=null) -> 'null' como valor por defecto
    
    //El método 'create' siempre se ejecutaría y devolvería 'true', haya o no un usuario logueado en este momento.

    //Antes teníamos la firma:
    //public function create(User $user, Outlet $outlet)

    //Esto funcionaba, pero requería crear una instancia de 'Outlet' solo para convalidar
    //la autorización, por lo que eliminamos el segundo argumento, ya que es innecesario e ineficiente.
    
    //Donde llamamos al método 'can' para evaluar esta autorización, le pasamos como segundo argumento solo el
    //nombre de la clase del modelo asociado a esta Policy (un string), en lugar de pasar un objeto instancia de ese modelo.

    /**
     * Determine whether the user can update the outlet.
     *
     * @param  \App\User  $user
     * @param  \App\Outlet  $outlet
     * @return mixed
     */
    public function update(User $user, Outlet $outlet)
    {
        // Update $user authorization to update $outlet here.
        return true;
    }

    /**
     * Determine whether the user can delete the outlet.
     *
     * @param  \App\User  $user
     * @param  \App\Outlet  $outlet
     * @return mixed
     */
    public function delete(User $user, Outlet $outlet)
    {
        // Update $user authorization to delete $outlet here.
        return true;
    }
}
