<?php

namespace App\Services;
use App\Outlet;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use Illuminate\Support\Facades\DB;

//Definimos esta clase, utilizada tanto por la API como por nuestros otros controladores, para generar los objetos GeoJSON
//con toda la información geoespacial pertinente nuestra aplicación Laravel, utilizados tanto por las vistas en Leaflet como por el resto
//de los módulos de la misma correspondientes al backend.
class GeoJSONService 
{

    

    //Genero un método que me convierta una instancia de modelo Outlet, a un array PHP con los datos correspondientes a
    //un GeoJSON de tipo Feature, en el que su atributo 'geometry' contiene los datos espaciales de su ubicación geográfica.
    private function modelToFeature(Outlet $outlet){
        error_log($outlet);

        return [
            //A la clave 'type' le asignamos el valor 'Feature' (indicando el tipo de objeto GeoJSON).
            'type'       => 'Feature', 

            //A la clave 'properties', le asignamos la instancia del array generado a partir del objeto de modelo 'Outlet',
            //procesado en este momento, cuando este método sea invocado para procesar cada una de las instancias deseadas de ese modelo.
            //Recordemos que en el modelo Outlet, tendremos el atributo 'coordinates' definido como oculto.
            //Por lo tanto, a la hora generar un texto JSON, o bien un array PHP, a partir de alguna instancia de modelo Outlet,
            //la información contenida en ese atributo 'coordinates' no será visible en ese resultado.
            //ya que luego esa información será
            //visible en el atributo 'geometry' del GeoJSON que estamos aquí generando.
            //Esta clave tendrá asignada un objeto JSON, anidado en el GeoJSON completo de tipo Feature que finalmente
            //será devuelto por este método del controlador, contenido dentro de un GeoJSON padre tipo FeatureCollection.
            'properties' =>  $outlet->toArray(),//$this->mymapper($outlet),

            //Si hicieramos:
            //'properties' =>  $outlet,

            //Obtendríamos el mismo resultado final, pero en cada array PHP devuelto correspondiente a una Feature,
            //tendríamos una instancia del modelo Outlet completo asignado a la clave 'properties', en lugar de otro
            //array PHP anidado. Esta instancia de modelo, contendría la información del campo 'coordinates' de cada
            //registro de la tabla. Al estar dicho campo definido como 'hidden' en nuestro modelo Outlet, al generarse
            //el un JSON en forma de texto desde una instancia del mismo, esos datos no terminarían allí definidos.

            //Sin embargo, podemos filtrarlos desde este punto del proceso de generación del GeoJSON devuelto al cliente,
            //utilizando el método 'toArray' para quedarnos con los campos pertinentes al atributo 'properties' de cada Feature que deseamos
            //obtener en el GeoJSON final generado (de tipo FeatureCollection).
            



            //A la clave 'geometry' le asignamos como valor la instancia de 'MatanYadaev\EloquentSpatial\Objects\Point',
            //donde estén alli codificadas las coordenadas geográficas del inmueble ahora procesado,
            //contenidas en el atributo oculto 'coordinates' de cada instancia del modelo Outlet.
            
            
            //Dado que el atributo 'coordinates' en nuestro modelo Outlet, está definido como 'hidden', su contenido no será incluido directamente
            //ni en el texto con formato JSON, ni en un array PHP, generados directamente a partir una instancia de un modelo Outlet.
            
            //Sin embargo, su información sí debe estar incluida dentro de cada objeto GeoJSON de tipo Feature que contenga los datos
            //de cada Outlet definido en el. Por defecto, según lo definido en el plugin 'MatanYadaev\EloquentSpatial' que utilizamos,
            //al convertir una instancia de objeto de tipo 'Point' (o cualquier otra geometría) a texto JSON o a un array PHP,
            //este resultado respetará el formato GeoJSON.
            //Por lo tanto, cuando el FeatureCollection completo que generemos en forma de array PHP, conteniendo la información
            //de todos los Outlets requeridos, sea convertido a texto JSON para ser luego parseado en JavaScript, o bien
            //devuelto por la API en forma de texto, la información correspondiente a su ubicación geográfica estará aquí incluida
            //en este atributo 'geometry', con el formato correcto, y no estará incluida en el atributo 'properties'.
            //Evitamos así, incluir información redundante en el recurso devuelto al cliente.
            'geometry'   => $outlet->coordinates
            //Podríamos hacer, con el mismo resultado final:
            //'geometry'   => $outlet->coordinates->toArray()

            /*
                > $outlet->coordinates;
                = MatanYadaev\EloquentSpatial\Objects\Point {#4909
                    +srid: 0,
                    +latitude: -34.55205243729,
                    +longitude: -58.4618461132,
                }

                > $outlet->coordinates->toJson();
                = "{"type":"Point","coordinates":[-58.4618461132,-34.55205243729]}"

                > $outlet->coordinates->toArray();
                = [
                    "type" => "Point",
                    "coordinates" => [
                    -58.4618461132,
                    -34.55205243729,
                    ],
                ]


            */
        ];
    }

    //Recibo una colección (Eloquent Collection) de instancias de mi modelo Outlet.
    //Devuelvo un array PHP correspondiente a un GeoJSON de tipo FeatureCollection, con los datos de cada
    //Outlet existente en la colección recibida por parámetro, definidos en cada uno de sus elementos Feature hijos.
    private function collectionToFeatureCollection(Collection $outlets){
        
        error_log("collectionToFeatureCollection");
        error_log(get_class($outlets->first())); //App\Outlet.

        //Aplico el método 'map' a la colección recibida, y me guardo lo que este devuelva en '$geoJSONdata'.
        //En cada invocación del callback por 'map' (una por cada outlet registrado), '$outlet' contendrá cada
        //objeto instancia del modelo 'Outlet' correspondiente a cada registro de la tabla mencionada, y devolverá
        //un array PHP conteniendo un objeto GeoJSON de tipo Feature con los datos de cada Outlet.
        $geoJSONdata = $outlets->map ( fn($outlet)=>  $this->modelToFeature($outlet) );


        error_log(get_class($geoJSONdata)); // 'Illuminate\Support\Collection'
        error_log(gettype($geoJSONdata->first()));//array

        //Si no aplicaramos 'toArray()' a cada instancia de Outlet en el método 'modelToFeature', obtendríamos:
        //error_log(get_class($geoJSONdata->first()['properties'])); // App\Outlet
        //error_log(get_class($geoJSONdata->first()['properties']->coordinates)); //MatanYadaev\EloquentSpatial\Objects\Point

        //(El valor del atributo 'coordinates' aún estaría disponible en las properties de cada Feature, siendo filtrados esos datos
        //recién cuando sea generado el texto JSON a partir del array PHP aquí devuelto, por ejemplo con 'json_encode' o 'toJson').

        error_log(get_class($geoJSONdata->first()['geometry']));//MatanYadaev\EloquentSpatial\Objects\Point
        

        //En $geoJSONdata tendremos una Collection de Laravel, con un un objeto GeoJSON de tipo Feature en cada uno de sus elementos,
        //con información de cada Outlet.
        //Una Collection de Eloquent tiene caracteristicas similares y prácticamente plena compatibilidad con un array PHP,
        //pero actua como una clase wrapper de el, proveyendo ciertos métodos para facilitar la obtención de información contenida en el.

        //Una vez que tenemos nuestra colección de outlets guardada en $geoJSONdata, la empaquetamos en el array PHP con el contenido
        //del GeoJSON de tipo FeatureCollection que aquí devolvemos (con solo dos pares clave/valor):
        return  [               
            'type'     => 'FeatureCollection', //A la clave 'type' le asignamos el valor 'FeatureCollection' (el tipo de objeto GeoJSON devuelto)
            'features' => $geoJSONdata, //A la clave 'features' le asignamos la colección que generamos y guardamos antes en '$geoJSONdata'.

            //Podriamos hacer, con el mismo resultado final:
            //'features' => $geoJSONdata->toArray()
        ];


        //Hecho esto, hemos devuelto un array PHP conteniendo un GeoJSON, listo para ser procesado tanto por Leaflet como por cualquier
        //herramienta compatible con este formato.
        //Tendremos entonces, en total cuatro niveles de anidamiento en el objeto GeoJSON final devuelto aquí como array PHP.
    }
    
    //Este método, devuelve un objeto GeoJSON de tipo FeatureCollection en formato de array PHP, con la información de todos los Outlets
    //registrados en la base de datos de nuestra aplicación.
    //Es el recurso requerido por Leaflet en nuestra vista principal, para procesar y generar el contenido visual del mapa allí implementado.
    public function allOutlets()
    {
        //Obtengo un objeto 'Illuminate\Database\Eloquent\Collection', una colección Laravel (wrapper) que es iterable como un array PHP.
        //Contendrá todas las instancias de objetos de tipo 'App\Outlet', cada una de las cuales es generada
        //a partir de cada registro de nuestra tabla de outlets, en nuestra base de datos.
        $outlets = Outlet::all();
        
        error_log("allOutlets");
        error_log($outlets);
        

        //Si no aplicaramos 'toArray' al definir el campo 'properties' en el método 'modelToFeature', podríamos hacer:
        /*
        error_log($this
                    ->collectionToFeatureCollection($outlets)
                    ['features']
                    ->first()['properties']
                    ->coordinates
                    ->toJson());//{"type":"Point","coordinates":[-58.4618461132,-34.55205243729]}
        */

        //Vemos que aún, en el array que devolveríamos aquí en ese caso,
        //estaría definida la información correspondiente a las coordenadas de cada Outlet
        //dentro de las 'properties' del Feature correspondiente, y esta sería recien filtrada del GeoJSON final en una instancia posterior.
        return  $this->collectionToFeatureCollection($outlets);
    }
        
        
    //Este método, recibe un array PHP, en el que estará codificado un GeoJSON de tipo Feature, en el que tendremos definido un polígono.
    //Devuelve un objeto GeoJSON de tipo FeatureCollection en formato de array PHP, con la información de los Outlets
    //registrados en la base de datos de nuestra aplicación, localizados dentro de dicho polígono recibido.
    //Es el recurso requerido por Leaflet en nuestra vista filtrada, para procesar y generar el contenido visual del mapa allí implementado.
    public function filteredOutlets(array $featurePoligono)
    {
        error_log("Vista desde Filtered");
        
        error_log(json_encode($featurePoligono)); //{"type":"Feature" ...}
        error_log(gettype($featurePoligono)); //array
        error_log(gettype($featurePoligono['geometry'])); //array
        error_log(json_encode($featurePoligono['geometry'])); //{"type":"Polygon" ...}
    
        
        //A partir del atributo 'geometry' del GeoJSON recibido, que contiene otro array PHP anidado con un objeto
        //GeoJSON de tipo "Polygon", hijo del GeoJSON de tipo "Feature" recibido, genero un objeto instancia de Polygon,
        //que es una clase definida por el plugín "Laravel Eloquent Spatial" para modelizar en el ORM, campos con dicho tipo de dato
        //existente en MySQL Spatial.
        $poligono=Polygon::fromArray($featurePoligono['geometry']);

        error_log("Clase: " . get_class($poligono)); //MatanYadaev\EloquentSpatial\Objects\Polygon. 
        error_log("Objeto: " . json_encode($poligono)); //Me muestra el objeto GeoJSON a partir del cual esta instancia de Polygon fue generada
        
 

        //Genero una instancia del Query Builder de Eloquent, asociada a mi modelo Outlet.
        $builderEloquent= Outlet::query() 

            //Le encadeno el método 'whereWithin', para generar la consulta que deseo.
            //Consulto si el atributo 'coordinates' de mi modelo Outlet, cuyo tipo de dato es una geometría de tipo punto,
            //se encuentra dentro del objeto Polygon que le paso a 'whereWithin' como segundo parámetro.
            ->whereWithin('coordinates', $poligono); 
        
        error_log(get_class($builderEloquent)); //MatanYadaev\EloquentSpatial\SpatialBuilder

        //Muestro el texto de la consulta en SQL, que nuestra aplicación ejecutará de fondo para comunicarse con la base de datos.
        error_log($builderEloquent->toSql());


        /*  
            select * from `outlets`
            where ST_WITHIN(
                `coordinates`,
                ST_GeomFromText('POLYGON(
                                (-58.510694503784 -34.666528226566,
                                -58.518934249878 -34.650148864469,
                                -58.540735244751 -34.651419792881,
                                -58.547773361206 -34.669210744558,
                                -58.526659011841 -34.679657618235,
                                -58.5120677948 -34.672881417894,
                                -58.510694503784 -34.666528226566)
                                )', 
                    0)
            )
        */

        //Ejecuto la consulta, que me trae una colección de todos los Outlets cuyas coordenadas se encuentren dentro del polígono recibido.

        //Obtengo un objeto 'Illuminate\Database\Eloquent\Collection', una colección Laravel (wrapper) que es iterable como un array PHP.
        //Contendrá todas las instancias de objetos de tipo 'App\Outlet', cada una de las cuales es generada
        //a partir de cada registro de nuestra tabla de outlets, en nuestra base de datos, devuelto por la consulta.
        $outlets=$builderEloquent->get();


        error_log(get_class($outlets)); //'Illuminate\Database\Eloquent\Collection'
        
        //Aplico el método 'map' a esa colección que obtuve, y me guardo lo que este devuelva en '$geoJSONdata'.
        return  $this->collectionToFeatureCollection($outlets);
        //return get_class($geoJSONdata); -> 'Illuminate\Support\Collection'

        //En $geoJSONdata tendremos un objeto GeoJSON, pero codificado como una Collection de Laravel en lugar
        //de direactamente como un array PHP (pero con caracteristicas similares y prácticamente plena compatibilidad con ese formato).

        //Una vez que tenemos nuestra colección de outlets guardada en $geoJSONdata, la empaquetamos en el JSON que será
        //devuelto al cliente por esta API, mediante este método del controlador de la API.

        //En el 'return' de este método, devolvemos un array PHP con dos pares clave/valor, con una estructura compatible con el formato
        //GeoJSON. Tendremos entonces, en total cuatro niveles de anidamiento en el objeto JSON final devuelto aquí por la API.

        //Hecho esto, hemos devuelto un array PHP conteniendo un GeoJSON, listo para ser procesado tanto por Leaflet como por cualquier
        //herramienta compatible con este formato.
    }


    //Podemos obtener el centroide del polígono recibido en una Request directamente del lado del servidor, utilizando funciones de MySQL Spatial.
    //Hacemos esto en lugar de calcularlo del lado del cliente con Turf JS, como hacíamos en versiones anteriores.
    //Devolvemos en este método, un array PHP con un GeoJSON correspondiente al centroide de un polígono,
    //siendo este último también recibido como un array PHP correspondiente a un GeoJSON.
    public function centroide(array $featurePoligono){

        error_log(json_encode($featurePoligono)); //{"type":"Feature" ...}
        error_log(json_encode($featurePoligono['geometry'])); //{"type":"Polygon" ...}
        
        //Debido a las limitaciones del plugin EloquentSpatial, debemos generar y ejecutar manualmente una query SQL cruda desde aquí:
        $dbQuery=DB::select('SELECT ST_AsGeoJSON(ST_Centroid(ST_GeomFromGeoJSON(\''. json_encode($featurePoligono) .'\')))');
       
        $centroide=array_values((array)$dbQuery[0])[0]; //En $centroide nos queda un GeoJSON como string
       
        error_log($centroide); //{"type": "Point", "coordinates": [-58.424712150445174, -34.605689506111084]}
        error_log(gettype($centroide)); //string
        error_log(get_class(json_decode($centroide))); //stdClass
        
        //Devolvemos el GeoJSON como array PHP.
        return (array)json_decode($centroide);
    }
}