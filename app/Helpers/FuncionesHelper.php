<?php


//Para agilizar las conversiones de tipos de datos, entre los utilizados por PHP y los utilizados por JavaScript,
//definimos algunas funciones globales para la aplicación, de utilidad para tal propósito.

//Podríamos considerar también, implementar:
//https://github.com/laracasts/PHP-Vars-To-Js-Transformer

//Esta función, convierte un array con multiples niveles de anidamiento, en un string, y luego lo devuelve.
function recursive_implode(array $array, $glue = ',', $include_keys = true, $trim_all = true)
{
	$glued_string = '';

	// Recursively iterates array and adds key/value to glued string
	array_walk_recursive($array, function($value, $key) use ($glue, $include_keys, &$glued_string)
	{
		$include_keys and $glued_string .= $key.$glue;
		$glued_string .= $value.$glue;
	});

	// Removes last $glue from string
	strlen($glue) > 0 and $glued_string = substr($glued_string, 0, -strlen($glue));

	// Trim ALL whitespace
	$trim_all and $glued_string = preg_replace("/(\s)/ixsm", '', $glued_string);

	return (string) $glued_string;
}


//Definimos una adaptación del 'console_log' de JavaScript para utilizarlo con PHP.


//DEPRECADO:
//Utilizabamos esta función para ver por la consola del navegador, el contenido de una varaible PHP, si es que
//esta es de un tipo primitivo, o bien un array que contiene tipos primitivos en múltiples niveles de anidamiento.
//NO soporta variables que referencien objetos, ni arrays que contengan objetos (incluidos Closures).

//Debido a que, si se utiliza dentro de una vista Blade, en la sección delimitada entre los tags <script>
//donde codificamos todo nuestro código JavaScript, nos quedarán otros tags <script> en esa sección, lo
//cual no es correcto. La podríamos utilizar sin embargo, en secciones de un documento Blade fuera de los tags <script>,
//pero no suele ser una práctica habitual.
function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = recursive_implode($output);

    echo "<script>console.log('PHP Log: " . $output . "' );</script>\n";
}


//Utilizamos esta función para ver por la consola del navegador, el contenido de una varaible PHP, si es que
//esta es de un tipo primitivo, o bien si es un array PHP que contiene tipos primitivos en múltiples niveles de anidamiento.
//NO soporta variables que referencien objetos, ni arrays que contengan objetos en sus atributos (incluidos Closures).
function debug_to_console_view($data) {
    echo "console.log('PHP Log:');";
    $output = $data;
    if (is_array($output))
        $output = recursive_implode($output);

    echo "console.log('".$output."')";
}

//Convertimos un objeto JSON codificado como un array PHP, en un string que es legible por JavaScript
//Lo utilizamos como paso intermedio en la conversión de un array PHP en un objeto JavaScript con el contenido del JSON.
//No podemos, desde una función PHP, devolver directamente un objeto JavaScript, ya que sus tipos son incompatibles entre sí.
function php_array_to_js_string($data) {

	//¡OJO! Debemos tomar en cuenta que el método JSON.parse de JavaScript, requiere tener en el string JavaScript que recibe como
	//argumento para ser parseado a un objeto JavaScript, un doble caracter de escape antes de las comillas dobles,
	//dentro de los campos de tipo String del objeto JSON leído. De otra forma, interpretaría las comillas dobles como
	//cierre de esa propiedad de tipo String, en lugar de como un caracter que se encuentra dentro del texto contenido en el String.
	//Esto se debe a que ocurre un doble parseo: 
	//Primero, al convertir el array PHP a string PHP con 'json_encode', las comillas dobles, al estar antecedidas
	//por el caracter de escape '\', se leen como un caracter dentro del string, y no como su delimitador.
	//Luego, posteriormente a ser convertido ese string PHP a un string JavaScript, en nuestro código JavaScript utilizamos el
	//método JSON.parse, para convertir el string resultante en un objeto JavaScript. JSON.parse de JavaScript, nos presenta
	//la misma regla que json_encode. Por lo tanto, cada caracter de comillas dobles que tenga presente en algún campo de tipo
	//string de nuestro array PHP original, necesita tener un doble caracter de escape en el proceso de ser convertido
	//de un array PHP a un objeto JavaScript por la cadena de métodos que aquí utilizamos.

	//Resolvemos esto con las siguientes dos líneas:
    $output = json_encode($data, JSON_UNESCAPED_UNICODE);
	$output = str_replace('"', '\\"', $output);

	//En https://stackoverflow.com/questions/29416529/json-parse-double-quotes-in-string-literal
	//tenemos la explicación de este comportamiento.
    echo  "'".$output."'";
}

/**
 * Como cuestión a resaltar:
 * 
 * La cadena de procedimentos utilizada para convertir un array PHP a un objeto JavaScript se asimilará a:
 * 
 * <$arrayPhp>  --> json_encode(...) -->  php_array_to_js_string (...) (desde Blade) ->  JSON.parse(...)
 * 
 * La cadena inversa sería de la forma:
 * 
 * const objJs= {...} --> JSON.stringify(...) --> Envio de String por peticion HTTP --> Al ser recibida por el controlador: json_decode(..)
 * 
 * 
 * Como podemos ver, tanto PHP como JavaScript tienen este par de funciones complementarias, necesarias desde ambos ámbitos
 * para que puedan de esta forma intercambiar información entre sí.
 * 
 * Estas funciones aquí definidas, funcionarán como una suerte de interfaz entre lenguajes.
 */

