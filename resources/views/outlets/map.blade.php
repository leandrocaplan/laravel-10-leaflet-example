{{-- 
    Aquí está codificada tanto la vista principal de nuestra apliciación de Laravel con Leaflet, la cual se mostrará ni bien entremos a la
    URL raíz de la aplicación, como la vista filtrada.

    Utilizando renderización condicional por medio de directivas Blade, el código HTML/JS devuelto al cliente no será siempre
    el mismo, sinó que será diferente según el contenido de los datos que le pasemos a esta vista Blade cuando la invocamos desde
    algún método del controlador de la aplicación.

    
    En el código definido en este archivo, utilzamos algunas funciones helper definidas por nosotros,
    para agilizar el intercambio de datos desde un formato compatible con PHP, a uno compatible con JavaScript.

    En nuestro archivo "composer.json" en el directorio raíz de nuestra aplicación, en su campo "autoload" -> "files",
    especificamos que tendremos dichas funciones definidas en el archivo "<root>/app/Helpers/FuncionesHelper.php".

    Recomendamos visualizar el contenido de dicho archivo para ver la documentación de dichas funciones, para comprender
    mejor su utilización desde esta vista.

    En versiones anteriores, aquí hacíamos uso de la librería Turf.js para agilizar las operaciones necesarias con objetos GeoJSON.
    Sin embargo, al lograr implementar todas estas operaciones directamente con métodos de Leaflet en el lado del cliente,
    y funciones de MySQL Spatial en el lado del servidor, podremos prescindir de la misma, optimizando así en buena medida la performance
    de la aplicación al reducir el volumen de recursos externos que deben ser cargados por la misma.
    


    Al ser invocada como vista principal:

    Mostrará un mapa Leaflet, con toda la información geográfica relevante que tengamos cargada en nuestra aplicación, plasmada en el.
    Particularmente, mostrando en dicho mapa, íconos de marcadores que señalen las ubicación geográfica de cada inmueble registrado en
    la aplicación, junto con el resto la información asociada cada uno. Esta última se verá en un recuadro (Popup), que aparecerá
    en el mapa cuando clickeemos el ícono de marcador correspondiente al inmueble representado por tal ícono.
    
    También, de estar algún usuario logueado en la aplicación en el momento, esta vista le facilitará ese usuario
    el registro de un nuevo inmueble, proveyendole un atajo a la vista de registro de un nuevo inmueble.
    Este atajo se mostrará cuando este usuario haga click en cierto punto del mapa, correspondiente a la ubicación
    en donde desee registrar el nuevo inmueble (la vista 'create'). Al acceder a esa vista desde este atajo, se 
    inicializará el formulario existente en ella con los valores de coordenadas (latitud y longiutd), correspondientes
    al punto aquí clickeado.

    Como funcionalidad adicional, haremos uso del plugín Leaflet Draw.
    Esta herramienta, nos permitirá dibujar polígonos en el mapa, que pueden ser fácilmente convertidos a formato GeoJSON.
    Utilizandola, podremos entonces realizar un proceso de filtrado, donde dado un polígono que hayamos dibujado en esta
    vista, podremos acceder a otra vista en donde solo se muestren los inmuebles que se encuentren dentro del perímetro
    que definimos aquí.
    




    Al ser invocada como vista filtrada:
    
    Recibe y muestra solamente los datos de los inmuebles contenidos en el polígono que hayamos dibujado desde la vista principal,
    luego de haber sido filtrados mediante las funcionalidades provistas por MySQL Spatial.
    Ademas, el centro inicial del mapa aquí mostrado, se corresponderá con el centroide calculado de dicho polígono, también mediante
    las funcionalidades de MySQL Spatial.
    Esta vista no permitirá la utilización de las herramientas de Leaflet Draw, estando estas solamente disponibles para la vista principal.

    



    Debemos tomar en cuenta que el principal propósito de este proyecto, tiene que ver con fines didácticos, ejemplificando
    la utilización combinada de las herramientas Leaflet y Laravel. Por lo tanto, es tan importante la documentación incluida
    en el, como la codificación de las funcionalidades que implementemos efectivamente a través de su código.
    Por lo tanto, en este mismo archivo Blade, ademas de implementar todas las funcionalidades que corresponden a esta vista,
    es donde tendremos definida la mayor parte de la documentación correspondiente a la utilización básica de Leaflet,
    así como la descripción de los principios básicos de su funcionamiento.
    El código correspondiente a la utilización de Leaflet está fundamentalmente implementado en lenguaje JavaScript.
    
    Debemos tomar en cuenta que, dentro de un archivo Blade, tendremos código definido con una sintaxis que es comprensible
    especificamente por el motor de vistas Blade de Laravel (practicamente un lenguaje propio). Dicho código, en el contexto
    de una aplicación Laravel, será traducido a código PHP puro, en un paso previo a la renderización del código devuelto
    al lado del cliente (consistente únicamente en HTML, JavaScript y CSS), realizada por nuestro servidor web
    (en donde estemos ejecutando nuestra aplicación Laravel).
    
    Tendremos, en el código de este archivo que utiliza la sintaxis mencionada, varias directivas Blade.
    Estas son similares a las annotations de PHP (líneas que comienzan con el caracter '@'),
    y serán comprendidas por el motor de vistas Blade de Laravel, para implementar así el comportamiento deseado
    de la vista, a menudo relacionando o fusionando código definido en diferentes archivos Blade para renderizar
    el código de la vista devuelta al cliente.

    Ademas, tendremos sentencias con código PHP incluidas dentro de los delimitadores de llave doble '{{' y '}}'.
    Lo que tengamos dentro de esos delimitadores, suele ser un valor de tipo string o numérico, ya sea definido en forma literal,
    o bien siendo el resultado devuelto por la invocación de algún método PHP.
    (puede ser algún método de PHP puro, o bien uno especifico de Laravel).
    Al ser renderizada la vista, lo que tengamos definido entre esos delimitadores se traducirá, en un primer paso,
    a código PHP puro, generalmente utilizando la sentencia 'echo' y la función 'e', (para generar entidades HTML),
    a la que se pasa como argumento lo que estaba dentro de los delimitadores.
    Finalmente, en el codigo devuelto al cliente, tendremos el resultado de ese 'echo', con el valor de texto deseado
    en ella.

    Por ejemplo, podemos tener un literal entre los delimitadores, que se procesa de la forma:


    <title>{{"Hola"}}</title>                                           -> Linea de código definida en nuestro archivo Blade
    <title><?php echo e("Hola"); ?></title>                             -> Linea de código traducida a PHP puro en un paso intermedio
    <title>Hola</title>                                                 -> Linea de código en el HTML devuelto al cliente


    Ilustraremos otro ejemplo, pero en este caso, con un método definido entre los delimitadores, en lugar de un literal:

    <title>{{ config('app.name', 'Laravel') }}</title>                   -> Linea de código definida en nuestro archivo Blade
    <title><?php echo e(config('app.name', 'Laravel')); ?></title>       -> Linea de código traducida a PHP puro en un paso intermedio
    <title>Laravel</title>                                               -> Linea de código en el HTML devuelto al cliente

    En el segundo ejemplo, lo que nos devuelve la ejecución de 'config('app.name', 'Laravel')' es el string
    "Laravel", que es el valor que se incluye finalmente en el código HTML devuelto al cliente.

    Por lo general, en esta vista, utilizaremos el código situado entre llaves dobles para obtener valores definidos en
    algún otro archivo de nuestra aplicación (no correspondiente a una vista).
    
    Utilizamos también, los delimitadores de llaves dobles junto con guiones dobles, para delimitar los comentarios Blade
    (por ejemplo, en la delimitación de este bloque de texto).
    Un comentario Blade, de forma similar a un comentario JavaScript, no tendrá ningún efecto en el funcionamiento
    del código definido en el archivo. Sin embargo, el texto definido en un comentario Blade, no será visible en el
    código devuelto al cliente, lo cual sí ocurrirá en un comentario JavaScript.

    

    El código JavaScript que tengamos definido en este archivo, estará embebido en código con sintaxis de Blade, de forma que
    se renderizará en el navegador de acuerdo a la estructura que definamos mediante este último.
    


--}}

{{--
    Esta vista, hereda de la plantilla principal de Blade que tenemos definida, utilizando la directiva '@extends'.
    Esto significa que, el código escrito en este archivo, será luego combinado con el código definido en la plantilla principal,
    siendo insertado en las secciones correspondientes de esta última.
    De acuerdo a como estructuremos el código en estos dos archivos (el de esta vista, y el de la plantilla principal), el
    servidor los combinará para generar el documento HTML devuelto al cliente.

    Entonces, en el documento correspondiente a esta vista, que será devuelto al cliente por nuestro servidor, tendremos partes de
    su código resultante (HTML, JavaScript y CSS) generadas a partir del código fuente definido en este archivo de Blade, y también
    partes de ese código resultante generadas a partir del código fuente definido en el archivo Blade de la plantilla principal. 

    De esta forma, todas las vistas en nuestra aplicación, respetarán la estructura definida en el archivo Blade correspondiente
    a la plantilla principal. Evitamos así, replicar código en cada archivo Blade correspondiente a cada vista que tengamos.
--}}
@extends('layouts.app')


{{--
    Usamos aquí, la directiva '@section', pasandole el string 'content' como argumento.
    Blade así, a la hora de generar el documento que será devuelto al cliente a partir de este archivo de vista, en su paso final,
    copiará todo el código situado entre estas directivas de apertura y cierre ('@section'/'@endsection'),
    en la sección correspondiente definida en el código de la plantilla principal. 
    Donde tengamos '@yield('content')' en el código de la plantilla principal, será insertado todo el bloque de código
    aquí definido entre las directivas '@section/@endsection' en el documento devuelto al cliente.

    En este caso, el código definido en este bloque, será el elemento donde el mapa Leaflet será renderizado por el navegador,
    en el DOM devuelto al cliente. Se renderizará una vez que lo implementemos utliizando JavaScript, mas abajo en este mismo archivo.
--}}
@section('content')
{{-- Este <div>, será el padre de otro <div> hijo, siendo este último el contenedor del mapa --}}
<div class="card">
    {{--
        En este <div> hijo, es donde será renderizado el mapa Leaflet, con toda la información y comportamiento que le implementemos 
        Es de suma importancia notar que su atributo 'id' tiene el valor 'mapid'. Este último valor, es el que utliizará Leaflet
        para identificar el elemento del DOM donde renderizará el mapa que nos genere.
     --}}
    <div class="card-body" id="mapid"></div>
</div>
@endsection


{{--
    Usamos aquí, nuevamente la directiva '@section', pero en este caso, pasandole el string 'styles' como argumento.
    Esto funciona igual que el 'section' anterior, solo que aquí, todo el bloque de código situado entre estas directivas
    de apertura y cierre ('@section'/'@endsection'), será insertado en donde tengamos la directivoa'@yield('styles')',
    dentro de la plantilla principal.

    En este bloque de código, importamos los archivos CSS correspondientes al plugin 'MarkerCluster' que utilizamos aquí.
    Si estamos en la vista principal, importamos también los archivos CSS correspondientes al plugin Leaflet Draw.

--}}

@section('styles')

<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css" />
@if(isset($principal) && $principal)
    {{--Solo incluimos Leaflet Draw en la vista principal --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.css" />
@endif
<style>
    /* Definimos la altura minima del mapa */
    #mapid { min-height: 500px; }
</style>
@endsection


{{--
    Usamos aquí, la directiva '@push', pasandole el string 'scripts' como argumento.

    Esta directiva '@push', funciona de foma muy similar a '@section'. La principal diferencia, es que utilizando '@section' y
    pasandole como argumento un string, cuyo valor sea el nombre de la sección de código que estamos definiendo en ese bloque,
    no podremos luego en la misma vista, utilizar otro '@section' con ese mismo nombre pasado como argumento.
    Si existen dos bloques de '@section/@endsection' con el mismo valor de nombre pasado como argumento, dentro una misma vista Blade,
    el motor de Blade procesará solo el primero en ser definido, y descartará el segundo ignorando su código.

    En cambio, utilizando '@push', si podemos definir diferentes bloques de código con el mismo nombre, para que estos sean incorporados,
    de forma secuencial, dentro del código definido en la plantilla principal.
    Podemos así, utilizar reiteradas veces en una misma vista de Blade, la directiva '@push' con un mismo valor
    de nombre pasado como argumento.

    Blade así, a la hora de generar el documento que devolverá al cliente, insertará cada bloque de código defenido entre estas
    directivas de apertura y cierre ('@push'/'@endpush'), en el lugar correspondiente dentro del código de la plantilla principal.
    Ese lugar será, donde tengamos en el código de la plantilla principal, la directiva '@stack('script')'.

    Como el nombre de la directiva '@stack' sugiere, estaremos implementando una pila dentro del código de la plantilla principal.
    De esta forma, al codificar en alguna vista hija, diversos bloques de codigo ubicados entre directivas '@push' con un mismo valor
    de nombre pasado como argumento, Blade irá añadiendo al 'stack' de la vista padre que tenga dicho nombre como valor,
    cada uno de estos bloques de código codificados en la vista hija, de forma acumulativa.

    En este caso, en este bloque de código definido entre estos '@push/@endpush', implementaremos todas las funcionalidades
    otorgadas por la librería Leaflet, para generar un mapa con el contenido que necesitamos en esta vista que estamos definiendo.
--}}

@push('scripts')

{{--
    En estos primeros dos tags <script>, importamos los archivos JS, que contienen el código JavaScript donde están implementadas
    todas las funcionalidades programables provistas por el plugin 'MarkerCluster' que utilizamos, y si estamos en la vista principal,
    las funcionalidades de Leaflet Draw.
    Si bien previamente importamos los archivos CSS correspondientes a estas herramientas,
    es aquí donde importamos los archivos con sus funcionalidades programables. Estas no funcionarían correctamente sin haber
    importado previamente los archivos CSS correspondientes.

--}}


<script src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
@if(isset($principal) && $principal)
    {{--Solo incluimos Leaflet Draw en la vista principal --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js"></script>   
@endif

{{--
    Es dentro de este tag <script>, donde implementaremos en la presente vista, toda la lógica correspondiente a la utilización de
    las funcionalidades de Leaflet.
    Así, al ser esta vista renderizada en el navegador, podremos ver en ella correctamente un mapa, con toda la información
    que necesitemos mostrada en el.
--}}
<!-- Codificación del mapa Leaflet -->
<script language="javascript" type="text/javascript">
    

    /*
    Esta es la sección que contiene el código JavaScript utilizado por la vista principal de nuestra aplicación Laravel,
    para la implementación de un mapa Leaflet en ella.
    
    El propósito de este proyecto de ejemplo es la comprensión del funcionamiento y utilización básicos de la librería
    Leaflet en el contexto de una aplicación Laravel. Debemos así, tener claros los fundamentos de ambas herramientas
    (Laravel y Leaflet) de forma independiente, aunque estén integradas en una misma aplicación.

    Debemos tomar en cuenta que Leaflet es una librería que utiliza JavaScript como lenguaje para su desarrollo
    y utilización, mientras que Laravel es un framework que utiliza el lenguaje PHP para tal propósito.

    Esta sección en particular, al consistir básicamente en código JavaScript, tiene énfasis mayormente en la ejemplificación de
    la utilización de Leaflet para implementar un mapa interactivo, independientemente de que dicha herramienta esté integrada en un
    proyecto Laravel.
    
    Sin embargo, esta sección tiene también unas pocas sentencias correspondientes a código PHP, y también a código
    interpretable por el motor de vistas Blade de Laravel (que no consiste en PHP puro). 
    Tales sentencias, estarán embebidas dentro del código JavaScript codificado en esta sección.
    
    Serán utilizadas principalmente para obtener, desde otros módulos de la aplicación, la información que necesita esta vista
    para mostrar el contenido correspondiente.
    
    Sobre todo, para la utilización de la API de la propia aplicación. A través de la misma, realizamos las consultas a la base de datos, 
    para obtener y poder plasmar en nuestro mapa la información allí almacenada.

    Describiremos entonces, linea por línea, que hace este código JavaScript para implementar la visualización y el comportamiento
    del mapa Leaflet que mostrará esta vista, con los elementos necesarios mostrados.

    Con el objetivo de analizar el tipo de información manejada por Leaflet, sobre todo contenida en objetos JavaScript,
    utilizaremos intensivamente la consola del navegador para visualizarla, a traves de reiteradas llamadas al método
    'console.log', desde diferentes lugares del código definido aquí para esta vista.



    Como cuestión importante:
    
    Una aplicación con desarrollada con Leaflet, utilizará intensivamente objetos Leaflet. 
    
    Un objeto Leaflet es, básicamente, un objeto de JavaScript que respeta ciertas convenciones, para poder
    de esta forma, ser procesado adecuadamente por los diferentes métodos provistos por Leaflet.
    También, los objetos Leaflet suelen contener métodos Leaflet definidos en sus atributos, los cuales a la vez,
    podrían recibir otros objetos Leaflet como argumentos.

    Estos objetos Leaflet, por lo general, son generados e instanciados inicialmente a través de los métodos de Leaflet
    contenidos en la clase 'L'. Es en dicha clase, donde están implementadas casi todas las funcionalidades de
    Leaflet. Al generar un objeto Leaflet que contiene a la vez, métodos de Leaflet definidos en el, podrémos así encadenar
    métodos, lo cual es una práctica usual en una aplicación Leaflet.

    Luego de ser creados, los objetos Leaflet suelen ser mutados reiteradas veces a lo largo de la ejecución del programa.
    Esto se debe a la propia dinámica que suelen tener los programas desarrollados con Leaflet.

    Los objetos Leaflet, ademas de poder llegar a ser muy grandes y complejos (con muchos niveles de anidamiento),
    suelen contener métodos, así como referencias circulares (referencias a si mismos en alguna parte de su estructura).
    Por lo tanto, generalmente su contenido no se podrá obtener en formato JSON, que consiste en solo texto.
    Entonces, en muchos casos, su contenido no podrá en ser copiado a un archivo de texto para el análisis de su estructura y sus datos,
    o para su comparación con otros objetos Leaflet.

    Debido a esta cuestión, en mi caso utilizo los navegadores Chrome o Chromium, para una adecuada visualización
    de estos objetos en la salida de la consola del navegador. Esto se debe a que, a diferencia por ejemplo de Mozilla Firefox,
    son mas amigables para visualizar toda la compleja estructura en forma de arbol de un objeto con tales caracteristicas,
    al ser este logueado en la consola del navegador.

    También, Chrome o Chromium nos permiten de forma nativa, en los casos que sea posible,
    copiar facilmente el contenido de un objeto que haya sido logueado en la consola del navegador como texto JSON, .
    
    De este modo, podremos transferir fácilmente este contenido a un editor o procesador de texto, si esto fuera necesario o conveniente.
    Ademas, su consola nos permite expandir recursivamente, todo el arbol de atributos anidados, existente en el contenido
    del objeto que esté siendo mostrado por consola.
    
    Esto claramente tendrá un inconveniente: si dicho objeto tiene referencias circulares, se comenzaría a expandir indefinidamente
    el arbol de sus atributos, hasta que se agote la memoria.
    Sin embargo, al desplegar en la consola únicamente las ramas que necesitamos de ese arbol, nos permite exportar facilmente
    como texto el contenido existente en ellas, aún cuando no sea posible exportar como texto el contenido completo del objeto.
    

    Mas allá de mi inclinación personal por estos navegadores, para realizar la visualización y análisis de la información manejada
    por Leaflet, cada uno podrá elegir el que mas sea de su agrado para tal fin. Debido a que solamente lo probé en los navegadores
    que mencioné aquí (Firefox y Chrome/Chromium), desconozco la potencialidad de otros navegadores para realizar esta visualización
    que necesitamos a traves de su consola, o de la existencia de algún plugin para algún otro navegador, que nos permita
    optimizar este proceso.

    
    
    Al realizar la visualización del contenido de los objetos Leaflet, debemos tener en cuenta una cuestión fundamental:

    Al utilizar el método 'console.log', pasandole como argumento una referencia a un objeto o array,
    cuando uno expanda ese objeto o array logueado en la consola, verá el contenido del mismo
    al momento de expandirlo allí, y no el que existía originalmente cuando 'console.log' fue llamado.

    Debido a esto, y dada la estructura tan compleja que suele tener un objeto Leaflet, que ademas será frecuentemente mutado,
    debemos encontrar una forma de resolver los inconvenientes que dicho funcionamiento acarrea.
    Utlilizaremos entonces, la librería 'Lodash',la cual nos provee el método 'cloneDeep'. Este método, devolverá una copia exacta
    del objeto al cual le pasemos una referencia como argumento.
    De este modo, al utilizar el método 'console.log', pero pasandole como argumento la referencia a un objeto dentro de una
    llamada a 'cloneDeep', en lugar de hacerlo directamente, podremos observar completamente el contenido que exista en el
    objeto cuando 'console.log' es llamado, y no el existente en el momento que expandamos el objeto en la consola.
    

    Luego, debemos mencionar que la renderización de un mapa Leaflet en el navegador, se estructura mediante capas.

    Una capa es, básicamente, un objeto Leaflet, el cual que contiene información geográfica relavante para nuestra aplicación.
    Dentro de ese objeto Leaflet, dicha información geográfica estará estructurada y definida en forma tal, que pueda ser leída
    y procesada por algún método de Leaflet.
    De esta forma, la información geográfica contenida en los objetos Leaflet, podrá así ser finalmente visualizada en el mapa,
    cuando el mismo sea renderizado por el navegador.

    Aplicar una capa, significa asociar un objeto Leaflet, el cual debe tener caracteristicas de capa, a otro objeto Leaflet,
    de forma que este último adoptará una jerarquía superior.
    Para aplicar un objeto Leaflet como capa, a algún otro objeto Leaflet, necesitaremos utilizar algún método provisto por Leaflet
    para tal propósito.   
    
    De esta forma, al serle aplicada una capa, el objeto Leaflet que adopte una jerarquía superior, incorporará una referencia
    dentro de su estructura, al otro objeto Leaflet que le fue aplicado como capa (el cual adoptaría una jerarquía inferior).

    A la vez, ese objeto Leaflet que adoptó una jerarquía superior al serle aplicada una capa, podría también ser aplicado
    como capa a otro objeto Leaflet, el cual adoptaría una jerarquía aún mayor.

    Debemos mencionar que, por lo general, los objetos Leaflet que son utilizables como capas, tienen un atributo llamado
    '_leaflet_id', el cual contiene un valor numérico (entero positivo), que lo identifica, siendo este su ID de Leaflet.
    Cuando dicho objeto Leaflet es aplicado como capa a otro, este último, que adoptará una jerarquía superior, incluirá un atributo
    '_layers'. El valor de ese atributo, será un objeto JavaScript, en donde se definen todas las capas que le fueron aplicadas.
    Las claves de dicho objeto en '_layers', se corresponden con los números de ID de Leaflet, de aquelllos objetos Leaflet aplicados
    como capa a este objeto Leaflet de jerarquía superior, mientras que los valores asociados a cada una de esas claves,
    corresponden al contenido completo de cada uno de esos objetos jerarquía inferior.

    El objeto Leaflet de mayor jerarquía de todos que tendremos en esta vista de nuestra aplicación (y por lo general, en todas),
    será el objeto de tipo mapa.
    A este objeto mapa, finalmente, le terminarían siendo aplicadas todas las capas generadas por este código, ya sea de forma directa o
    indirecta. Por lo tanto, este objeto mapa finalmente contendrá, en alguna parte de su estructura,
    una referencia a cada objeto Leaflet que haya sido creado en esta sección del código, y que le haya sido aplicado como capa.

    Debemos considerar que, si no le aplicaramos ninguna capa al objeto mapa de Leaflet, su renderización en el navegador solo
    nos mostraría un rectangulo gris, sin siquiera mostrar una visualización cartográfica de tipo alguno.


    Podría en este código, llegar a generar un objeto Leaflet con caracteristicas de capa, y sin embargo,
    en ningún momento aplicarselo, directa o indirectamente, como capa al objeto mapa de Leaflet.
    
    Si bien esto es posible, a fines practicos, sería de dudosa utilidad para el usuario final de la aplicación.
    Tendría mas sentido hacer esto, sin embargo, durante la etapa de desarrollo de la aplicación, para poder así analizar
    el contenido existente en dicho objeto Leaflet, sea el que tenga al momento de ser recien generado,
    o bien al ser posteriormente mutado.
    Esto se haría para fines analíticos o de debugging, y así visualizar mejor el funcionamiento interno de Leaflet, siendo
    que el propósito de esta aplicación es esencialmente didáctico.



    Debemos ahora, describir el formato GeoJSON, utilizado por Leaflet en esta aplicación:

    El formato GeoJSON es básicamente, una norma para representar información geográfica, definendo la estructura que debe tener
    un objeto GeoJSON, en donde se define esa información, para tal propósito.
    Un objeto GeoJSON es, escencialmente, un objeto JSON, pero que respeta ciertas convenciones en su estructura, acordes a este formato.
    Estas convenciones, fueron establecidas con el fin de asegurar compatibilidad, entre distintas herramientas informáticas
    que manipulan información geográfica, para poder efectuar intercambio de datos entre si. Leaflet es una de estas herramientas.
    A pesar de que existen diferentes formatos en los cuales puede representarse información geográfica con fines similares,
    GeoJSON es el mas amigable con Leaflet, que lo soporta de forma nativa. Esto es, Leaflet nos provee métodos
    para procesar la información geográfica contenida en un objeto GeoJSON.
    Entonces, GeoJSON será el formato utilizado por esta aplicación, en parte de su funcionamiento interno, para procesar la información
    geográfica que se visualizará en el mapa Leaflet, al ser este renderizado en el navegador.
    Esta información geográfica, está inicialmente registrada en nuestra base de datos sin un formato especifico.
    Por lo tanto, en algún módulo de nuestra aplicación, esa información geográfica en existente en crudo, deberá ser formateada
    como un objeto GeoJSON, el cual será obtenido y procesado desde esta vista mediante Leaflet.


    
    La información geográfica existente en nuestra aplicación, se procesará entonces de la siguiente forma:

    Campos de tipo texto o númerico, obtenidos desde la base de datos -> Objeto GeoJSON -> Objetos Leaflet

    Debemos tener algo en cuenta: existe un software llamado PostGIS, el cual añade soporte para trabajar con datos de tipo
    geográfico al motor de base de datos PostgreSQL. No obstante, en nuestra aplicación utilizamos el motor
    MySQL/MariaDB para implementar nuestra base de datos.
    La información geoespacial de cada inmueble registrado en nuestra aplicación, consiste únicamente en sus coordenadas geográficas.
    Estas coordenadas (su latitud y longitud), están almacenadas en nuestra base de datos simplemente en formato numérico.
    Esta metodología, por lo pronto, aquí resulta efectiva, ya que los únicos datos geoespaciales que la aplicación maneja
    hasta el momento consisten en puntos, los cuales son muy sencillos de representar, simplemente a partir de dos números con sus
    coordenadas geográficas.
    Si la aplicación tuviera que manejar datos geoespaciales mas complejos, tales como polígonos, en ese
    caso sería probable que este método que utilizamos aquí para almacenar información geográfica, no resulte tan efectivo.

    Hechas estas consideraciones generales, pasamos entonces a describir, linea por línea, esta sección de código.

    */


    {{--
    IMPORTANTE: Utilizaremos bloques de código, delimitado por las directivas Blade 'if/endif', donde tendremos definido el código
    que debe ser renderizado EXCLUSIVAMENTE para la vista del mapa principal

    El contenido de estos bloques NO será renderizado por Blade cuando estemos generando la vista filtrada.

    La variable '$principal', que contiene un valor booleano recibido desde el método del controlador que llame a esta vista,
    valdra 'true' si queremos renderizar la vista principal, o 'false' si queremos renderizar la vista filtrada.
    --}}

    @if(isset($principal) && $principal)
        console.log("Esta es la vista del mapa principal");
    @endif

    
    //Declaro luego, una constante JavaScript que contendrá un array de dos elementos, correspondiente a las coordenadas
    //del centro donde deberá ser inicializada la vista del mapa, según corresponda.


    //Añado el primer bloque con renderizado condicional.

    @isset($centroidePoligono) //Este bloque solo se ejecutará si recibimos algo en '$centroidePoligono'
        var geoJsonPolygon;    
        //En '$centroidePoligono' deberíamos tener un array PHP, donde tendremos codificado un objeto GeoJSON con el centroide de un polígono

        console.log("$centroidePoligono está definida");
        
        //Con una función auxiliar definida por nosotros, mostramos por consola lo que tenemos en '$centroidePoligono', convirtiendolo
        //antes a string
        {{debug_to_console_view(json_encode($centroidePoligono))}}
        {{debug_to_console_view(gettype($centroidePoligono))}}
        
        
        //Con otra función auxiliar definida por nosotros, convertimos el valor de '$contenido' desde un array PHP
        //a un string legible por JavaScript
        const objPoligono= {{ php_array_to_js_string($centroidePoligono['coordinates']) }};
        console.log("Centroide");
        console.log(objPoligono);
        console.log(JSON.parse (objPoligono));

        const coordenadasLngLat=JSON.parse(objPoligono);
        //Finalmente, con 'JSON.parse', convertimos ese string JavaScript que contiene el JSON, a un objeto JavaScript.
        //Nos guardamos ese objeto en la variable global 'geoJsonPolygon'

        const centro=[coordenadasLngLat[1],coordenadasLngLat[0]];

    @else
        //Si no recibimos nada en '$centroidePoligono', definimos 'centro'
        //a partir de los valores obtenidos desde el archivo 'config/leaflet.php', mediante el método 'config' de Laravel. 
        const centro=[
                        {{ config('leaflet.map_center_latitude') }},
                        {{ config('leaflet.map_center_longitude') }}
                    ];
    @endisset

    
        
   
    //Declaro primero, una variable 'map', donde me guaradré la referencia al objeto mapa de Leaflet que genere para esta vista.
    //Este objeto mapa, es el que será renderizado por el navegador, dentro de la sección que corresponda, en el documento
    //devuelto al lado del cliente.
    //Así, podremos luego aplicarle capas utilizando esta referencia que guardamos, y así visualizar en el mapa renderizado,
    //la información contenida en cada una de ellas.
    //alert("Hola");
    var map =               
        //Llamo al método 'L.map', que me generará un objeto Leaflet de tipo mapa, devolviendo una referencia a el.
        //Le paso un solo argumento: primero un string con el valor 'mapid'. Este valor se corresponde con el mismo valor
        //del atributo 'id' del tag <div>, que tenemos en el código HTML definido mas arriba en este archivo de Blade.
        //Es en ese <div>, dentro del cual queremos que sea renderizado el contenido del mapa.

 
        //Esto haría si quisiera tener el panel de control por defecto de Leaflet Draw (pasaría '{ drawControl: true }')
        //como segundo argumento a L.map
       // L.map('mapid', { drawControl: true })


     L.map('mapid')      
            //Una vez hecha la llamada a 'L.map', vemos que no cerramos la sentencia con ';', sinó que le encadenamos otro método.
            
            //Este método 'setView', encadenado a 'map', lo utilizamos para fijar la posición geográfica en donde 
            //queremos que sea inicializada la visualización del mapa, ni bien sea renderizado en la página.
            //Le pasamos dos argumentos para ello, indicandole las coordenadas en el primero y el nivel de zoom en el segundo.
            //Si no le fijamos una posición inicial al mapa, obtendremos un error y no podremos visualizarlo.
            //¡Ojo! Aquí, incluimos algo de código PHP, entre la llaves dobles leidas por Blade.
            //Lo utilizamos para obtener valores definidos en otro archivo de nuestra aplicación Laravel, y así pasarlos 
            //como argumentos a 'setView'.
            .setView( 
                //Como primer argumento a 'setView', le pasamos un array con dos números, correspondientes a las coordenadas
                //geográficas donde queremos que se inicialize la visualización del mapa, con el array 'centro' definido
                //mas arriba en el renderizado condicional.
                
                centro,

                //Como segundo argumento a 'setView', le pasamos un número, con el nivel de zoom con el que queremos
                //que se inicialize la visualización del mapa.
                //Este valor es obtenidodesde el archivo 'config/leaflet.php', mediante el método 'config' de Laravel. 
                {{ config('leaflet.zoom_level') }}
            );

    /*
    Ahora, al renderizar el documento, tendremos un mapa navegable en la página, ubicado dentro del tag <div>
    correspondiente al cual fue asignado el objeto mapa (con id="mapid").
    
    Sin embargo, si no procedieramos a generar capas, y aplicarselas a este objeto mapa, lo único que veríamos
    al renderizarlo en el navegador, sería solo un rectángulo gris, y dos íconos para regular el nivel de zoom (+ y -).
    
    Notese que, si bien generamos recién un objeto Leaflet de tipo mapa, este objeto inicialmente no contiene información de ningún
    mapa en el sentido convencional de la palabra: el de la representación gráfica de un territorio físico.

    Esta representación gráfica, debe ser añadida al objeto de tipo mapa, posteriormente a que tal objeto haya sido generado por
    'L.map'. Se logrará esto aplicandole una capa a ese objeto mapa, que tenemos referenciado en la variable  'map'.

    Debemos así, generar un objeto Leaflet que genere tal representación gráfica, y así poder aplicarselo como capa al objeto mapa.
    Este objeto Leaflet será de tipo TileLayer. Se encargará de obtener y procesar las imágenes de un mapa geográfico,
    a partir de algún proovedor que ofrezca el servicio correspondiente para ello, mediante el servidor provisto por el.
    En este caso, nuestro proovedor de imágenes geográficas será OpenStreetMap.
    
    Estaremos aquí en este caso, obteniendo desde este servicio, imágenes estructuradas en forma de Tile Map.
    Esto es, un mapa cuya información geográfica estará subdividida en secciones con forma de cuadrados,
    llamados Tiles ('azulejos' o 'mosaicos' en inglés). Estos Tiles, estarán contenidos unos dentros de otros, de forma anidada.
    
    De esta forma, si bien en el mapa podremos ver la representación de la Tierra entera,  solo se cargará la información
    geográfica detallada relevante para una determinada región, como sus calles y alturas, al hacer zoom o al 
    desplazarnos sobre ella.
    Esta información, la veremos en los mosaicos que hayan sido cargados al hacer zoom o al desplazarnos en el mapa,
    con mayor nivel de detalle a medida que hagamos cada vez mas zoom, y se vayan cargando mosaicos de cada vez
    mayor nivel de anidamiento.

    Sería poco eficiente, cuando no directamente improcesable, que se cargue en nuestra aplicación de una sola vez,
    toda la información correspondiente a cada entidad geográfica existente en la Tierra.

    El objeto de tipo TileLayer que generemos, se encargará así de hacer las consultas correspondientes al servidor de OpenStreetMap
    (que en este caso, es el proovedor elegido), cada vez que nos desplazemos o hagamos zoom en el mapa, para obtener la
    información geográfica pertinente a la región que focalizemos, y así poder mostrarla.
    */


    //Generarmos entonces, el objeto Leaflet de tipo TileLayer, llamando al método 'L.tileLayer'.
    
    //Le pasámos dos argumentos:
    //- Un string con una plantilla URL, que será utilizada por el objeto TileLayer generado,
    //  para hacer las peticiones necesarias al servidor del proveedor elegido, y obtener así la información que necesitemos.
    //- Un objeto JavaScript, donde definimos las opciones utilizadas por este método, para generar este objeto Leaflet de tipo TileLayer.

    //Nos guardamos la referencia al objeto generado por 'L.tileLayer' en la variable 'tile'. Si no lo hicieramos, la aplicación
    //aún funcionaría correctamente. Lo hacemos aquí, para poder visualizar luego su contenido, con propósitos de análisis y debug.
    var tile = 
        L.tileLayer(
            //Como primer argumento, le pasamos aquí a 'L.tileLayer' un string con una plantilla URL.
            //Esta plantilla URL, es proporcionada por nuestro proovedor de imágenes TileMap, para hacer peticiones a su servidor.
            //En este caso, nuestro proveedor es OpenStreetMap.
            //Su valor contiene el prototipo de las URLs que utilizará este objeto Leaflet que estamos generando,
            //para hacer tales peticiones al servidor de nuestro proveedor de imágenes TileMap.
            //Por ejemplo, una URL posible sería: "https://a.tile.openstreetmap.org/13/-34.61315/-58.37723.png"
            //De esta forma, esta incluiría: el subdominio 'a', un nivel de zoom '13', y las coordenadas '-34.61315/-58.37723',
            //para una determinada petición.
            //Se realizará una petición nueva, cada vez que deba cargarse la información de un mosaico diferente al navegar
            //en nuestro mapa renderizado.
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

            //Como segundo argumento, le pasamos un objeto JavaScript, el cual contiene en este caso, un solo atributo, de tipo String.
            {
                //En este atributo 'attribution', incluimos una atribución (mención), a nuestro proovedor OpenStreetMap.
                //Esta atribución, si bien a nivel técnico no es necesaria para que los datos que queremos sean obtenidos correctamente
                //desde el servidor de OSM, y poder así ser vistos en el mapa, es exigida por este proveedor a efectos legales.
                //Su única función es colocar en el mapa una mención al proveedor, así como un link a su página de Copyright.
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }
        )   //Aquí se cierra la llamada a 'L.tileLayer'. Si fué ejecutada con éxito, nos habrá generado 
            //el objeto Leaflet de tipo TileLayer que necesitábamos. 
            
        //Notese que al cerrar la llamada a 'L.tileLayer', no cerramos la sentencia con ';', por el siguiente motivo:
        
        //Este objeto tipo TileLayer que generó 'L.tileLayer', debe serle aplicado como capa a nuestro objeto mapa de Leaflet.
        //Para hacer eso, a 'L.tileLayer', le encadenamos el método 'addTo', pasandole la variable 'map' como argumento.
        //Recordemos que 'map', contiene la referencia a nuestro objeto mapa de Leaflet, que instanciamos previamente.
        //Este método 'addTo', existirá en cada objeto Leaflet con caracteristicas de capa, incluyendo este generado por 'L.tileLayer'.
        
        //Realizará así, la acción buscada:
        //Aplica como capa el objeto desde el cual fue llamado, al objeto que recibe como argumento (referenciado por una variable).
        //Por ejemplo:
        //'A.addTo(B);' -> Aplica como capa el objeto A, al objeto B (B resultará el de jerarquía superior).
        
        //Debemos tener en cuenta que este método, mutará el objeto B que le pasemos como argumento,
        //incorporando en el, una referencia al objeto A que le estamos aplicando como capa.
        //En este caso, será mutado el objeto mapa, al incorporar ahora una referencia al objeto TileLayer que recién generó
        //'L.tileLayer'.

        .addTo(map); //Aquí cerramos la sentencia con los métodos 'L.tileLayer' y 'addTo' encadenados.
    
    
    //NOTA:

    //Podría no haber utilizado el encadenamiento directo de 'L.tileLayer' con 'addTo', y en cambio, utilizado la sentencia:
    //map.addLayer(tile); ('addLayer es un método que funciona de forma inversa a 'addTo')
    //O bien:
    //tile.addTo(map);

    //Hubiera obtenido así el mismo resultado, siempre y cuando me haya guardado previamente la referencia al objeto TileLayer
    //que generamos, en la variable 'tile'.

    //Sin embargo, es conveniente implementar e ilustrar aquí, el encadenamiento de métodos que suele utilizar una aplicación Leaflet.
    //Vemos que, haciendo el encadenamiento, no tendríamos necesidad de guardarnos una referencia al objeto TileLayer en una
    //variable, ya que este es generado y luego aplicado como capa al objeto mapa, en una sola sentencia.


    //En este punto, tras haberle aplicado esa capa TileLayer al objeto mapa, ya tendríamos renderizado en el navegador,
    //un mapa navegable con información gráfica elemental visible

   

   
   {{--
    IMPORTANTE: En el siguiente bloque de código, tendremos definido el código mas importante
    que debe ser renderizado EXCLUSIVAMENTE para la vista del mapa principal, implementando aquí las funcionalidades de Leaflet Draw
    y algunas funciones que nos permiten pasar los polígonos que dibjujemos rápidamente a la vista filtrada, como también descargarlos.

    --}}
    @if(isset($principal) && $principal)

    //En la siguiente sección, implementamos las funcionalidades provistas por el plugin "Leaflet.draw", de forma que
    //podamos dibujar polígonos (o geometrías similares) y convertirlas a formato GeoJSON, para que así pueda ser
    //procesada su información por otros módulos de la aplicación (GeoJSON es el formato que utilizamos para el intercambio de datos
    //geográficos).


    //Primero que nada, definimos una función que nos genere un <form> de forma dinámica

    //Utilizamos esta función para mandar al controlador, mediante una petición POST, el GeoJSON que generaremos a partir de los
    //polígonos que dibujamos con Leaflet.draw (con dicho objeto convertido
    //primero a texto, para así poder ser enviado en el cuerpo de una request HTTP).
    //Recibe un string con una URI como primer parámetro, y un objeto JavaScript (que debería respetar el formato GeoJSON) como segundo parámetro.
    //Al ejecutarse, crea un <form> de forma dinámica para su propósito, y lo envía de forma instantánea a la URI recibida como
    //primer parámetro. Debemos generar necesariamente el <form>, ya que en HTML, es la única forma que tenemos de hacer una petición POST.
    //No obstante, podremos definir este
    function PostObjectToUri(uri, obj) {
        //Habilitamos el modo estricto
        "use strict";

        //Declaramos tres variables a utliizar
        var form, input;

        //En 'form', creamos un nuevo elemento <form> en nuestro DOM
        form = document.createElement("form");

        //El elemento form, tendrá asociado el método 'post'
        form.method = "post";

        //El elemento form, tendrá asociada la URI que pasemos por parámetro a la funcion
        form.action = uri;

        //Al form recien generado, le añadimos un Token CSRF, para evitar problemas de validación en Laravel (es una medida de seguiridad)
        form.insertAdjacentHTML('afterbegin', '{{ csrf_field() }}');

        //En 'input', creamos un nuevo elemento <input> en nuestro DOM
        input = document.createElement("input");
        
        //Al atributo 'name' del <input>, le asignamos el valor 'json'
        input.setAttribute("name", "json");

        //Al atributo 'type' del <input>, le asignamos el valor 'hidden' (para que no sea visible en el navegador) 
        input.setAttribute("type", "hidden");

        //Al atributo 'vaule' del <input>, le asignamos el contenido del objeto GeoJSON que recibimos por parámetro (codificado como String)
        input.setAttribute("value", obj);
        
        //Agregamos el elemento <input> generado como hijo al elemento <form> generado
        form.appendChild(input);
        
        //Agregamos el elemento <form> completo generado, como hijo al elemento <body>
        document.body.appendChild(form);
        
        //Finalmente, hacemos el submit de lo que nos haya quedado en el <form>
        form.submit();
       
        //Luego de que hicimos el submit con los datos pertinentes en nuestra petición POST, este form generado dinámicamente
        //ya no es necesario, y de hecho sería un defecto en nuestra vista si es mostrado en el navegador, donde queremos que
        //la metodología aquí utilizada para realizar una petición POST utilizando un hipervínculo (y no mediante un botón de un
        //form), sea lo mas transparente posible para el usuario. Por lo tanto, lo eliminamos del body del HTML luego de que se haya realizado el 
        //submit.
        document.body.removeChild(form);
    };

    //Inicializamos un objeto Leaflet de tipo FeatureGroup, para almacenar las capas editables
    //Este objeto Leaflet contendrá todos los dibujos que hagamos con Draw:
    var editableLayers =  L.featureGroup();

    //Se lo aplicamos como capa al objeto mapa:
    map.addLayer(editableLayers);

    //Definimos las opciones de la barra de herramientas de Leaflet Draw, a mostrar en el mapa Leaflet renderizado:
    var drawPluginOptions = {
        //Situamos la barra en la sección superior derecha del mapa
        position: 'topright', 

        //Definimos las opciones para la barra de herramientas de dibujo (Draw)
        draw: {
            polygon: {       //Opciones para el dibujado de un polígono
                allowIntersection: false, // Restringe las figuras a polígonos simples
                drawError: {
                    color: '#e1e100', // Color que adoptará la figura cuando es intersectada al ser dibujada
                    message: '<strong>Oh snap!<strong> you can\'t draw that!' //Mensaje que se mostrará cuando eso ocurra
                },
                shapeOptions: {
                    color: '#97009c'
                }
            },
            // Deshabilito las opciones de la barra de herramientas que no quiero que se muestren:
            polyline: false,
            marker: false,
            circlemarker:false,
            circle:false //ToDo: Configurar correctamente la opción para dibujar un círculo
        },
        //Definimos las opciones para la barra de herramientas de edicion (Edit)
        edit: {
            //Asociamos nuestro objeto FeatureGroup al control de Leaflet Draw (si no lo hacemos, obtendremos un error)
            featureGroup: editableLayers, 

            //Habilitamos el ícono Remover en la barra de herramientas
            remove: true
        }
    };

    //Inicializamos el objeto de control de dibujo de Leaflet Draw, con las opciones que definimos recién:
    var drawControl = new L.Control.Draw(drawPluginOptions);

    //Se lo aplicamos como capa al objeto mapa:
    map.addControl(drawControl);

    //Definimos algunas opciones para formatear el dibujo de un polígono:
    drawControl.setDrawingOptions({
        polygon: {
            shapeOptions: {
                clickable: false
            }
        }
    });
    
    
    //Aquí, debemos definir la forma en que, cuando el usuario esté dibujando una figura, el manejador de eventos asociado
    //al click sobre el mapa (codificado en la última sección de este archivo), se comporte de forma diferente
    //cuando estamos dibujando una figura, que cuando clickeamos en el mapa sin estar dibujandola.

    //Definimos primero un flag 'dibujando' inicializado en 'false'.
    //Este flag se activará cuando comenzemos a dibujar un polígono con Leaflet Draw, y se volverá a desactivar cuando lo cerremos.
    var dibujando=false; 

    
    //Codificamos luego, los manejadores de eventos asociados a las funciones de Leaflet Draw:

    //Cuando clickeamos en un ícono de la barra de herramientas para comenzar un dibujo, se dispara este evento:
    map.on('draw:drawstart', function(e) {
        if(poligono_anterior)  //Si ya teníamos un polígono previamente dibujado
            editableLayers.removeLayer(poligono_anterior); //Lo borramos al comenzar a dibujar uno nuevo
        console.log("Dibujo Comenzado");
        console.log(e);

        dibujando=true; //Activamos el flag 'dibujando'
    });

    //Cuando finalizamos el dibujo, se dispara este evento:
    map.on('draw:drawstop', function(e) {
        
        console.log("Dibujo Finalizado");
        console.log(e);
        dibujando=false; //Desactivamos el flag 'dibujando'
    });



    //Definimos una variable 'poligono_anterior' en el scope global, donde guardaremos el objeto Leaflet correspondiente
    //a la última figura que hayamos dibujado.
    let poligono_anterior;

    //Añado un escuchador de eventos que se ejecutará cuando un dibujo de una figura haya sido creado.
    //Lo utilizaremos para generar un popup asociado a dicha figura, y para guardarnos el objeto Leaflet correspondiente
    //a la figura dibujada en la variable 'poligono_anterior', de nuestro scope global.
    //Ademas, es aquí donde realizamos la generación del objeto GeoJSON con la información del polígono dibujado,
    //ya sea para ser descargado, o bien para ser pasado al controlador para generar así la información que tendrá la vista filtrada,
    //correspondiente a este objeto aquí definido.

    //Aquí en este manejador de eventos, el objeto de evento 'e' contendrá la información de la figura dibujada.

    //¡OJO! Al terminar de cerrar una figura, este manejador de eventos se ejecutará ANTES que el asociado a 'draw:drawstop'
    map.on('draw:created', function(e) {
        
        console.log("Dibujo Creado");
        console.log(e);

        layer = e.layer;  //El objeto anidado en el campo 'layer' de 'e', será el que aplicaremos como capa a nuestro FeatureGroup.
        poligono_anterior=layer; //Nos guardamos ese objeto en una variable accesible desde el scope global.
        console.log("Poligono: ");
        
        //Con el método 'toGeoJSON', generamos un GeoJSON a partir del objeto Leaflet que fue generado a raíz de nuestro dibujo del polígono.
        //Le pasamos el valor 15, indicando la cantidad de decimales de precisión en sus coordenadas.
        //Definimos en su atributo 'properties', un objeto anidado con solamente un atributo 'name', con el string 'zona' asingnado como valor.
        const polygon= {...layer.toGeoJSON(15), properties:{name:'zona'}};


        /*
        geometry: {type: 'Polygon', coordinates: Array(1)}
        properties: {name: 'zona'}
        type: "Feature"
        */

        //Con 'JSON.stringify', convertimos ese JSON de Objeto JavaScript a String, y lo guardamos en 'geojson_polygon_string'
        geojson_polygon_string=JSON.stringify(polygon);
         
        console.log(polygon);
        console.log(editableLayers);
        console.log(layer);


        
        //Una vez que tenemos el GeoJSON generado, definimos primero el código que nos permita descargarlo como archivo:

        //Generamos un elemento de DOM <a> para definir el enlace de descarga
        var enlaceDescarga = document.createElement("a");

        //Un objeto Blob representa un objeto tipo fichero de datos planos inmutables.
        //Los Blobs representan datos que no necesariamente se encuentran en un formato nativo de JavaScript
        //Creamos un objeto Blob con el contenido del GeoJSON generado (como texto).
        var blob_geojson=new Blob([geojson_polygon_string], {type: "application/json"});

        console.log(blob_geojson);

        //Asociamos el link del elemento <a> a la URL generada a partir del objeto Blob
        enlaceDescarga.href = URL.createObjectURL(blob_geojson);

        //Definimos el nombre del archivo que se descargará
        enlaceDescarga.download = "archivo.geojson";

        //Definimos el texto mostrado en el enlace de descarga visible en el navegador
        enlaceDescarga.textContent = 'Descargar';




        //Luego, definimos el código que nos permita enviar la información del GeoJSON a la vista del mapa filtrado:

        //Generamos un elemento de DOM <a> para definir el enlace a la vista filtrada
        var enlacePost = document.createElement("a");

        //Asociamos el link del elemento <a> a la URL generada con el prefijo 'javascript', que llama al método
        //'PostObjectToUri' pasandole como argumentos la ruta que recibe la petición POST, y los datos del GeoJSON generado
        //en forma de string.
        enlacePost.href = "javascript:PostObjectToUri(\'{{ route('outlet.filter') }}\',geojson_polygon_string);"
        
        //Definimos el texto mostrado en el enlace hacia la vista filtrada, visible en el navegador.
        enlacePost.textContent = 'Mapa Filtrado';

        
        
        //Cuando tenemos los dos enlaces generados correctamente, los agrupamos dentro de un <div>
        var contPopup = document.createElement("div");

        contPopup.appendChild(enlaceDescarga);
        contPopup.appendChild(document.createElement("br"));
        contPopup.appendChild(enlacePost);
        console.log(contPopup);

        //Al objeto Leaflet que representa la figura que recién dibujamos, le asignamos un Popup conteniendo el elemento
        //<div> que generamos, que agrupa los dos enlaces definidos.
        layer.bindPopup(contPopup).openPopup();
        
        //Aplicamos ese objeto como capa a nuestro FeatureGroup contenido en 'editableLayers' (y al objeto Mapa de forma indirecta).
        editableLayers.addLayer(layer);
    });

    {{--Cerramos el bloque de código correspondiente a la implementación de las funcionalidades mas importantes exclusivas de la vista principal--}}
    @endif
   
    /*
    Como siguiente paso, debemos incluir, en la visualización del mapa, toda la información geográfica que sea
    relevante especificamente para nuestra aplicación. En este caso, esta es la información de los inmuebles registrados en ella,
    correspondientes a la vista que debe ser generada desde este archivo (principal o filtrada).

    Generaremos entonces para esto, un objeto Leaflet que constituirá una capa de marcadores (markers), para aplicarsela así al objeto mapa.

    Cada marcador generado, es un ícono que señalará en el mapa, la ubicación geográfica de cada inmueble registrado en nuestra aplicación.
    Algunos datos del inmueble, se mostrarán a través de un Popup (un recuadro) en el mapa . Este aparecerá al hacer click
    en el marcador correspondiente a cierto inmueble en el mapa, y desaparecerá al hacer click fuera de el, en otra parte del mapa.

    Todos los datos de cada inmueble, tanto los correspondientes a su información geoespacial (las coordenadas de su ubicación),
    como a su información no geoespacial (como su nombre y su dirección), están almacenados en nuestra base de datos,
    en una tabla correspondiente. Debemos entonces, generar el objeto Leaflet que será utliizado como capa de marcadores,
    a partir de los datos obtenidos desde dicha tabla de la base de datos.
    
    Sin embargo, para generar un objeto Leaflet que contenga la información geográfica que necesitemos, utilizando ún método de Leaflet
    para tal propósito, no podremos tener esos datos geográficos de origen estructurados de cualquier forma.
    Debemos tenerlos en cierto formato, el cual sea capaz de ser procesado adecuadamente por algún método de Leaflet.
    En este caso, los tendremos definidos en un objeto GeoJSON.

    Recibimos los datos de los inmuebles que deberán ser aquí mostrados en el mapa, directamente desde el método del controlador
    que invoque a esta vista Blade, con los datos correspondientes, ya sea para generar la vista principal o la filtrada, directamente
    como un objeto GeoJSON.
    
    Tal objeto GeoJSON, lo generará un método correspondiente, definido en el controlador.
    Lo construirá a partir de los datos de los inmuebles obtenidos en bruto (sin el formato adecuado), traidos desde la base de datos
    mediante un modelo de Eloquent ORM.
    
    Por lo tanto, en este archivo, correspondiente a la vista de la aplicación, ya obtendremos la información geográfica existente
    en nuestra base de datos, directamente estructurada en un formato adecuado para ser procesada por los métodos de Leaflet.
    
    Como cuestión importante, el objeto GeoJSON devuelto por la API será de tipo "FeatureCollection".
    
    Entonces, este objeto tendrá únicamente estos dos atributos:
    -   Un atributo 'type', con el string 'FeatureCollection' como valor. Este valor indica el tipo de objeto GeoJSON que es.
    
    -   Otro atributo 'features'. Este atributo contiene un array con varios elementos.
        Cada uno de los elementos de este array, es a la vez, otro objeto GeoJSON, pero de tipo "Feature".
    

    Cada objeto de tipo "Feature", existente dentro del objeto padre "FeatureCollection" (en su array 'features'),
    contendrá información geográfica, definida para algún elemento determinado. En nuestro caso, cada uno de los elementos
    representados por cada "Feature", será cada inmueble registrado en la aplicación.
    
    En un objeto de tipo "Feature", tendremos tres atributos:
    -   Un atributo 'type', con el string 'Feature' como valor. Este valor indica el tipo de objeto GeoJSON que es.
    
    -   Un atributo 'properties', con un objeto JSON anidado como valor.
        Este objeto JSON anidado en el atributo 'properties', no debe respetar ninguna convención establecida por el formato GeoJSON,
        por lo que no se trata de un objeto GeoJSON.
        Por lo general, este contiene toda la información NO geoespacial del elemento representado en ese "Feature".
        En nuestro caso, contiene datos tales como el nombre o dirección del inmueble representado.
        Aquí NO tendra su información geoespacial (si la tenía en versiones anteriores, pero fue eliminada para evitar información redundante).
        Podemos tener en este objeto anidado, atributos con cualquier nombre y cualquier valor.
        Estos mismos, pueden estar definidos en función de como los procese alguna parte de la aplicación.
        Sin embargo, la forma en que los definamos será una cuestión independiente de las normas definidas por el formato GeoJSON.
    
    -   Un atributo 'geometry', también con un objeto JSON anidado como valor.
        En este caso, el objeto anidado definido en este atributo, sí debe respetar las normas definidas por el formato GeoJSON.
        Será entonces, también un objeto GeoJSON. Este contendrá la información que si sea de tipo geoespacial,
        correspondiente al elemento representado en ese "Feature".
        Contiene un atributo 'type', cuyo valor define el tipo de geometría utilizada para representar los datos
        geográficos de ese elemento, y un atributo 'coordinates', cuyo tipo de valor estará determinado por el
        tipo de geometría a ser representada.
        En nuestro caso, al representar ubicaciones de inmuebles, siempre tendremos una geometría de tipo punto
        en cada objeto "Feature", que tengamos dentro del objeto GeoJSON padre (de tipo "FeatureCollection").
        Por lo tanto, el valor de 'type', aquí siempre será "Point".
        Ademas, el valor de 'coordinates', aquí siempre será un array con dos valores numéricos, que representan
        la latitud y la longitud del punto geográfico en que esté ubicado el inmueble aquí representado.
        Son las coordenadas definidas dentro este objeto 'geometry', las que efectivamente serán procesadas
        como información geoespacial.

    */



    //Pasamos entonces, a generar los objetos Leaflet que contengan información de los inmuebles.
    //Estos objetos, definiran los íconos de marcadores que serán mostrados en el mapa para señalar la ubicación de cada inmueble,
    //con un Popup asociado a cada uno, el cual se mostrará al clickear el ícono de marcador.
    //Luego de ser generados tales objetos Leaflet, deben serles aplicados al objeto mapa como capas, para poder así visualizar
    //el contenido definido en ellos en la renderización del mapa.



    //Utilizamos el plugin 'Leaflet.markercluster' para generar un cluster de marcadores: un objeto Leaflet
    //que podremos utilizar para añadir estilo y comportamiento a nuestra capa de marcadores, la cual generaremos luego.

    //Utilizaremos este objeto Cluster como capa intermedia, entre la capa de marcadores que vamos a generar,
    //y el objeto mapa de Leaflet al que se le debe finalmente aplicar.

    //¡Ojo! No utilizamos un método nativo de Leaflet para generar este objeto, sinó uno provisto por aquel plugin externo mencionado:
    //'L.markerClusterGroup'.
    //Declaramos la variable 'cluster_marcadores', en donde nos guardaremos una referencia al objeto que genere ese método.
    //Luego, en la misma línea, llamamos al método, inicializando directamente la variable que declaramos con una referencia
    //al objeto Leaflet que este método genere.

    let cluster_marcadores = L.markerClusterGroup();


    //Definimos aquí, la función que generará y devolverá nuestra capa de marcadores, que será aplicada
    //(indirectamente, por medio de la capa Cluster), a nuestro objeto mapa de Leaflet.
    //Esta recibe un solo parámetro, que será un objeto JavaScript que contenga el GeoJSON de 
    //tipo FeatureCollection, con todos los Outlets que deben ser señalados en el mapa de esta vista.

    function generarCapaMarcadores(featureColecttion) {
                  
                console.log("Entramos a generarCapaMarcadores");
                
                console.log(featureColecttion);
                    
                
                //Dentro del scope de esta función,
                //declaramos la constante 'capa_marcadores', donde nos guardaremos la referencia al objeto Leaflet correspondiente
                //a la capa de marcadores que vamos a generar y devolver. Incluimos el operador de asignación directamente en la misma línea,
                //para inicializar la variable directamente con esa referencia al objeto que generemos.
                //Utilizando esta referencia que nos guardamos, en una parte posterior de nuestro código,
                //le podremos aplicar este objeto aquí generado como capa a nuestro objeto mapa.
                const capa_marcadores = 
                
                //Llamamos al método 'L.geoJSON', para generar los objetos Leflet correspondientes a la capa de marcadores
                //que queremos obtener.
                //Estos objetos Leaflet, serán generados a partir de la información existente en el objeto GeoJSON completo
                //que obtuvimos desde la API.
                //Este método parsea un objeto GeoJSON completo, pasado como argumento, y genera varios objetos Leaflet,
                //utilizables como capas, a partir de la infomación contenida en él.
                //Aunque este método en realidad, generará varios objetos Leaflet diferentes, solo nos devuelve una referencia
                //a uno: el objeto generado de tipo LayerGroup, que utilizaremos como capa de marcadores.
                //Este objeto Leaflet de tipo LayerGroup, al que el método devuelve una referencia, se puede utilizar como capa.
                //Sin embargo, a la vez contendrá referencias a otros objetos Leaflet hijos, también generados por el propio método.
                //Esto es, 'L.geoJSON' genera varios objetos Leaflet hijos, y un objeto Leaflet padre de tipo LayerGroup,
                //que agrupa a los objetos hijos siendole estos aplicados como capas, y del cual devuelve una referencia.

                //Entonces, ese objeto LayerGroup devuelto por 'L.geoJSON' como referencia,
                //puede actuar como capa intermedia, ya que agrupa
                //a todos los objetos Leaflet hijos generados por ese método, y a la vez es fácilmente aplicable como
                //capa a otro objeto Leaflet de jerarquía superior.

                //De esta forma, podremos aplicarle facilmente como capa a nuestro objeto mapa,
                //con una sola línea de código, todos los objetos Leaflet que
                //definen marcadores generados aquí por este método,
                //sin necesidad de hacerlo individualmente por cada uno de ellos.


                //¡Ojo! Para evitar confusiones: este método 'L.geoJSON' NO genera ni devuelve objetos GeoJSON.
                //En cambio, genera y devuelve objetos Leaflet, creados a partir de la información contenida en un
                //objeto GeoJSON que recibe como argumento. Puede así, llegar a procesar un importante volumen de datos existentes
                //en ese objeto GeoJSON recibido, y volcarlos a varios objetos Leaflet generados por el propio método.

                //Le pasamos entonces, dos argumentos a 'L.geoJSON':
                //-El primero es el objeto GeoJSON completo que nos devolvió la API, para que lo parsee y procese sus datos.
                //-El segundo es un objeto JavaScript, que contendrá opciones sobre la forma en que este método debe generar
                // cada objeto Leaflet hijo, a partir de la información de cada elemento presente en el objeto GeoJSON.

                L.geoJSON(
                    //Aquí le pasamos como primer argumento a 'L.geoJSON', el objeto GeoJSON tipo FeatureCollection completo,
                    //que fue recibido desde el controlador y parseado luego como objeto JavaScript.
                    //De esta forma, este objeto GeoJSON padre pasado como aquí como argumento, contendrá varios objetos GeoJSON hijos
                    //anidados definidos en el, todos de tipo "Feature".
                    featureColecttion
                    
                    ,
                    //Aquí le pasamos, como segundo argumento a 'L.geoJSON', un objeto JavaScript, con las opciones correspondientes
                    //para definir las caracteristicas de cada objeto Leaflet hijo a ser generado por este método.
                    //Cada objeto Leaflet hijo, se generará a partir de cada objeto GeoJSON hijo (de tipo "Feature"),
                    //que tengamos en el objeto GeoJSON padre (de tipo "FeatureCollection"), pasado como primer argumento.
                    
                    {
                        //Podríamos tener varios atributos a ser definidos en este objeto JavaScript de opciones.
                        //En este caso, solo definimos el atributo 'pointToLayer'.
                        //Este determinará las caracteristicas
                        //de cada objeto Leaflet hijo que 'L.geoJSON' generará, a partir de cada objeto GeoJSON 
                        //hijo de tipo "Feature" existente en el
                        //objeto GeoJSON padre procesado, que tenga geometría de tipo punto ("Point").  
                        
                        //El objeto GeoJSON padre aquí procesado, que fue generado por nuestro controlador,
                        //siempre tendrá todos sus elementos "Feature" (que representan cada inmueble), con geometría de tipo "Point".
                        //Esto establecimos en la instacia de la aplicación donde el objeto GeoJOSN padre es creado,
                        //a partir de la información de la base de datos.

                        //Entonces, lo que establezcamos aquí en este atributo 'pointToLayer', se aplicará para generar
                        //todos los objetos Leaflet hijos de 'L.geoJSON', a partir de cada objeto
                        //GeoJSON hijo de tipo "Feature" existente en el objeto GeoJSON padre.

                        pointToLayer: 
                            //Este atributo contendrá un método, el cual definimos como una función anónima.
                            
                            //Esta función anónima, se ejecutará tantas veces como la cantidad de elementos (objetos "Feature"),
                            //tengamos en nuestro objeto GeoJSON padre (el "FeatureCollection") que está siendo parseado.
                            //Procesará un elemento distinto del objeto GeoJSON padre por cada ejecución.
                            
                            //Esta función devolverá un objeto Leaflet. Es a partir de cada objeto Leaflet devuelto por esta función,
                            //que 'L.geoJSON' generará cada objeto Leaflet hijo. Cada uno de ellos, contendrá información
                            //correspondiente al elemento ("Feature"), que esté siendo procesado en
                            //este momento por 'L.geoJSON'.

                            
                            //El objeto Leaflet devuelto por esta función, deberá tener caracteristicas de capa.
                            
                            //Debemos tomar en cuenta que, si bien cada objeto Leaflet devuelto por cada ejecución de esta función
                            //anónima, dará origen a cada objeto Leaflet hijo generado por 'L.geoJSON',
                            //posteriormente el mismo método 'L.geoJSON' mutará por si solo este objeto devuelto,
                            //añadiendole mas atributos y datos de forma transparente para nosotros,
                            //independientemente de lo que definamos aquí en esta función.
                            
                            //Por lo tanto, aquí simplemente estableceremos el prototipo de cada objeto Leaflet hijo
                            //generado por 'L.geoJSON', pero no todo el contenido que cada uno tendrá.

                            //Esta función anónima recibe dos parámetros en su definición: 'geoJsonPoint' y 'latlng'.
                            //Al definir la función correspondiente al método 'pointToLayer' desde este lugar y con tales
                            //parámetros, al ejecutarse la función, estos dos parámetros adoptatán referencias a objetos.
                            //Estos objetos, adoptarán un contenido diferente cada vez que esta función sea ejecutada.
                            //Si bien la estructura de estos dos objetos será siempre la misma, en su contenido
                            //adoptarán valores correspondientes al contenido del objeto GeoJSON hijo que
                            //'L.geoJSON' esté procesando en ese momento, y ejecutando esta función anónima para hacerlo.

                            //El objeto referenciado por 'geoJsonPoint', adoptará el contenido íntegro de cada objeto "Feature"
                            //existente
                            //en el objeto GeoJSON padre, que esté siendo procesado en este momento por 'L.geoJSON'.


                            //El objeto referenciado por 'latlng', contendrá solo dos atributos: 'lat' y 'lng'.
                            //Estos dos atributos, adoptarán los valores de latitud y longitud, codificados como números,
                            //correspondientes al objeto GeoJSON hijo que esté siendo procesado en este momento por 'L.geoJSON'.

                            //Este objeto referenciado por 'latlng', no incorporará el resto de la información del
                            //elemento "Feature" procesado en este momento, sinó solamente sus coordenadas numéricas.
                            

                            //Definimos entonces, la función:
                            function(geoJsonPoint, latlng) {

                                //Ejecutamos primero, con propósitos de análisis, estos dos 'console.log', pasandoles como argumento
                                //los parámetros recibidos en la función anónima. Así, podemos visualizar el contenido que
                                //adoptarán los objetos referenciados por estos parámetros,
                                //en cada ejecución diferente de esta función anónima por cada elemento que se procese.

                                console.log(geoJsonPoint);
                                console.log(latlng);


                                //Debemos ahora, generar el objeto Leaflet con características de capa, a partir del cual será luego
                                //devuelta una referencia por esta función anónima.

                                //Lo generamos utilizando el método 'L.marker', al cual le pasamos 'latlng' como argumento.
                                //Recordemos que 'latlng', hace referencia a un objeto que contiene únicamente los valores 
                                //de las coordenadas del objeto GeoJSON hijo siendo procesando en ese momento
                                //por 'L.geoJSON', y ninguna otra información adicional. 
                                
                                //Para propositos de debug, nos guardaremos una referencia al objeto que nos devuelva 'L.marker',
                                //en una constante 'capa_marcador' que definimos.
                                const capa_marcador=L.marker(latlng);
                                
                                
                                
                                //IMPORTANTE: al ejecutar el método 'console.log', pasandole como argumento una referencia
                                //a un objeto, al expandir en el navegador el contenido de ese objeto que estamos logueando,
                                //veremos el contenido que este tiene en el momento que lo expandimos en el navegador, y
                                //no el contenido que este tenía en el momento que fue emitido por 'console.log'.

                                //Ejecutamos ahora, con propósitos de debug, estos 'console.log's:
                                

                                //Al primero, le pasamos como argumento directamente 'L.marker(latlng)', sin guardarnos,
                                //en este caso, ninguna referencia al objeto generado por dicho método en esa sentencia.
                                //Veremos así que, cada objeto Leaflet generado por 'L.marker', es inicialmente muy sencillo,
                                //teniendo apenas tres atributos, y con muy poca información en sus valores. Básicamente,
                                //la única información relevante que contiene inicialmente, son las coordenadas geográficas
                                //recibidas como argumento a traves de 'latlng'.
                                console.log("L.marker(latlng):");
                                console.log(L.marker(latlng)); 

                                //Luego, mostramos por consola un objeto también generado por 'L.marker', pero al cual nos guardamos
                                //previamente una referencia al momento de ser generado, en 'capa_marcador'.
                                //Al ser mostrado por consola mediante esa referencia, veremos que el objeto tiene ahora
                                //una estructura mucho mas grande y compleja que al ser generado inicialmente por
                                //'L.marker'.
                                //Esto es un claro indicio de que este objeto, al que nos guardamos la referencia, si bien
                                //inicialmente fue generado por el método 'L.marker' dentro de esta función anónima,
                                //fue luego mutado por 'L.geoJSON', en algún otro momento de su ejecución.
                                
                                //Esto es también evidente debido a que, al desplegar la visualización del contenido de este
                                //objeto Leaflet (referenciado por 'capa_marcador') en la consola del navegador,
                                //emitida por este 'console.log', veremos mucha
                                //información contenida en el, correspondiente al objeto GeoJSON hijo ("Feature"), a partir del
                                //cual fue creado.
                                //Sin embargo, nunca le pasamos aquí al método 'L.marker', al momento de generar este objeto
                                //Leaflet, toda esa información que ahora contiene y podemos ver.
                                //Esto significa que, este objeto solamente pudo haber adquirido dicha información posteriormente
                                //a ser generado aquí por 'L.marker', siendo que nunca le pasamos a ese método más información
                                //que dos coordenadas numéricas, utilizadas para establecer la ubicación del marcador deseado
                                //en el mapa. 


                                console.log("capa_marcador:");
                                console.log(capa_marcador);
                                
                                //Como estos 'console.log' se ejecutarán tantas veces como esta función anónima sea ejecutada,
                                //una por cada elemento "Feature" existente en el objeto GeoJSON padre,
                                //en cada una de estas ejecuciones mostrarán el
                                //contenido de un objeto diferente, correspondiente a cada
                                //objeto Leaflet hijo generado por 'L.geoJSON'. Veremos allí toda la información cargada en
                                //esos objetos por ese método. Vemos también que cada uno,
                                //tendrá un numero de ID de Leaflet diferente
                                //como valor en el atributo correspondiente '_leaflet_id'.




                                //En el 'return' de esta función anonima, debemos devolver un objeto Leaflet,
                                //con caracteristicas de capa.
                                //Cada objeto devuelto aquí en cada ejecucion de esta función anónima, dará origen a
                                //cada objeto Leaflet hijo generado por 'L.geoJSON'.

                                
                                //Queremos que cada uno de estos objetos Leafler hijos, definan un marcador, pero con un Popup
                                //asociado, con información sobre el inmueble representado. El Popup debe mostrarse al hacer click
                                //sobre ese marcador, dentro del mapa renderizado.
                                
                                //Entonces, en este 'return' de la función anónima, colocamos primero 'capa_marcador',
                                //variable donde referenciamos al objeto Leaflet previamente generado con 'L.marker'.
                                //A esta variable, le encadenamos el método 'bindPopup', que mutará el contenido del objeto
                                //desde el cual fue llamado, asociandole el Popup correspondiente que queríamos,
                                //antes de ser finalmente devuelto por esta función.
                                

                                
                                //A este 'return', podríamos hablerle pasado directamente 'L.marker(latlng)', 
                                //en lugar de pasarle una variable, encadenadolo con el
                                //método 'bindPopup' y sus argumentos correspondientes.
                                //Sin embargo, al pasarle una variable, podremos así observar, utilizando esa referencia,
                                //que ese mismo objeto generado aquí en esta función anónima, será mutado luego.
                                
                                //Esto no lo podríamos observar si a este 'return', le pasaramos directamente 'L.marker', sin
                                //referenciar previamente en ningun lado el objeto que este método genere.

                                //Este fue entonces, el procedimiento que debimos realizar para comprender mejor el funcionamiento
                                //del método 'L.geoJSON': definimos variables en esta función anónima, la cual es llamada por
                                //'L.geoJSON', en parte de su proceso de generación de contenido, donde referenciamos
                                //los objetos que allí se instancian. 
                                //De esta manera, es así posible visualizar todo el contenido generado por 'L.geoJSON',
                                //de parte en parte.
                                //No podríamos ver sinó, de forma independiente, el contenido de cada objeto Leaflet hijo generado
                                //por dicho método, fuera del contexto del objeto LayerGroup del que nos devuelve una referencia.
                                //Probablemente ademas, ni hubieramos dado cuenta de la existencia de estos objetos hijos
                                //de forma autónoma, ṕor fuera del objeto LayerGroup donde son referenciados.

                                return capa_marcador
                                    //Al método 'bindPopup', que encadenamos al objeto devuelto por 'L.marker', le pasamos solo una
                                    //función callback como argumento. Este callback, será utilizado para generar dinámicamente,
                                    //el contenido del Popup asociado a cierto marcador mostrado en el mapa.
                                    //Al llamar a este método desde un objeto, el contenido de este será mutado,
                                    //al incorporar información correspondiente al Popup que le asociamos aquí.
                                    .bindPopup(
                                        //Este callback, también lo definimos como función anónima.
                                        //El mismo SOLO se ejecuta al hacer click en el marcador correspondiente dentro del
                                        //mapa, para así generar dinámicamente el contenido de su Popup asociado,
                                        //en el momento que se efectúe ese click.

                                        //Funciona de manera similar al manejador de un evento 'onClick'.
                                        //Recibe un parámetro 'layer' en su definición. Este parámetro, adoptará la referencia
                                        //a un objeto Leaflet: aquel que corresponda a la capa que estableció el ícono de marcador
                                        //que fue clickeado en el mapa por el usuario. Este será uno de los objetos Leaflet
                                        //hijos generados por 'L.geoJSON'. Si bien estamos definiendo esta función anónima
                                        //dentro de la estructura de un argumento pasado a 'L.geoJSON' en su llamada,
                                        //esta solo se ejecutará una vez que 'L.geoJSON' haya completado su ejecución y
                                        //generado todos los objetos que debía. Ademas, también luego de que estos
                                        //objetos generados fueran aplicados como capas al objeto mapa.
                                        
                                        //Tal procedimiento es realizado ni bien la página es
                                        //cargada en el navegador. Por lo tanto, al hacer click en algún ícono de marcador
                                        //mostrado en el mapa, el objeto referenciado por el parámetro 'layer' en la
                                        //ejecución esta función, ya contendrá en su estructura todos los valores cargados
                                        //que necesitamos, de manera que puedan
                                        //ser accedidos correctamente mediante la cadena de anidamiento de atributos,
                                        //tal como está codificada en este callback.

                                        function (layer) {

                                            //Utilizo primero, estos 'console.log', sobre todo para poder observar que el objeto
                                            //referenciado aquí por el parámetro 'layer', corresponde al mismo que
                                            //referenciamos en la variable 'capa_marcador', mas arriba en nuestro código
                                            //en el lugar correspondiente.

                                            //Vemos que los dos primeros 'console.log' muestran objetos con idéntico contenido,
                                            //y el tercero devuelve el valor 'true', indicando que tanto el parámetro recibido
                                            //'layer' como la variable 'capa_marcador', referencian exactamente al mismo objeto.
                                            console.log("layer y capa_marcador:");
                                            console.log(layer);
                                            console.log(capa_marcador);
                                            console.log(capa_marcador==layer);

                                            //Utilizo otro 'console.log', para mostrar por consola el contenido de
                                            //nuestro objeto mapa al momento de ejecutarse este callback.
                                            console.log("map:");
                                            console.log(map);

                                            //Este callback pasado a 'bindPopup', devuelve un string que contiene código HTML.
                                            //En el, estará codificado el contenido del Popup que estamos asociando
                                            //al marcador que clickeamos en el mapa.
                                            //Dicho código HTML, está contenido dentro de los atributos del objeto referenciado
                                            //por el parámetro 'layer', recibido en este callback.
                                            //Particularmente, estará contenido en el atributo 'map_popup_content',
                                            //definido dentro de su atributo 'properties', el cual a su vez, está definido
                                            //dentro de su atributo 'feature'.
                                            //En este atributo 'feature', estará cargado el contenido completo del 
                                            //objeto GeoJSON de tipo "Feature", a partir del cual fue generado este objeto Leaflet,
                                            //utilizado como capa para mostrar el ícono de marcador clickeado en el mapa.
                                            //El propio método 'L.geoJSON', se habrá encargado de cargar tal información
                                            //en este objeto Leaflet, al parsear el objeto GeoJSON padre de tipo "FeatureCollection",
                                            //que le pasamos como primer argumento.
                                            
                                            //Vemos aquí que, el objeto anidado que está definido en el atributo 'properties'
                                            //del objeto GeoJSON de tipo "Feature", si bien no respeta el formato GeoJSON,
                                            //es aquí utilizado por la aplicación para generar el contenido que necesita.
                                            //Por lo tanto, debe respetar una estructura requerida por esta aplicación en particular,
                                            //independiente del formato GeoJSON.

                                            //El contenido de ese código HTML devuelto, no está almacenado en la base de datos 
                                            //directamente, sino que es generado por el modelo de Eloquent ORM que tenemos
                                            //definido en la aplicación, a partir de los datos que si existen, efectivamente,
                                            //en nuestra base de datos.
                                            return layer.feature.properties.map_popup_content;

                                        } //Cerramos el cuerpo del callback pasado a 'bindPopup' como argumento.
                                    ); //Cerramos la llamada a 'bindPopup'.
                            } //Cerramos el cuerpo de la función anónima asociada al atributo 'pointToLayer'.
                    } //Cerramos la definición del objeto de opciones, pasado como segundo parámetro a 'L.geoJSON'.
                ); //Cerramos la llamada a 'L.geoJSON'.

                //En estas líneas previas, vemos que cerramos una sentencia muy compleja, con una estructura con muchos
                //niveles de anidamiento.

                //Por lo tanto, es conveniente indicar que aquí, aún nos encontramos dentro del cuerpo de función callback pasada
                //como argumento al método 'then', de la Promise devuelta por 'axios.get' (con el que hicimos la petición
                //a la API).

                //Ejecutamos un 'console.log', mostrando el contenido del objeto referenciado por la variable 'capa_marcadores'.
                //Este es el cual 'L.geoJSON' nos devolvió su referencia, correspondiente al objeto Leaflet padre de tipo LayerGroup
                //generado por el, que agrupa a los objetos Leaflet hijos también generados por el.

                console.log("Contenido de capa_marcadores:");
                console.log(capa_marcadores);
                

                //Vemos que, en el objeto referenciado por 'capa_marcadores' que logueamos por consola, tenemos un atributo
                //'_layers'. El valor de este atributo, es un objeto JavaScript, que consta de tanta cantidad de atributos
                //como cantidad de inmuebles registrados en la aplicación tengamos en este momento.
                //Siendo que, por cada inmueble registrado, teníamos un "Feature" diferente existente en el
                //objeto GeoJSON padre, de tipo "FeatureCollection", obtenido desde la API y parseado por 'L.geoJSON'.
                //A la vez, por cada uno de estos "Feature" existentes, tendremos un objeto Leaflet hijo,
                //generado por 'L.geoJSON'.
               
                //Cada clave definida en ese objeto anidado (en el atributo '_layers' del objeto completo recién logueado), es igual a
                //cada número de ID de Leaflet de cada objeto Leaflet hijo generado por 'L.geoJSON'.
                //Luego, el valor asociado a cada una de esas claves, constituye el contenido íntegro
                //de cada uno de estos objetos Leaflet hijos.
                
                //Pudimos visualizar previamente, mediante el código definido mas arriba en el lugar correspondiente,
                //el contenido de todos estos objetos Leaflet hijos, uno por uno y en forma autónoma al objeto padre LayerGroup,
                //mediante un 'console.log' con la variable 'capa_marcador' pasada como argumento. Esa variable referenciaba
                //cada objeto Leaflet hijo, desde el momento de su generación inicial por el método 'L.marker'.

                //De esta manera, podemos ahora observar que el objeto referenciado por 'capa_marcadores', incorpora en su estructura,
                //el contenido de cada objeto Leaflet hijo, de forma íntegra. Se convierte así, en su objeto padre.
                
                //Sin embargo, vemos que en este objeto LayerGroup referenciado por 'capa_marcadores', no hay mucho mas contenido que
                //ese objeto anidado en su atributo '_layers', ademas del campo con su numero de ID de Leaflet.
                
                //Devolvemos el objeto Leaflet que será nuestra capa de marcadores
                return capa_marcadores;

        } //Cerramos aquí, el cuerpo de 'generarCapaMarcadores'

    //Mostramos aquí por consola, el contenido del objeto Cluster de marcadores en su estado en este punto del código:
    console.log('cluster inicial:');
    console.log(_.cloneDeep(cluster_marcadores));

    //Añado un bloque de código renderizado condicionalmente, utilizado para convertir el GeoJSON recibido por la vista, correspondiente a
    //nuestra FeatureCollection con los Outlets que deben aquí ser mostrados, desde un formato comprensible por PHP,
    //a un formato comprensible por JavaScript. 
    //Este bloque solo se ejecutará si recibimos en esta vista, una variable '$outletsFeatColl', desde donde la vista
    //fue invocada por la aplicación Laravel. Como en los únicos dos métodos de nuestro controlador que invocan esta vista
    //(aunque pasandole datos diferentes), le pasan a esta misma dicha variable '$outletsFeatColl', el bloque debería renderizarse siempre.
    //Aquí haremos uso intensivo de las funciones que definimos en :'<root>/app/Helpers/FuncionesHelper.php'
    @isset($outletsFeatColl)

        //En '$outletsFeatColl' deberíamos tener un array PHP, donde tendremos codificado el objeto GeoJSON de tipo FeatureCollection
        //a ser aquí procesado.
        console.log("$outletsFeatColl está definida");
        
        //Con una función auxiliar definida por nosotros, mostramos por consola lo que tenemos en '$outletsFeatColl', convirtiendolo
        //antes a string
        {{debug_to_console_view(json_encode($outletsFeatColl))}}
        {{debug_to_console_view(gettype($outletsFeatColl))}} //array

        
        //Con otra función auxiliar definida por nosotros, convertimos el valor de '$outletsFeatColl' desde un array PHP
        //a un string legible por JavaScript
        const strOutletsFeatColl= {{ php_array_to_js_string($outletsFeatColl) }};
        console.log({{ php_array_to_js_string($outletsFeatColl) }});
        console.log(strOutletsFeatColl);

        //Finalmente, con 'JSON.parse', convertimos ese string JavaScript que contiene ahora el GeoJSON recibido, a un objeto JavaScript.
        //Nos guardamos ese objeto en la constante 'outletsFeatColl'.
        const outletsFeatColl=JSON.parse(strOutletsFeatColl);
        
        console.log(outletsFeatColl);

        
        //Ahora, obtenemos una referencia al objeto Leaflet de tipo LayerGroup devuelta por 'generarCapaMarcadores', al pasarle como argumento
        //el objeto que nos quedó en outletsFeatColl.
        //Esa referencia se la aplicamos como capa al objeto Leaflet correspondiente al Cluster de marcadores, generado previamente
        //con un método del plugín 'Leaflet.markercluster', y referenciado por 'cluster_marcadores'.
        
        //Esta variable 'cluster_marcadores', fue declarada e inicializada por fuera del scope de la llamada a
        //'generarCapaMarcadores', por lo que el objeto referenciado por ella, podrá ser mutado desde este lugar del código.
        //El resultado de esta mutación, será visble en todo el scope donde la variable fue declarada. En este caso,
        //el scope global de este archivo.  
        
        cluster_marcadores.addLayer(generarCapaMarcadores(outletsFeatColl));

        //De esta forma, podemos utilizar este objeto Cluster, como capa intermedia:
        //En vez de aplicar el objeto LayerGroup como capa directamente al objeto mapa, se lo aplicamos como capa al
        //objeto Cluster. Luego, este objeto Cluster sí será aplicado directamente como capa al objeto mapa.
        //De esta forma, podremos utlizar todas las funcionalidades provistas por el plugin 'Leaflet.markercluster'.
        

        //Ejecutamos otro 'console.log', mostrando ahora el contenido del objeto correspondiente al cluster de marcadores,
        //referenciado por la variable 'cluster_marcadores'.
        console.log("Contenido de cluster_marcadores:");
        console.log(cluster_marcadores);
        
        //Vemos que, en este caso, no tenemos un atributo '_layers' definido en este objeto Cluster.
        //Tenemos, en cambio, muchos atributos correspondientes a las caracteristicas y funcionalidades provistas por
        //el plugín 'Leaflet.markercluster'. Entre ellos, está el atributo '_featureGroup'. Allí
        //tendremos como valor, un objeto anidado. Es dentro de este objeto anidado que, efectivamente,
        //tendremos los atributos correspondientes a un objeto estandar de Leaflet, entre ellos el atributo '_layers'.
        //El valor del atributo '_layers' que tenemos aquí, es un objeto anidado,
        //con un contenido idéntico al que teníamos como valor en
        //el atributo '_layers' del objeto LayerGroup referenciado por 'capa_marcadores'.
        
        //No tenemos aquí, en nuestro objeto Cluster, ninguna referencia al número de ID de Leaflet correspondiente al
        //objeto LayerGroup, aún siendo que fue este último el que aplicamos directamente como capa al primero.
        //Es decir, el objeto Cluster adoptó directamente las referencias a los objetos Leaflet
        //hijos generados por 'L.geoJSON', omitiendo la referencia al objeto padre LayerGroup que los agrupaba,
        //y a traves del cual se los aplicamos como capas.

        //Si no quisieramos utilizar esa capa intermedia provista por 'Leaflet.markercluster', haríamos directamente:
        //map.addLayer(generarCapaMarcadores(outletsFeatColl));

        //La variable 'map' también es visible en el scope global, por lo que la sentencia anterior, si no estuviera
        //comentada, afectaría directamente a nuestro objeto mapa existente en ese scope global.
    
    
    {{--
    Si no recibieramos nuestro FeatureCollection directamente desde el controlador, podríamos obtenerlo mediante un llamado a la API
    (esto hacíamos en versiones previas):
    NOTA: Dejamos definido este código a propositos de ejemplificación, pero de acuerdo a como tenemos definidas en nuestro controlador
    las dos posibles invocaciones diferentes a esta vista, nunca debería ser renderizado (siempre le pasamos '$outletsFeatColl').
    --}}
    @else
        //Llamamos a la API con axios, y nos devolvería el FeatureCollection en la response, referenciado en 'response.data'
        //directamente como objeto JavaScript (parseado automáticamente por Axios)
        axios.get('{{ route('api.outlets.index') }}')
        .then(function(response){
            //Generamos la capa de marcadores y se la aplicamos al objeto Cluster de la misma forma que en el bloque anterior.
            cluster_marcadores.addLayer(generarCapaMarcadores(response.data));
        })
        .catch(
            function (error) {
                //Lo único que haremos aquí, será mostrar por consola el objeto devuelto por la Promise, con los datos del error.
                console.log('error');
                console.log(error);
            }//Cerramos aquí el cuerpo del callback que le pasamos como argumento al 'catch', de la Promise devuelta por 'axios.get'.
        );//Cerramos aquí la llamada al método 'catch'.

    //NOTA: En versiones previas, la vista filtrada recibía el polígono a partir del cual debia filtrar los Outlets, e invocaba a la
    //API con un método POST, pasandole la información de ese polígono. Como la versión actual de esta vista ya no recibe ese polígono, sinó la
    //colección de Outlets ya filtrada a partir de el, mas las coordenadas del centroide correspondiente al polígono, ya no podríamos hacer
    //dicha llamada a la API para generar la información de la vista filtrada.

    //El llamado se hacía de la forma:
    //axios.post('{{ route('api.outlets.filtered') }}',{'json':geoJsonPolygon})

    //Donde en 'geoJsonPolygon' teníamos un objeto JavaScript con un GeoJSON de tipo Feature definido allí, conteniendo el polígono recibido
    //desde el controlador, tras haber sido parseado con 'JSON.parse'.

    //En ese caso, teníamos una operación asíncrona: la capa de marcadores era generada y aplicada indirectamente al objeto Mapa
    //recién cuando se completaba la Request HTTP a la API y su correspondiente Response, lo cual demoraba un tiempo considerable.
    //Ahora, ese recurso requerido, es recibido por la vista Blade en un paso previo a la renderización del código devuelto al cliente,
    //por lo cual ya no tenemos esa operación asíncrona, y optimizamos un montón la performance: es ahora mucho mas rápida y
    //ademas utiliza un volumen mucho menor de información transferida mediante Requests/Responses HTTP.
    @endisset

    //Una vez ejecutadas estas operaciones, tendremos todos los objetos Leaflet que necesitamos, 
    //cargados con la información pertinente obtenida desde dicho recurso, visibles desde el scope global.
    
    //No tendremos, sin embargo, en el scope global, una referencia a los objetos Leaflet que fueron generados y referenciados
    //solo en cada scope existente dentro de la estructura de 'generarCapaMarcadores', al ser esta función invocada.
    //Simplemente, veremos el contenido de los objetos allí generados, volcado en los objetos visibles en el scope global.


    //Mostramos aquí, el contenido de nuestro objeto mapa, con solamente la capa TileLayer aplicada a el.
    console.log('Contenido de map solo con TileLayer:');
    console.log( _.cloneDeep(map));

    //Vemos que aquí, en su atributo '_layers', tenemos un solo elemento, correspondiente a ese objeto de tipo TileLayer.

    //Luego, aplicamos el objeto Cluster que utilizamos como capa intermedia, al objeto mapa, siendo que previamente,
    //le aplicamos el objeto LayerGroup devuelto por 'L.geoJSON', como capa a este objeto Cluster (mediante 'generarCapaMarcadores').
    map.addLayer(cluster_marcadores);
    

    
    console.log('Contenido de map con TileLayer y Cluster:');
    console.log(map); 

    //NOTA: Cuando la carga de la capa de marcadores se realizaba de forma asíncrona, no era aplicada inmediatamente al
    //objeto Mapa, sino que demoraba unos segundos hasta que se completara la petición a la API.


    //Luego de haber realizado todas estas operaciones, en este punto del código, ya tendremos renderizado nuestro mapa Leaflet,
    //en el cual podemos visualizar sobre un plano, la ubicación de cada inmueble registrado en la aplicación, señalado cada uno
    //por un marcador. Ademas, al hacer click sobre alguno de esos marcadores, podremos ver un Popup que muestre la información
    //correspondiente al inmueble que este señala, como su nombre, dirección y coordenadas.

    //Ya tenemos así, un mapa Leaflet plenamente funcional, en el cual se ve la información geográfica que necesitamos.

    //Debemos ahora, codificar el comportamiento que tendrá el mapa exclusivamente cuando algún usuario se encuentre
    //logueado en la aplicación. De no haber ningún usuario logueado en la aplicación, esta comportamiento codificado a continuación
    //no se cargará, y el mapa se comportará únicamente de acuerdo al código codificado hasta este punto.

    //Sin embargo, queremos que, de estar logueado un usuario, este tenga la posibilidad de registrar fácilmente un
    //nuevo inmueble en la aplicación, haciendo click sobre un punto cualquiera en el mapa.
    //Al hacer click sobre ese punto en el mapa (siempre y cuando este click no sea sobre algún marcador ya renderizado),
    //se mostrará en dicho punto un nuevo marcador, con un Popup asociado al mismo.
    //Ese Popup, ademas de mostrar las coordenadas del punto, mostrará también un link, que nos dirigirá a la vista de creación
    //de un nuevo inmueble para ser registrado en la aplicación, con los valores de las coordenadas del punto clickeado en el mapa.
    //Estos serán establecidos como valores por defecto, en los campos correspondientes a las coordenadas del formulario de dicha vista,
    //cuando el link del Popup nos diriga a ella.

    
    //Incluimos aquí, algo de código PHP embebido entre las directivas Blade 'php/endphp', simplemente a efectos de prueba
    
    @php
    use Illuminate\Support\Facades\Gate;

    
    error_log('Log desde la vista: ');
    if(Auth::user()){
        error_log("Usuario logueado: ".Auth::user()->name);  
    }
    else{
        error_log("No hay usuario logueado");
    }

    if(Gate::allows('prueba-gate')){
        error_log("Puerta abierta");
    }
    else{
        error_log("Puerta cerrada");
    }

    //error_log(Auth::user()->name);
    error_log("\n\nEntramos al log desde la vista:\n\n");
    error_log(gettype(Auth::user()));
    $inmueble=new App\Outlet;

    error_log(request('hola'));
    //dd((new App\User)::all());

    error_log(gettype($inmueble));
    error_log(json_encode((array)$inmueble));
    //error_log(json_encode(new App\Outlet));
    //error_log($obj_inmueble);
    error_log("\n\nSalimos del log desde la vista:\n\n");
    @endphp

    
    

    //Incluimos ahora, un bloque de código dentro de las directivas Blade 'can/else/endcan'.
    //Esta directiva 'can', hace uso de la clase 'Gate', para determinar si una determinada acción está autorizada o no.
    //Si dentro de las Policies (politicas de autorización), definidas en nuestra aplicación Laravel, el usuario autenticado
    //en este momento, tiene permiso para crear un nuevo Outlet, es decir, registrar un nuevo inmueble en nuestra aplicación,
    //entonces el bloque de código JavaScript situado entre estas directivas de apertura y cierre (can/endcan),
    //definido en este archivo Blade en el lado del servidor, se incluirá en el código devuelto al lado del cliente.

    @can('create', 'App\Outlet')

    //El código generado por la directiva anterior, es equivalente a este:
    {{--@if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', new App\Outlet))--}}
    
    //Puede ademas, asemejarse a esta línea, que funcionaría igual en este caso particular (pero su comportamiento sería diferente
    //si modificaramos la definición de la Policy, en el archivo correspondiente de nuestra aplicación):
    {{-- @if (Auth::user() && Auth::user()->can('create', new App\Outlet)) --}}
   
    //En este caso, en la Policy correspondiente a la acción 'create', definida en nuestro archivo 'OutletPolicy',
    //(la cual fue previamente registrada en el archivo 'AuthServiceProvider', asociandola al modelo 'Outlet'), definimos que
    //cualquier usuario autenticado en un determinado momento, tiene la potestad de registrar nuevos inmuebles en la aplicación.
    //Sin embargo, si en este momento no hay ningún usuario autenticado en la aplicación, la directiva 'can' dará un valor falso,
    //por lo que este bloque de código definido a continuación, no se renderizará en el documento devuelto al lado del cliente.
    //Esto nos sirve entonces, para renderizar condicionalmente este bloque de código en esta vista Blade:
    //se renderiza en el navegador si y solo si, hay algún usuario autenticado en este momento.
    //De lo contrario, el mapa allí renderizado, no incorporará el comportamiento definido en este bloque.

    //El comportamiento definido en este bloque consiste en que, cuando el usuario haga click en algun punto del mapa renderizado,
    //aparezca inmediatamente allí un ícono de marcador, mostrando a la vez un Popup con un link que nos dirija a la vista
    //de registro de un nuevo inmueble ('create'). En el formulario definido en dicha vista, tendremos cargados por defecto,
    //en sus campos 'Latitude' y 'Longitude', los valores correspondientes a las coordenadas del punto aquí clickeado,
    //y todos los demás inicialmente vacíos.

    //Añadimos también aquí el siguiente comportamiento: cuando el usuario haga click en el mapa, fuera de un polígono que previamente
    //dibujó, ese dibujo debe desaparecer.

    //Pasamos entonces, a comentar cada línea del código de este bloque:

    //Definimos una variable 'theMarker', sin darle ningún valor inicial.
    let theMarker;
    
    //Debemos tener en cuenta que nuestro objeto Leaflet de tipo mapa, tendrá métodos que hereda de la clase 'Evented'.
    //Estos métodos, nos permiten manejar eventos que ocurren sobre el objeto Leaflet desde el que son llamados,
    //de forma similar a como se manejan los eventos estandar de JavaScript (sin la librería Leaflet).

    //Para asociar un manejador de evento a nuestro objeto de tipo mapa, desde la variable 'map' donde el mismo está
    //referenciado, llamamos al método 'on'. El método 'on' recibe dos argumentos:
    //-Un string, indicando el evento al que responderá el manejador del evento.
    //-Un callback, que funcionará como manejador del evento.
    

    map.on(
        //Como primer argumento, pasamos un string con valor 'click'. Este es el nombre del evento asociado.
        //De esta forma, el callback correspondiente al manejador del evento, se ejecutará cuando el usuario haga
        //click en alguna parte del mapa.

        //¡Ojo! Si el usuario hace click sobre algún ícono de marcador ya mostrado en el mapa, este evento NO se disparará.
        //Solo lo hará cuando el usuario haga click en alguna parte del mapa en donde no esté presente un ícono de marcador.
        'click'
        , 
        //Como segundo argumento, pasamos la función callback manejadora de evento, la cual se ejecutará cuando el evento ocurra.
        //En este caso, la función es llamada 'onClick', pero podría tener cualquier otro nombre, o incluso ser una función anónima.
        //Recibe un parámetro 'e'. Este parámetro, funcionará de forma similar a un objeto Event estandar de JavaScript.
        //Sin embargo, este objeto contendrá algunos atributos con información específica relativa al objeto Leaflet sobre el
        //que ocurrió el evento, ademas de un atributo que contiene, efectivamente, un objeto Event
        //correspondiente al de un evento estandar de JavaScript.

        //En este caso, como el evento (click), ocurrirá sobre nuestro objeto mapa, el objeto referenciado por el parámetro 'e',
        //contendrá dentro de sus atributos, información sobre la ubicación del punto en donde se hizo click en el mapa,
        //tanto a nivel geográfico como a nivel del recuadro donde es renderizado el mapa en el navegador.
        //Nos interesa aquí, particularmente, obtener las coordenadas de la posición del mapa en donde el usuario hizo click.
        
        function onClick(e) {

            @if(isset($principal) && $principal)        
            //Solo si estamos en la vista principal, debemos definir este 'if' encerrando el siguiente bloque de código:
            //el mismo NO debe ejecutarse mientras estemos dibujando un polígono. En la vista filtrada, ni tendremos dicho flag,
            //por lo que sería innecesario renderizar esta línea de código JavaScript (y nos daría error al no tenerlo declarado).
            if(!dibujando){
            @endif       

            console.log("Punto clickeado")
            //Primero, hacemos unos 'console.log's, con propósito de debug.
            //En este caso, primero del contenido de los objetos referenciados por 'map' y 'tile'
            console.log("Contenido de Map y tile:");
            console.log(map);
            console.log(tile);

            //Luego, hacemos un 'console.log' del objeto 'e'. De esta forma, podremos ver su estructura y contenido.
            console.log("Contenido del objeto e:");
            console.log(e);

            //Vemos que uno de los atributos de 'e', es 'latlng'.
            //'latlng', contendrá un objeto anidado, con solo dos atributos: 'lat' y 'lng'.
            //Estos dos atributos, adoptarán los valores de latitud y longitud, en formato de número (no de string), del punto
            //geográfico clickeado en el mapa.

            //Declaramos entonces dos variables 'latitude' y 'longitude', en donde nos guardaremos los valores de latitud y longitud
            //del punto del mapa  clickeado por el usuario, en algún formato que nos convenga, para luego generar el contenido
            //a mostrar en el Popup que queremos.
            
            //Para lograr esto, debemos acceder a la información correspondiente a las coordenadas que tenemos en el objeto
            //referenciado en 'e'.
            //Debemos buscarla en la parte correspondiente de su estructura, la cual presenta varios niveles de anidamiento.
            //Luego, a esos valores obtenidos, debemos aplicarles métodos para procesarlos como Strings, en lugar de como números.

            //Describiremos aquí, paso a paso, como obtenemos el valor que necesitamos de la latitud del punto clickeado:

            let latitude =          //Declaro la variable 'latitude'

                e.                  //Comienzo accediento al primer nivel de la estructura anidada del objeto referenicado por 'e'.

                latlng.             //Dentro ese objeto, accedo al atributo 'latlng', que contendrá un objeto anidado.

                lat.                //Dentro del objeto anidado en ese atributo 'latlng', accedo al atributo 'lat' en el nivel
                                    //siguiente de anidamento.
                                    //Este atributo contendrá el valor de la latitud del punto clickeado, en formato de número.

                toString().         //Al valor numérico que obtuve en 'e.latlng.lat', le aplico el método 'toString()', de
                                    //forma que me devuelva ese mismo valor, pero codificado como String en lugar de como número.

                substring(0, 17);   //Finalmente, con el método 'substring', al que le paso los valores 0 y 15 como argumentos,
                                    //recorto el String recien generado, de forma tal que me quedo solo con sus primeros 15
                                    //caracteres, Descarto así, varias cifras decimales de ese valor, que se consideran innecesarias.
            
            //Hecho esto, en la variable 'latitude', tenemos el resultado devuelto por el último método en la cadena,
            //del encadenamiento que realizamos recién ('substring').
            //Este resultado será el valor de latitud del punto clickeado, en el formato que necesitamos.

            //Una vez descripto el procedimiento para obtener el valor de latitud, utilizaremos el mismo procedimiento para obtener
            //ahora, el valor de longitud. Vemos que podemos hacer todos los pasos, directamente en una sola línea.
            //La única diferencia con la sentencia anterior, es que aquí accedemos al atributo 'lng' en vez de 'lat',
            //en la parte correspondiente de la estructura anidada del objeto 'e', ademas del nombre de la variable donde
            //nos guardamos el resultado obtenido ('longitude' en vez de 'latitude').
                                    
            let longitude = e.latlng.lng.toString().substring(0, 17);

            //Tenemos ahora guardado, ambos valores de coordenadas, en el formato que necesitamos.

            //Luego, este 'if' definido a continuación evita que, al hacer click el usuario reiteradas veces en distintos puntos del
            //mapa renderizado (fuera de un ícono de marcador),
            //se vayan acumulando en él, todos los íconos de marcador generados a partir de cada click, borrando el ícono
            //generado en un click anterior.

            //Recordemos que la variable 'theMarker', la declaramos mas arriba, pero sin ningún valor inicializado.
            //Por lo tanto, en un principio, su valor es 'undefined', de modo que no entramos al 'if'.
            //Solo entramos al 'if' a partir de la segunda vez que hagamos click en algún punto del mapa.
            if (theMarker != undefined) {

                console.log("Entro al if de theMarker");
                
                //Cuando hagamos click en un punto del mapa, diferente al del click previo,
                //'theMarker' en ese momento, contendrá el objeto Leaflet utilizado como capa aplicada al mapa,
                //para mostrar el ícono de marcador correspondiente al generado por el click previo.
                //Por lo tanto, si 'theMarker' ya había sido cargada con dicho objeto Leaflet, y aplicado como capa
                //al objeto mapa, se la desaplicaremos en esta sentencia.

                //Utlizamos para tal propósito, el método 'removeLayer', existente en el objeto referenciado por 'map' desde
                //el cual la llamamos, pasandole 'theMarker' como argumento.

                //Así, el ícono de marcador generado por el click anterior, así como su Popup asociado, se eliminará
                //de la renderización del mapa, antes de que se genere el ícono de marcador y el Popup
                //correspondiente al click actual.
                map.removeLayer(theMarker);
            };

            //Declaramos una variable 'popupContent', que contendrá un string. El valor de este string será el contenido del
            //Popup del marcador que se genere cuando el usuario haga click en algún punto del mapa.

            
            var popupContent =
                //La inicializamos con el texto 'Your location:', seguido de los valores de latitud y longitud del punto clickeado,
                //que guardamos previamente con el formato adecuado en las variables 'latitude' y 'longitude'.
                "Your location :<br>" + latitude + " , " + longitude;

            //Luego de inicializarla con aquel texto, le concatenaremos otro texto mas.
            //Este texto que le concatenaremos, corresponde a código HTML, estableciendo un link hacia la vista 'create',
            //que es en donde registran los nuevos inmuebles en la aplicación.
            
            //Si bien es factible definir todo el código HTML que queremos concatenar al string 'popupContent' en una sola línea,
            //es conveniente definirlo de a pasos, de forma que se ilustre mejor el procedimiento que utlizamos para ello.

            //Generamos primero, la URL a la que dirigirá este link.
            //En esta URL que generamos, incluímos parámetros dentro de la misma, correspondientes a los valores obtenidos
            //de latitud y longitud, para de esta forma pasarselos a la vista 'create'. Así, dentro del formulario que esta
            //vista contiene, podran ser inicializados los campos de latitud y longitud con los valores correspondientes
            //que aquí le pasamos.
            

            //Vamos concatenando las partes del string con el operador '+'.

            //Declaramos una const 'url', a la cual le asignamos el string con la URL que generamos.
            const url=                              

                //Invocamos al método 'route' de Laravel, que nos devuelve, a partir del nombre interno definido en Laravel para
                //una determinada ruta, su URL completa.
                '{{ route('outlets.create') }}' + 
                
                //Añadimos primero el caracter '?', que indica que estamos pasando parámetros por URL. Luego, el texto 'latitude'
                //que será el nombre de la primera variable pasada como parámetro.
                //Finalmente, añadimos el caracter '=', que utilizamos para asignar el valor que contendrá dicho parámetro.
                '?latitude=' +

                //Concatenamos luego, el contenido de la variable 'latitude',
                //con el valor de latitud que obtuvimos previamente como string.
                latitude + 

                //Añadimos ahora, el caracter '&', que indica que estamos pasando otro parámentro por URL.
                //Luego el texto 'longitude', que será el nombre de la otra variable pasada como parámetro.
                //Finalmente, añadimos nuevamente el caracter '=', para asignar el valor que contendrá el segundo parámetro.
                '&longitude=' +

                //Por último, concatenamos el contenido de la variable 'longitude',
                //con el valor de longitud que obtuvimos previamente como string, cerrando aquí la definición del String.
                longitude;

            //Luego de este procedimiento, en la constante 'url' tenemos cargado el valor de la URL a la cual debe dirigir
            //el link que se muestre en el Popup de los marcadores que se muestren cuando el usuario haga click en alguna
            //parte del mapa.

            //Podemos verla a través de un 'console.log'.
            console.log("url:");
            console.log(url);

            //Un ejemplo de URL generada posible, sería:
            //"http://localhost:8000/outlets/create?latitude=-34.61653279911&longitude=-58.38400840759"

            //A continuación, debemos seguir concatenando texto a 'popupContent', el cual contendrá en este caso, la definición
            //de un link hacia la URL generada previamente.
            //Utilizamos el operador '+=',  para concatenar mas contenido al String que tenemos allí hasta el momento.

            //En el string que se concatenará, con código HTML, tenemos allí un tag <br>, y un tag <a> de apertura y cierre. 
            //En el valor del atributo 'href' del tag <a>, colocamos, utilizando el operador '+', el valor de la URL previamente
            //generada, guardado en la const 'url'.
            //Entre los tags de apertura y cierre de <a>, colocamos el texto mostrado en el link del Popup.
            popupContent += '<br><a href="' + url + '">Add new outlet here</a>';

            //Podríamos haber hecho todo este procedimiento anterior en una sola línea, de la siguiente forma:
            //popupContent += '<br><a href="{{ route('outlets.create') }}?latitude=' + latitude + '&longitude=' + longitude + '">Add new outlet here</a>';

            //Ahora entonces, tenemos cargado en 'popupContent' todo el contenido que se debe mostrar en el Popup asociado al
            //marcador que se muestra cuando el usuario haga click en algún punto del mapa.

            //Debemos ahora, generar el objeto Leaflet con características de capa, utilizado para mostrar el ícono de marcador
            //en el mapa, cuando el usuario haga click en algun punto de el.
                
            //Lo generamos utilizando el método 'L.marker', al cual le pasamos como argumento ahora, un array de dos elementos.
            //Estos dos elementos serán el contenido de las variables 'latitude' y 'longitude', que tienen los valores de
            //las coordenadas del punto donde el usuario clickeó el mapa, codificadas en este caso como strings
            //('L.marker' las reconoce igual con ese formato).

            //Al objeto devuelto por 'L.marker', le encadenamos el método 'addTo' con 'map' como argumento, de manera que
            //le sea aplicado como capa a nuestro objeto mapa.

            //Ademas, nos guardamos una referencia a este objeto generado en la variable 'theMarker',
            //de modo que posteriormente podamos operar con el.

            theMarker = L.marker([latitude, longitude]).addTo(map);
            
            //Añado un escuchador de eventos al nuevo objeto marcador, de modo que desaparezca al clickear sobre el.
            theMarker.on('click',function onClick(e){
                map.removeLayer(theMarker);
                }
            );

            //Una vez que tenemos generado ese objeto de marcador, y luego aplicado como capa al objeto mapa,
            //le encadenamos el método 'bindPopup' a través de la referencia que nos guardamos de el en 'theMarker',
            //para asociarle así, el Popup correspondiente que queremos.

            //A 'bindPopup' le pasamos como argumento la variable 'popupContent',
            //que contiene el string con el contenido que definimos para el.

            //Finalmente, le encadenamos el método 'openPopup' (sin argumentos), de forma que el Popup que generemos,
            //asociado al marcador recién generado,
            //se muestre de forma inmediata, sin necesidad de clickear en el ícono del marcador para que se muestre.
            theMarker
                .bindPopup(popupContent)
                .openPopup();

        @if(isset($principal) && $principal)
        //Nuevamente, solo si estamos en la vista principal, debemos renderizar estas líneas:

            //Si hacemos click en el mapa MIENTRAS NO estemos dibujando un polígono:
            //Si previamente dibujamos un polígono con Leaflet Draw (siendo que quedó guardada una referencia a el en 'poligono_anterior').
            if(poligono_anterior) 
                //Lo eliminamos como capa de 'editableLayers', objeto a traves del cual estaba aplicado indirectamente al objeto Mapa.
                editableLayers.removeLayer(poligono_anterior); 
        } //Cerramos aquí el cuerpo del 'if' renderizado condicionalmente en la vista principal ('  if(!dibujando) {...  ').
        @endif

    });
        

    
    //Este 'else' define el bloque de código que se renderizará condicionalmente si no hay ningún usuario autenticado en
    //este momento en la aplicación, y ademas estemos en la vista principal.
    //Si no hay nigún usuario autenticado, igual queremos borrar el polígono que haya sido dibujado por el usuario anónimo
    //cuando se haga click fuera del mismo en el mapa.

    @else
    @if(isset($principal) && $principal)

    map.on('click', function onClick(e){
        if(!dibujando){
            if(poligono_anterior)
                editableLayers.removeLayer(poligono_anterior);
        }
    });
    
    @endif
    @endcan
    //Finalizamos aquí, nuestro bloque de código que será renderizado condicionalmente. 
    //También, cerramos aquí la definición de todo el código JavaScript que codificamos para esta vista Blade.

</script>

{{--
    Finalmente, cerramos el bloque de código encerrado entre las directivas '@push/@endpush'. En este caso,
    correspondiente a código interpretado por el motor de vistas Blade, y no especificamente a código JavaScript.
    De esta forma, todo el código de este bloque, en el código devuelto al lado del cliente,
    será fusionado con el código definido el archivo Blade correspondiente
    a la plantilla principal, de acuerdo las directivas que definamos tanto aquí como en aquel archivo.
--}}
@endpush