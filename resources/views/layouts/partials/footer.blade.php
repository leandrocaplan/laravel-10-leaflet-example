<footer class="text-center fixed-bottom mb-2">
    The source code of this project is available on <a href="https://gitlab.com/leandrocaplan/laravel-10-leaflet-example/" target="_blank">GitLab</a>.
</footer>
