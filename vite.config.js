import { defineConfig } from 'vite';

import laravel, { refreshPaths } from 'laravel-vite-plugin';

import vue from '@vitejs/plugin-vue';
import { viteCommonjs, esbuildCommonjs } from '@originjs/vite-plugin-commonjs';


export default defineConfig({
    plugins: [
        // This creates part of the magic.
        viteCommonjs(),
        laravel.default({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: [
                ...refreshPaths,
                
            ],
        }),
        vue({
                template: {
                    transformAssetUrls: {
                        base: null,
                         includeAbsolute: false,
                     },
                 },
             }),
        
        ],
    
});


